/**
 * written&maintain by Teacher Dong
 * just use its function
 */

#pragma once

#include <vector>
#include <string>
#include <atlimage.h>
#include "regpara.h"

using namespace std;

std::string wstr2str(const std::wstring& ws);
std::wstring str2wstr(const std::string& s);
bool CaptchaPred(FBB::Entity::CaptchaPara& para,string picfname,string rootdir,string& restr);
