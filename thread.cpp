#include "thread.h"

boost::shared_ptr<boost::thread_group> ThreadClass::_regThreadGroup 
	= boost::shared_ptr<boost::thread_group>(new boost::thread_group());
boost::shared_ptr<boost::thread_group> ThreadClass::_publishThreadGroup 
	= boost::shared_ptr<boost::thread_group>(new boost::thread_group());

ThreadClass::ThreadClass(IEEntityClass* p_IEEntity, string regOrPublish) {
	if( regOrPublish=="reg" )
		_thread = _regThreadGroup->create_thread(
			boost::bind(&ThreadClass::excute, this, p_IEEntity)
			);
	else
		_thread = _publishThreadGroup->create_thread(
			boost::bind(&ThreadClass::excute, this, p_IEEntity)
			);
}

ThreadClass::~ThreadClass() {
	_thread->interrupt();
	_thread->join();
}

void ThreadClass::excute(IEEntityClass* p_IEEntity) {
	if( SUCCEEDED(::CoInitialize(NULL)) ) {
		try {
			p_IEEntity->excuteSingle();
		} catch(boost::thread_interrupted& ) {

		} catch(std::exception& ) {

		}

		delete p_IEEntity;
		p_IEEntity = NULL;

		CoUninitialize();
	} else {
		delete p_IEEntity;
		p_IEEntity = NULL;
	}
}