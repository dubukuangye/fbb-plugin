#pragma once

#include <string>
#include <ctime>
#include <vector>

using namespace std;

namespace FBB{
	namespace Entity{
#pragma region RegPara
		class RegParaEntity;
		class RegStepEntity;
		class OneOptRegEntity;
		class DomElementEntity;
		class PWGenRuleEntity;
		class EmailFindRule;
		class EmailURLRule;

		class RegParaEntity
		{
		public:
			RegParaEntity(){}
			~RegParaEntity() {}
			long totSteps;
			string startUrl;
			vector<RegStepEntity> allsteps;
			string LoginName;
			string LoginPw;
			string sucText;
			long regCompStep;
			long siteID;
			bool initWait;
			bool Valid;
			time_t ValidTime;
			bool NeedEmailValid;
		};

		class RegStepEntity{
		public:
			RegStepEntity(){}
			~RegStepEntity(){}

			bool stepWait;
			vector<OneOptRegEntity> allopts;
		};

		class DomElementEntity
		{
		public:
			enum DomElementSeachTypeEntity{
				ID,
				Indexes,
				FullInnerText,
				PartialInnerText
			};
			
			DomElementEntity(){}
			~DomElementEntity(){}

			DomElementSeachTypeEntity stype;
			string eleId;
			vector<long> indexes;
			vector<long> frameIndexs;
			string tagName;
			string fitext;
			string pitext;
		};

		class PWGenRuleEntity
		{
		public:
			PWGenRuleEntity(){
				minLength = maxLength = 8;
				FirstAlpha = true;
			}
			~PWGenRuleEntity(){}

			long minLength;
			long maxLength;
			bool FirstAlpha;
			bool ContainNum;
			bool lowercase;
		};

		class UNGenRuleEntity
		{
		public:
			UNGenRuleEntity(){
				minLength = maxLength = 8;
				FirstAlpha = true;
			}
			~UNGenRuleEntity(){}

			long minLength;
			long maxLength;
			bool FirstAlpha;
			bool ContainNum;
			bool lowercase;
		};

		class EmailFindRule //ÓÊ¼þ²éÕÒ¹æÔò
        {
        public:
        	EmailFindRule(){}
			~EmailFindRule(){}

			bool empty() {
				return fromStr.empty()&&subjectStr.empty()
					? true : false;
			}

        	string fromStr;//·¢¼þÈËÖÐ°üº¬µÄ×Ö·û
        	string subjectStr;//ÓÊ¼þÖ÷ÌâÖÐ°üº¬µÄ×Ö·û
    	};
    	class EmailURLRule//ÓÊ¼þÖÐµÄURL²éÕÒ¹æÔò
    	{
    	public:
    		EmailURLRule(){}
			~EmailURLRule(){}

			bool empty() {
				return urlStr.empty() ? true : false;
			}
			
        	string urlStr;//URL³¬Á´ÖÐ°üº¬µÄ×Ö·û
    	};

		class OneOptRegEntity
		{
		public:
			enum OneOptRegTypeEntity{
				InvokeMember = 0,//InvokeMember eleOpt 
				// FireEvent=1,   //FireEvent
				SetDirectValue = 1,//SetValue eleOpt
				SetFieldValue = 2,//SetValue eleOpt
				SelectionDirect = 3,// SetValue eleOpt FireEvent
				SelectionSequence = 4,//SetValue eleOpt index Some Option
				Captcha = 5,
				User=6,
				PassWord=7,
				RepeatPassWord=8,
				BuinessDirectionMap=9,
				BuinessModeMap = 10,
				ProDirectoryMap = 11,
				AreaMap = 12,
				Focus=13,
				MobileCaptcha=14,
				ExtractUserName=15,
				QuestionCaptcha=16,
				QuestionCaptchaSel=161,
				InvokeKeyPress=17,
				RedirectURL = 18,
				SetRegUser = 19,
				AttachValue = 20,
				UploadFile = 21,
				Email_URL = 22,
				WaitReady = 23,
				RecordCaptcha = 24,
				RepeatCaptcha = 25,
			};

			OneOptRegEntity(){}
			~OneOptRegEntity(){}

			DomElementEntity domEle;
			OneOptRegTypeEntity type;
			string eleOpt;
			long selindex;
			bool fireNext;
			DomElementEntity captureEle;
			bool captchaNeedActive;
			PWGenRuleEntity pwgenrule;
			long pageType;
			string Url;
			UNGenRuleEntity usernamegenrule;
			DomElementEntity changeEle;
			long fileIndex;
			EmailFindRule emailFind; //ÓÊ¼þ²éÕÒ¹æÔò
			EmailURLRule emailUrl; //ÓÊ¼þÖÐµÄurl²éÕÒ¹æÔò
			long emailTime; //²éÕÒÓÊ¼þµÄ×î´óµÈ´ýÊ±¼ä(Ãë)
			long emailTimeError; //¶Ô·½ÍøÕ¾·¢ËÍÓÊ¼þµÄÊ±¼äÎó²î(Ãë)
			bool autoCaptcha; //ÊÇ·ñ×Ô¶¯Ê¶±ðÑéÖ¤Âë
			long captchaType;//ÑéÖ¤ÂëÀàÐÍ
		};
#pragma endregion

#pragma region PublishPara
		class PublishParaEntity;

		class PublishParaEntity
		{
		public:
			PublishParaEntity(){}
			~PublishParaEntity(){}
			long totSteps;
			string startUrl;
			vector<RegStepEntity> allsteps;
			bool initWait;
			long siteID;
			bool Valid;
			time_t ValidTime;
			long PublishType;
		};	
#pragma endregion

#pragma region BusinessDirection
		class BusinessDirectionPara;
		class BusinessDirePair;
		class MyBusinessDire;

		class MyBusinessDire
		{
		public:
			MyBusinessDire(){}
			~MyBusinessDire(){}

			long id;
			string title;
		};

		class BusinessDirePair{
		public:
			BusinessDirePair(){}
			~BusinessDirePair(){}

			MyBusinessDire mymode;
			OneOptRegEntity oor;
		};

		class BusinessDirectionPara
		{
		public:
			BusinessDirectionPara(){}
			~BusinessDirectionPara(){}
			long siteID;
			long Type;
			string Url;
			vector<BusinessDirePair> allbms;
			bool Valid;
			time_t ValidTime;
		};
#pragma endregion

#pragma region BusinessMode
		class BusinessModePara;
		class BusinessModePair;
		class MyBusinessMode;

		class MyBusinessMode
		{
		public:
			MyBusinessMode(){}
			~MyBusinessMode(){}

			long id;
			string title;
		};

		class BusinessModePair{
		public:
			BusinessModePair(){}
			~BusinessModePair(){}

			MyBusinessMode mymode;
			OneOptRegEntity oor;
		};

		class BusinessModePara
		{
		public:
			BusinessModePara(){}
			~BusinessModePara(){}
			long siteID;
			long Type;
			string Url;
			vector<BusinessModePair> allbms;
			bool Valid;
			time_t ValidTime;
		};
#pragma endregion

#pragma region ProductDirectory
		class MyProDire;
		class ProductDirectoryPara;
		class ProDirePair;

		class MyProDire
		{
		public:
			MyProDire(){}
			~MyProDire(){}

			long id;
			string title;
			long parentid;
		};

		class ProDirePair{
		public:
			ProDirePair(){}
			~ProDirePair(){}

			MyProDire mymode;
			vector<OneOptRegEntity> oors;
		};

		class ProductDirectoryPara
		{
		public:
			ProductDirectoryPara(){}
			~ProductDirectoryPara(){}
			long siteID;
			long Type;
			string Url;
			vector<ProDirePair> allbms;
			bool Valid;
			time_t ValidTime;
		};
#pragma endregion

#pragma region AreaMap
		class MyArea;
		class AreaPara;
		class AreaPair;

		class MyArea
		{
		public:
			MyArea(){}
			~MyArea(){}

			string id;
			string title;
			string parentid;
		};

		class AreaPair{
		public:
			AreaPair(){}
			~AreaPair(){}

			MyArea mymode;
			vector<OneOptRegEntity> oors;
		};

		class AreaPara
		{
		public:
			AreaPara(){}
			~AreaPara(){}
			long siteID;
			long Type;
			string Url;
			vector<AreaPair> allbms;
			bool Valid;
			time_t ValidTime;
		};
#pragma endregion

#pragma region CaptchaPara
		class CaptchaOpt
	    {
		public:
			enum CaptchaOptType {
		        Color2Gray=1,
		        RemoveFrame=2,
		        ZoomDouble=3,

		        TwoValuedOTSU=101,
		        TwoValuedOTSU_Fixed=102,

		        ProduceConRegion=201,
		        ProduceConRegion_Reverse=202,
		        Correction_TemplateChangeBlack=203,
		        Correction_TemplateChangeWhite=204,
		        DilationPM=205,
		        RuiHua_Gaosi=206,

		        BackbonePM=301,
		        ZoomBackbonePM=302,

		        GeneralPartConSplit=401,

		        PredictPM=501,
	    	};

			enum CaptchaOptType opt;
	        int intpara;
	    };
		class CaptchaPara
		{
		public:
			CaptchaPara(void){}
			~CaptchaPara(void){}
			 int SiteID;
		     int CaptchaType;	 
			 vector<CaptchaOpt> allsteps;
			 int modelType;
			 int numChar;
			 
			 bool Valid; 
			 int totSteps(){
				 return allsteps.size();
			 };
		};
#pragma endregion

#pragma region EmailSite
		class EmailSite
	    {
		public:
			long ID;
			string SiteName;
	        string SiteURL;
	        string DomainName;
	        string Pop3;
	        long Port;
	        bool UseSSL;
	    };
#pragma endregion

	}
}