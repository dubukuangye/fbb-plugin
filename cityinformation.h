#pragma once

#include "wsClient.h"
#include <iostream>
#include <string>
#include <map>
#include <cstdlib> // for getenv
#include <fstream>
#include "SystemHelpers.h" // for getAppDataPath

using namespace std;

struct CITYINFO
{
	string id;
	string cityname;
	string parentid;
};

class CityInformation {
private:
	map<string, CITYINFO> _idCityInfoMap;
	
	static const string _sFILENAME;
	static const string getFileName();

public:
	~CityInformation(){}

	static CityInformation* _pCityInfo;

	static CityInformation* getInstance() {
		if( _pCityInfo==NULL )
			_pCityInfo = new CityInformation();

		return _pCityInfo;
	}

	static void deleteInstance() {
		if( _pCityInfo!=NULL )
			delete _pCityInfo;
	}

	/**
	 * [GetLocated description]
	 * 获取城市相应编号
	 * 具体请咨询董老师，我只负责实现他的意思
	 * 现在有点忘了具体的含义
	 * @param  provinceName
	 * @param  cityName
	 * @param  countryName
	 * @return
	 */
	string GetLocated(string provinceName, string cityName, string countryName); 
private:
	CityInformation();

	void readFile();
	void writeFile();

	bool matchCityName(string provinceName, string cityName, string countryName, CITYINFO cityinfo);
	CITYINFO getCityInfoByID(string id);
};
