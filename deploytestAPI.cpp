#include "JSObject.h"
#include "variant_list.h"
#include "DOM/Document.h"
#include "global/config.h"

#include "deploytestAPI.h"
#include <cstdlib>
#include <exception>
#include <windows.h>

// for debug
#include <ctime>
string currentTime1 = "";
string currentTime2 = "";
string getCurrentTime() {
    time_t rawtime;
    time(&rawtime);

    return ctime(&rawtime);
}

deploytestAPI::~deploytestAPI() {
	try {
		stopRegister_thread();

        // must afer all threads stop
        CityInformation::deleteInstance();
        UserinformationClass::deleteInstance();
	} catch(...) {
		FBLOG_INFO("guo", "error occur in ~deploytestAPI");
	}
}

deploytestPtr deploytestAPI::getPlugin()
{
    deploytestPtr plugin(m_plugin.lock());
    if (!plugin) 
        throw FB::script_error("The plugin is invalid");
    return plugin;
}
std::string deploytestAPI::get_version()
{
    return FBSTRING_PLUGIN_VERSION;
}

void deploytestAPI::beginDownload(const string& fnameremote, const string& fnamelocal) {
    m_downloadThread.reset(new boost::thread(
        boost::bind(&deploytestAPI::beginDownload_thread, this, fnameremote, fnamelocal)
    ));
}

void deploytestAPI::beginDownload_thread(const string& fnameremote, const string& fnamelocal) {
    downloadFromUrl(fnameremote, fnamelocal);
}

void writeToFile(const char *ptr, size_t len, string filepathForSave) {
    ofstream fp;
    fp.exceptions ( std::ofstream::failbit | std::ofstream::badbit );
    try {
        FBB::Helper::UtilClass::createDirectoryByPath( filepathForSave );

        fp.open(filepathForSave, ios::out | ios :: binary );
        fp.write(ptr, len);
        fp.close();
    } catch(std::ofstream::failure e) {
        throw FBB::Exception::FBBException("[writeToFile] : fail");
    }
}

void deploytestAPI::downloadFromUrl(string url, string filepathForSave) {
    FB::HttpStreamResponsePtr resp = FB::SimpleStreamHelper::SynchronousGet(m_host, FB::URI::fromString(url));
    if( resp==NULL||!resp->success ) 
        throw FBB::Exception::FBBException("[downloadFromUrl] : fail");

    writeToFile(reinterpret_cast<const char*>(resp->data.get()), resp->size, filepathForSave);
}

void deploytestAPI::stopRegisterOrPublish(const string& registerOrPublish){
    if( registerOrPublish=="register" )
        m_stopRegThread.reset(new boost::thread(
            boost::bind(&deploytestAPI::stopRegister_thread, this)
        ));
    else
        m_stopPublishThread.reset(new boost::thread(
            boost::bind(&deploytestAPI::stopPublish_thread, this)
        ));
}

void deploytestAPI::stopRegister_thread(){
	try {
		ThreadClass::interruptRegThreadGroup();
        ThreadClass::waitUtillRegThreadsStop();
        m_regThread->join();
    } catch(std::exception& ) {
		FBLOG_INFO("guo", "error occur in stopRegister");
	}   
}

void deploytestAPI::stopPublish_thread(){
    try {
        ThreadClass::interruptPublishThreadGroup();
        ThreadClass::waitUtillPublishThreadsStop();
        m_publishThread->join();
    } catch(std::exception& ) {
        FBLOG_INFO("guo", "error occur in stopRegister");
    }   
}

bool deploytestAPI::initializeFBB(
     long userId
    ,const FB::JSObjectPtr& regErrorCallback
    ,const FB::JSObjectPtr& regCompleteCallback
    ,const FB::JSObjectPtr& regFinishCallback
    ,const FB::JSObjectPtr& regCaptchaCallback
    ,const FB::JSObjectPtr& batchFinishCallback 
    ,const FB::JSObjectPtr& publishErrorCallback
    ,const FB::JSObjectPtr& publishCompleteCallback
    ,const FB::JSObjectPtr& publishCaptchaCallback
    ,const FB::JSObjectPtr& downloadCallback)
{
    UserinformationClass::setUserId( userId );
    _batchFinishCallback = batchFinishCallback;

    IEEntityClass::downloadCallback = downloadCallback;
    RegistrationClass::setCallback(
         regErrorCallback
        ,regCompleteCallback
        ,regFinishCallback
        ,regCaptchaCallback
    );
    PublishClass::setCallback(
         publishErrorCallback
        ,publishCompleteCallback
        ,publishCaptchaCallback
    );
    return true;
}

void deploytestAPI::setRegCaptchaAnswer(long siteId, const string& captchaKind, const wstring& answer) {
    if( siteRegistrationMap.find(siteId)==siteRegistrationMap.end() ) return;

    RegistrationClass* registrationClass = siteRegistrationMap[siteId];
    registrationClass->setCaptchaAnswer( captchaKind, answer );
}

vector<wstring> deploytestAPI::getRegCaptchaQuestion(long siteId, const string& capthcaKind) {
    try {
        RegistrationClass* registrationClass = siteRegistrationMap[siteId];
        return registrationClass->getCaptchaQuestion();
    } catch(...) {
		vector<wstring> errorVec;
		errorVec.push_back(L"error");
		return errorVec;
    }
}

void deploytestAPI::setPublishCaptchaAnswer(long siteId, long PMID, const string& captchaKind, const wstring& answer) {
    if( sitePublishMap.find( PublishKey(siteId, PMID) )==sitePublishMap.end() ) return;
    
    PublishClass* publishClass = sitePublishMap[ PublishKey(siteId, PMID) ];
    publishClass->setCaptchaAnswer( captchaKind, answer );
}

vector<wstring> deploytestAPI::getPublishCaptchaQuestion(long siteId, long PMID, const string& capthcaKind) {
    try {
        PublishClass* publishClass = sitePublishMap[ PublishKey(siteId, PMID) ];
        return publishClass->getCaptchaQuestion();
    } catch(...) {
        vector<wstring> errorVec;
        errorVec.push_back(L"error");
        return errorVec;
    }
}

bool deploytestAPI::registerInBatch(const vector<long>& siteIds){
    m_regThread.reset(new boost::thread(
        boost::bind(&deploytestAPI::registerInBatch_thread,
            this, siteIds)
    ));

    return true;
}

void deploytestAPI::registerInBatch_thread(const vector<long>& siteIds){
    try {
        vector<long>::const_iterator it = siteIds.begin();
        while( it!=siteIds.end() ) {
            long siteId = *it;

            RegistrationClass* p_registration = new RegistrationClass(siteId);
            siteRegistrationMap[siteId] = p_registration;
            ThreadClass* p_thread = new ThreadClass(p_registration, "reg");
            siteRegThreadMap[siteId] = p_thread;

            it++;
        }
        
        ThreadClass::waitUtillRegThreadsStop();

        it = siteIds.begin();
        while( it!=siteIds.end() ) {
            long siteId = *it;

            delete siteRegThreadMap[siteId];
            siteRegThreadMap[siteId] = NULL;

			it++;
        }
        siteRegistrationMap.clear();
        siteRegThreadMap.clear();

        _batchFinishCallback->InvokeAsync("", FB::variant_list_of("register"));

    } catch (std::exception& ) {
        
    } catch(boost::exception& ) {

    }
}

bool deploytestAPI::publishInBatch(const vector<long>& siteIds, const std::vector<long>& PMIDs){
    m_publishThread.reset(new boost::thread(
        boost::bind(&deploytestAPI::publishInBatch_thread,
            this, siteIds, PMIDs)
    ));

    return true;
}

void deploytestAPI::publishInBatch_thread(const vector<long>& siteIds, const std::vector<long>& PMIDs){
    try {
        vector<long>::const_iterator sIt = siteIds.begin();
        std::vector<long>::const_iterator pIt = PMIDs.begin();
        while( sIt!=siteIds.end()&&pIt!=PMIDs.end() ) {
            long siteId = *sIt;
            long PMID = *pIt;

            PublishClass* p_publish = new PublishClass(siteId, PMID);
            sitePublishMap[ PublishKey(siteId, PMID) ] = p_publish;
            ThreadClass* p_thread = new ThreadClass(p_publish, "publish");
            sitePublishThreadMap[ PublishKey(siteId, PMID) ] = p_thread;

            sIt++;
            pIt++;
        }
        
        ThreadClass::waitUtillPublishThreadsStop();

        sIt = siteIds.begin();
        pIt = PMIDs.begin();
        while( sIt!=siteIds.end()&&pIt!=PMIDs.end() ) {
            long siteId = *sIt;
            long PMID = *pIt;

            delete sitePublishThreadMap[ PublishKey(siteId, PMID) ];
            sitePublishThreadMap[ PublishKey(siteId, PMID) ] = NULL;

            sIt++;
            pIt++;
        }
        sitePublishMap.clear();
        sitePublishThreadMap.clear();

        _batchFinishCallback->InvokeAsync("", FB::variant_list_of("publish"));

    } catch (std::exception& ) {
        
    } catch(boost::exception& ) {

    }
}