﻿#include "evaluator.h"

RegistrationClass* castToRegistrationClass(IEEntityClass* parentClass) {
    RegistrationClass* childClass = dynamic_cast<RegistrationClass*>(parentClass);
    if( childClass==NULL )
        throw FBB::Exception::FBBException("[downcast error] : from IEEntityClass to RegistrationClass");
    return  childClass;
}

bool InvokeMemberEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();

    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    if( !oor.fireNext ) {
        p_browser->invokeMember(he, oor.eleOpt, p_parser);
    } else {
        MSHTML::IHTMLElementPtr hefire = NULL;
        CIEEvent* evobj = NULL;

        try {
            hefire = p_parser->FindElement(oor.changeEle);
            if( hefire ) {
                evobj=new CIEEvent();
                evobj->AttachEvent(hefire);
            }
        } catch(FBB::Exception::FBBException& ) {

        }

        p_browser->invokeMember(he, oor.eleOpt, p_parser);
        p_browser->checkDocumentComplete(WM_ELE_ONCHANGE);

        if( evobj!=NULL )
            delete evobj;
    }

    return true;
}

bool SetDirectValueEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();

    HRESULT hr;
    CComBSTR tmpBstr;
    wstring wsEleOpt;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    he->get_tagName( &tmpBstr );
    std::wstring tagName( tmpBstr );
    std::transform(tagName.begin(), tagName.end(), tagName.begin(), ::tolower);
    wsEleOpt = FB::utf8_to_wstring( oor.eleOpt );
    if( tagName.compare(L"input")==0 ) {
       _bstr_t bstrBag = (_bstr_t)"value";
       _variant_t var = _variant_t( wsEleOpt.c_str() );
       if( FAILED( hr=he->setAttribute(bstrBag, var, 0) ) )
           return false;
   } else {
       CComBSTR bs(wsEleOpt.c_str());
       if( FAILED( hr=he->put_innerText(bs) ) )
           return false;
   }

   return true;
}

bool AttachValueEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();

    HRESULT hr;
    CComBSTR tmpBstr;
    wstring wsEleOpt;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    try {
        he->get_tagName( &tmpBstr );
        std::wstring tagName( tmpBstr );
        std::transform(tagName.begin(), tagName.end(), tagName.begin(), ::tolower);
        if( tagName.compare(L"input")==0 ) {
            _bstr_t bstrBag = (_bstr_t)"value";
            variant_t tmpVariant = he->getAttribute(bstrBag, 0);
            string variantValue = (char*)(_bstr_t)tmpVariant;
            string newValue = oor.fireNext ? oor.eleOpt+variantValue : variantValue+oor.eleOpt;
            _variant_t var = _variant_t( newValue.c_str() );
            if( FAILED( hr=he->setAttribute(bstrBag, var, 0) ) )
                return false;
        } else {
            string oldInnerText = he->innerText;
            string newInnerText = oor.fireNext ? oor.eleOpt+oldInnerText : oldInnerText+oor.eleOpt;
            CComBSTR bs( newInnerText.c_str() );
            if( FAILED( hr=he->put_innerText(bs) ) )
             return false;
        }
    } catch (... ) {}

    return true;
}

bool SetFieldValueEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();
    
    HRESULT hr;
    CComBSTR tmpBstr;
    wstring wsEleOpt;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    wstring value;
    try {
        wsEleOpt = FB::utf8_to_wstring( oor.eleOpt );
        value = _ieEntity->getFromEntity( wsEleOpt );
        if( oor.fireNext ) {
			string str_value = FB::wstring_to_utf8( value );
            std::regex regexstr ("<[^>]*>");
			string fmt="";
            str_value=std::regex_replace(str_value,regexstr,fmt);
            value = FB::utf8_to_wstring( str_value );
        }
    } catch(FBB::Exception::FBBException& ) {
        value = L"";
    }

    he->get_tagName( &tmpBstr );
    std::wstring tagName( tmpBstr );
    std::transform(tagName.begin(), tagName.end(), tagName.begin(), ::tolower);
    if( tagName.compare(L"input")==0 ){
         _bstr_t bstrBag = (_bstr_t)"value";
         _variant_t var = _variant_t( value.c_str() );
         if( FAILED( hr=he->setAttribute(bstrBag, var, 0) ) )
             return false;
     } else {
         CComBSTR bs(value.c_str());
         if( FAILED( hr=he->put_innerText(bs) ) )
             return false;
     }

    return true;
}

bool SelectionDirectEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();

    HRESULT hr;
    wstring wsEleOpt;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    wsEleOpt = FB::utf8_to_wstring( oor.eleOpt );
    CComBSTR tmpBstr( wsEleOpt.c_str() );

    MSHTML::IHTMLElementCollectionPtr pColl = he->all;
    long len = pColl->length;
    MSHTML::IHTMLElementPtr hec = NULL;
    for(int i=0; i<len; i++) {
        _variant_t va(i);
        hec = static_cast<MSHTML::IHTMLElementPtr>( pColl->item(va) );

        _bstr_t bstrBag = (_bstr_t)"value";
        variant_t tmpVariant = hec->getAttribute(bstrBag, 0);
        string newValue = (char*)(_bstr_t)tmpVariant;
        if( newValue==oor.eleOpt ) {
            if( FAILED(hr = static_cast<MSHTML::IHTMLOptionElementPtr>(hec)->put_selected( VARIANT_TRUE )) )
                return false;
            break;
        }
    }

    if( oor.fireNext ) {
        MSHTML::IHTMLElementPtr hefire = NULL;
        CIEEvent* evobj = NULL;

        try {
           hefire = p_parser->FindElement(oor.changeEle);
           evobj=new CIEEvent();
           evobj->AttachEvent(hefire);
       } catch (FBB::Exception::FBBException& ) {

       }

       MSHTML::IHTMLElement3Ptr heo = static_cast<MSHTML::IHTMLElement3Ptr>(he);
       _bstr_t bstrBag = (_bstr_t)"onchange";
       heo->FireEvent(bstrBag);
       p_browser->checkDocumentComplete(WM_ELE_ONCHANGE);

       if( evobj!=NULL )
           delete evobj;
    }

    return true;
}

bool SelectionSequenceEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();

    HRESULT hr;
    CComBSTR tmpBstr;
    MSHTML::IHTMLElementPtr the;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    MSHTML::IHTMLElementCollectionPtr pColl = he->all;
    long len = pColl->length;

    long count = 0;
    MSHTML::IHTMLElementPtr optEle = NULL;
    for(int i=0; i<len; i++) {
        _variant_t va(i);
        the = static_cast<MSHTML::IHTMLElementPtr>( pColl->item(va) );

        the->get_tagName(&tmpBstr);
        std::wstring tagName( tmpBstr );
        std::transform(tagName.begin(), tagName.end(), tagName.begin(), ::tolower);
        if( tagName.compare(L"option")==0 ){
            count++;
            if( count==oor.selindex+1 ){
                optEle = the;
                break;
            }
        }
    }

    if( optEle!=NULL ) {
        // if( FAILED(hr = static_cast<MSHTML::IHTMLOptionElementPtr>(optEle)->get_value( &tmpBstr )) )
        //     return false;
        // if( FAILED( hr=static_cast<MSHTML::IHTMLSelectElementPtr>(he)->put_value( tmpBstr ) ) )
        //     return false;
        if( FAILED(hr = static_cast<MSHTML::IHTMLOptionElementPtr>(optEle)->put_selected( VARIANT_TRUE )) )
            return false;

        if( oor.fireNext ){
            MSHTML::IHTMLElementPtr hefire = NULL;
            CIEEvent* evobj = NULL;

            try {
               hefire = p_parser->FindElement(oor.changeEle);
               evobj=new CIEEvent();
               evobj->AttachEvent(hefire);
           } catch(FBB::Exception::FBBException& ) {

           }

           MSHTML::IHTMLElement3Ptr heo = static_cast<MSHTML::IHTMLElement3Ptr>(he);
           _bstr_t bstrBag = (_bstr_t)"onchange";
           heo->FireEvent(bstrBag);
           p_browser->checkDocumentComplete(WM_ELE_ONCHANGE);

           if( evobj!=NULL )
               delete evobj;
       }
   }

   return true;
}

const string CaptchaEvaluator::autoCaptchaModelDir = FB::System::getAppDataPath("zhendao\\deploytest\\1.0.0.0");

bool CaptchaEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();
    FBB::Helper::UtilClass* p_util = _ieEntity->getUtil();
    long siteID = _ieEntity->getSiteId();

    HRESULT hr;
    CComBSTR tmpBstr;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    MSHTML::IHTMLElementPtr captchaele = NULL;
    if( oor.captchaNeedActive ){
       MSHTML::IHTMLElementPtr captchainputele = he;
       try {
           p_browser->invokeMember(captchainputele, "focus", p_parser);
       } catch(...) {}

       p_browser->checkDocumentComplete(WM_NOMSG);
   }

    captchaele = p_parser->FindElement(oor.captureEle);
    if( p_parser->copyImage(oor.captureEle, captchaele)==E_FAIL )
        return false;
    string captchaUrl = _ieEntity->generateCaptchaFilePath(); 
    if( p_util->saveImageFromClipboard(captchaUrl)==false )
        return false;

    bool autoResult = false;
    wstring value;
    if( oor.autoCaptcha ) {
        string rootdir=CaptchaEvaluator::autoCaptchaModelDir;//安装路径
        FBB::Entity::CaptchaPara para;//验证码识别参数
        // bool re = GetCaptchaPara(siteID, oor.captchaType, para);//此处使用webservice获得
        bool re = FBB::WS::WSClientClass::GetCaptchaPara(siteID, oor.captchaType, para);
        if( re&&para.Valid ) {
            string captchaStr;
            autoResult = CaptchaPred(para, captchaUrl, rootdir, captchaStr);
            if( autoResult )
                value = FB::utf8_to_wstring( captchaStr );
        }
    }
    if( autoResult==false ) {
        boost::this_thread::interruption_point();
        _ieEntity->makeCaptchaCallback("CAPTCHA");
        boost::this_thread::interruption_point();
        while( _ieEntity->getCaptha()==L"" )
           boost::this_thread::sleep(boost::posix_time::seconds(1));
        value = _ieEntity->getCaptha();
        _ieEntity->resetCaptha();
    }
    _bstr_t bstrBag = (_bstr_t)"value";
    _variant_t var = _variant_t( value.c_str() );
    if( FAILED( hr=he->setAttribute(bstrBag, var, 0) ) )
        return false;

    return true;
}

bool UserEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();
    FBB::Helper::UtilClass* p_util = _ieEntity->getUtil();

    HRESULT hr;
    CComBSTR tmpBstr;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    long minLength = oor.usernamegenrule.minLength;
    long maxLength = oor.usernamegenrule.maxLength;
    bool FirstAlpha = oor.usernamegenrule.FirstAlpha;
    bool ContainNum = oor.usernamegenrule.ContainNum;
    bool lowercase = oor.usernamegenrule.lowercase;
    string value = p_util->generateUsername(minLength, maxLength, 8, FirstAlpha, ContainNum, lowercase);

    wstring wsValue = FB::utf8_to_wstring( value );
    _bstr_t bstrBag = (_bstr_t)"value";
    _variant_t var = _variant_t( wsValue.c_str() );
    if( FAILED( hr=he->setAttribute(bstrBag, var, 0) ) )
        return false;

    castToRegistrationClass(_ieEntity)->setLoginNameToRegPara( value );
    return true;
}

bool SetRegUserEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();

    HRESULT hr;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    // equal to string.IsNullOrEmpty
    string LoginName = castToRegistrationClass(_ieEntity)->getLoginNameFromRegPara();
    if( LoginName.empty() )
        return false;

    wstring wsValue = FB::utf8_to_wstring( LoginName );
    _bstr_t bstrBag = (_bstr_t)"value";
    _variant_t var = _variant_t( wsValue.c_str() );
    if( FAILED( hr=he->setAttribute(bstrBag, var, 0) ) )
        return false;
    else
        return true;
}

bool PassWordEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Helper::UtilClass* p_util = _ieEntity->getUtil();

    HRESULT hr;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    long minLength = oor.pwgenrule.minLength;
    long maxLength = oor.pwgenrule.maxLength;
    bool FirstAlpha = oor.pwgenrule.FirstAlpha;
    bool ContainNum = oor.pwgenrule.ContainNum;
    bool lowercase = oor.pwgenrule.lowercase;
    string value = p_util->generatePassword(minLength, maxLength, 8, FirstAlpha, ContainNum, lowercase);

    wstring wsValue = FB::utf8_to_wstring( value );
    _bstr_t bstrBag = (_bstr_t)"value";
    _variant_t var = _variant_t( wsValue.c_str() );
    if( FAILED( hr=he->setAttribute(bstrBag, var, 0) ) )
     return false;

    castToRegistrationClass(_ieEntity)->setLoginPwToRegPara( value );
    return true;
}

bool RepeatPassWordEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();

    HRESULT hr;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    wstring value = FB::utf8_to_wstring( castToRegistrationClass(_ieEntity)->getLoginPwFromRegPara() );
    _bstr_t bstrBag = (_bstr_t)"value";
    _variant_t var = _variant_t( value.c_str() );
    if( FAILED( hr=he->setAttribute(bstrBag, var, 0) ) )
     return false;
 else
    return true;
}

bool BuinessDirectionMapEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    int siteId = _ieEntity->getSiteId();
    map<long, FBB::Entity::BusinessDirectionPara> pageTypeBuiDirePara;
    FBB::WS::WSClientClass::loadBuiDireMap(siteId, pageTypeBuiDirePara);
    FBB::Helper::UtilClass* p_util = _ieEntity->getUtil();

    long BusinessDirectionID = -1;
    wstring wsIdStr = _ieEntity->getFromEntity(L"BusinessDirectionID");
    string idStr = FB::wstring_to_utf8( wsIdStr );
    BusinessDirectionID = FBB::Helper::UtilClass::charArrayToLong( idStr.c_str() );

    FBB::Entity::BusinessDirectionPara pdb = pageTypeBuiDirePara[oor.pageType];
    vector<FBB::Entity::BusinessDirePair> allbms = pdb.allbms;
    int po = -1;
    for(unsigned long i=0; i<allbms.size(); i++){
     if( allbms[i].mymode.id==BusinessDirectionID ) {
         po = i;
         break;
     }
 }
 if( po==-1 )
     return false;
 _ieEntity->EvaluateOneOptReg(allbms[po].oor);

 return true;
}

bool BuinessModeMapEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    int siteId = _ieEntity->getSiteId();
    map<long, FBB::Entity::BusinessModePara> pageTypeBuiModePara;
    FBB::WS::WSClientClass::loadBuiModeMap(siteId, pageTypeBuiModePara);
    FBB::Helper::UtilClass* p_util = _ieEntity->getUtil();

    long BusinessModeID = -1;
    wstring wsIdStr = _ieEntity->getFromEntity(L"BusinessModeID");
    string idStr = FB::wstring_to_utf8( wsIdStr );
    BusinessModeID = FBB::Helper::UtilClass::charArrayToLong( idStr.c_str() );

    FBB::Entity::BusinessModePara pdb = pageTypeBuiModePara[oor.pageType];
    vector<FBB::Entity::BusinessModePair> allbms = pdb.allbms;
    int po = -1;
    for(unsigned long i=0; i<allbms.size(); i++){
     if( allbms[i].mymode.id==BusinessModeID ) {
         po = i;
         break;
     }
 }
 if( po==-1 )
     return false;
 _ieEntity->EvaluateOneOptReg(allbms[po].oor);

 return true;
}

bool ProDirectoryMapEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Helper::UtilClass* p_util = _ieEntity->getUtil();
    long siteID = _ieEntity->getSiteId();

    wstring wsIdStr = _ieEntity->getFromEntity(L"BusinessIndustryID");
    long BIID = -1;
    string idStr = FB::wstring_to_utf8( wsIdStr );
    BIID = FBB::Helper::UtilClass::charArrayToLong( idStr.c_str() );

    FBB::Entity::ProductDirectoryPara proDirePara;
    if( !FBB::WS::WSClientClass::getProDireMapOptBySiteIDandPageType(siteID, oor.pageType, BIID, proDirePara) )
        return false;
    for(long i=proDirePara.allbms.size()-1; i>=0; i--){
        for(unsigned long j=0; j<proDirePara.allbms[i].oors.size(); j++)
            _ieEntity->EvaluateOneOptReg(proDirePara.allbms[i].oors[j]);
    }

    return true;
}

bool AreaMapEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Helper::UtilClass* p_util = _ieEntity->getUtil();
    long siteID = _ieEntity->getSiteId();

    wstring wsProvinceName = _ieEntity->getFromEntity(L"Province");
    wstring wsCityName = _ieEntity->getFromEntity(L"City");
    wstring wsCountryName = _ieEntity->getFromEntity(L"County");
    string provinceName = FB::wstring_to_utf8( wsProvinceName );
    string cityName = FB::wstring_to_utf8( wsCityName );
    string countryName = FB::wstring_to_utf8( wsCountryName );

    string CompanyLocated = CityInformation::getInstance()->GetLocated(provinceName, cityName, countryName);    
    if( CompanyLocated=="" )
        return false;

    FBB::Entity::AreaPara areaPara;
    if( !FBB::WS::WSClientClass::getAreaMapOptBySiteIDandPageType(siteID, oor.pageType, CompanyLocated, areaPara) )
        return false;
    for(long i=areaPara.allbms.size()-1; i>=0; i--)
        for(unsigned long j=0; j<areaPara.allbms[i].oors.size(); j++)
            _ieEntity->EvaluateOneOptReg(areaPara.allbms[i].oors[j]);

    return true;
}

bool FocusEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();

    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    if( !oor.fireNext ) {
        try {
          p_browser->invokeMember(he, "focus", p_parser);
        } catch(...) {

        }
    } else {
        MSHTML::IHTMLElementPtr hefire = NULL;
        CIEEvent* evobj = NULL;

        try{
            hefire = p_parser->FindElement(oor.changeEle);
            evobj=new CIEEvent();
            evobj->AttachEvent(hefire);
        } catch(FBB::Exception::FBBException& ) {

        }

        try {
            p_browser->invokeMember(he, "focus", p_parser);
            MSHTML::IHTMLElement3Ptr heo = static_cast<MSHTML::IHTMLElement3Ptr>(he);
            _bstr_t bstrBag = (_bstr_t)"onchange";
            heo->FireEvent(bstrBag);
        } catch(...) {

        }
        p_browser->checkDocumentComplete(WM_ELE_ONCHANGE);

        if( evobj!=NULL )
            delete evobj;
    }

    return true;
}

bool MobileCaptchaEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    long siteID = _ieEntity->getSiteId();

    HRESULT hr;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    boost::this_thread::interruption_point();
    _ieEntity->makeCaptchaCallback("MOBILECAPTCHA");
    // if( dynamic_cast<RegistrationClass*>(_ieEntity)!=NULL ) {
    //     RegistrationClass::_regCaptchaCallback->InvokeAsync("", FB::variant_list_of(siteID)("MOBILECAPTCHA");
    // } else {
    //     long PMID = dynamic_cast<PublishClass*>(_ieEntity)->getPMID();
    //     PublishClass::_publishCaptchaCallback->InvokeAsync("", FB::variant_list_of(siteID)(PMID)("MOBILECAPTCHA"));
    // }
    boost::this_thread::interruption_point();

    while( _ieEntity->getMobileCaptha()==L"" ) {
       boost::this_thread::sleep(boost::posix_time::seconds(1));
   }

   _bstr_t bstrBag = (_bstr_t)"value";
   wstring value = _ieEntity->getMobileCaptha();
   _ieEntity->resetMobileCaptcha();
   _variant_t var = _variant_t( value.c_str() );
   if( FAILED( hr=he->setAttribute(bstrBag, var, 0) ) )
       return false;

   return true;
}

bool ExtractUserNameEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

	HRESULT hr;
    string innerText;
	CComBSTR innerTextBSTR;
	if( SUCCEEDED(hr=he->get_innerText(&innerTextBSTR)) ) {
        if( innerTextBSTR!=NULL ) {
            wstring ws_innerText( innerTextBSTR );
            innerText = FB::wstring_to_utf8( ws_innerText );
        }

	}
    if( FAILED(hr=he->get_innerText(&innerTextBSTR)) || innerText.empty() ) {
        try {
            _variant_t tmpVariant = he->getAttribute(_bstr_t(L"value"), 0);
            innerText = (char*)(_bstr_t)tmpVariant;
        } catch (...) {
            innerText = "";
        }
    }

    castToRegistrationClass(_ieEntity)->setLoginNameToRegPara( innerText );

    return true;
}

bool QuestionCaptchaEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();
    long siteID = _ieEntity->getSiteId();

    HRESULT hr;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    MSHTML::IHTMLElementPtr queele = NULL;
    if( oor.captchaNeedActive ) {
       MSHTML::IHTMLElementPtr captchainputele = he;
       if( p_browser->invokeMember(captchainputele, "focus", p_parser)==false )
           return false;

       MSHTML::IHTMLDocument4Ptr document = static_cast<MSHTML::IHTMLDocument4Ptr>( p_parser->getDocumentPtr() );
       MSHTML::IHTMLElement3Ptr element = static_cast<MSHTML::IHTMLElement3Ptr>(captchainputele);
       MSHTML::IHTMLEventObjPtr eventObject = document->CreateEventObject();
       CComVariant var_eventObject;
       var_eventObject.pdispVal = eventObject;
       VARIANT_BOOL result = element->FireEvent(L"onmouseover", &var_eventObject);

       p_browser->checkDocumentComplete(WM_NOMSG);
   }
   queele = p_parser->FindElement(oor.captureEle);

   wstring strquest = queele->innerText;
   if( strquest.empty()||strquest==L"" )
    strquest = queele->innerHTML;

    vector<wstring> captchaInfoVector;
    captchaInfoVector.push_back(strquest);
    _ieEntity->setCaptchaQuestion( captchaInfoVector );

    boost::this_thread::interruption_point();
    _ieEntity->makeCaptchaCallback("QUESTIONCAPTCHA");
    // if( dynamic_cast<RegistrationClass*>(_ieEntity)!=NULL ) {
    //     RegistrationClass::_regCaptchaCallback->InvokeAsync("", FB::variant_list_of(siteID)("QUESTIONCAPTCHA")(captchaUrl));
    // } else {
    //     long PMID = dynamic_cast<PublishClass*>(_ieEntity)->getPMID();
    //     PublishClass::_publishCaptchaCallback->InvokeAsync("", FB::variant_list_of(siteID)(PMID)("QUESTIONCAPTCHA")(captchaUrl));
    // }
    boost::this_thread::interruption_point();

    while( _ieEntity->getQuestionCaptcha()==L"" ) {
        boost::this_thread::sleep(boost::posix_time::seconds(1));
    }

    _bstr_t bstrBag = (_bstr_t)"value";
    wstring value = _ieEntity->getQuestionCaptcha();
    _ieEntity->resetQuestionCaptcha();
    _variant_t var = _variant_t( value.c_str() );
    if( FAILED( hr=he->setAttribute(bstrBag, var, 0) ) )
        return false;

    return true;
}

bool QuestionCaptchaSelEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();
    long siteID = _ieEntity->getSiteId();

    HRESULT hr;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    MSHTML::IHTMLElementPtr queelesel = NULL;
    if( oor.captchaNeedActive ) {
     MSHTML::IHTMLElementPtr captchainputele = he;
     if( p_browser->invokeMember(captchainputele, "focus", p_parser)==false )
         return false;
     MSHTML::IHTMLDocument4Ptr document = static_cast<MSHTML::IHTMLDocument4Ptr>( p_parser->getDocumentPtr() );
     MSHTML::IHTMLElement3Ptr element = static_cast<MSHTML::IHTMLElement3Ptr>(captchainputele);
     MSHTML::IHTMLEventObjPtr eventObject = document->CreateEventObject();
     CComVariant var_eventObject;
     var_eventObject.pdispVal = eventObject;
     VARIANT_BOOL result = element->FireEvent(L"onmouseover", &var_eventObject);

     p_browser->checkDocumentComplete(WM_NOMSG);
    }

    vector<wstring> captchaInfoVector;
    queelesel = p_parser->FindElement(oor.captureEle);
    wstring strquestsel = queelesel->innerText;
    if( strquestsel.empty()||strquestsel==L"" )
        strquestsel = queelesel->innerHTML;
    captchaInfoVector.push_back( strquestsel );

    CComBSTR tmpBstr;
    MSHTML::IHTMLElementPtr the = NULL;
    MSHTML::IHTMLElementCollectionPtr pColl = he->all;
    long len = pColl->length;
    for(int i=0; i<len; i++){
        _variant_t va(i);
        the = static_cast<MSHTML::IHTMLElementPtr>( pColl->item(va) );

        the->get_tagName(&tmpBstr);
        std::wstring tagName( tmpBstr );
        std::transform(tagName.begin(), tagName.end(), tagName.begin(), ::tolower);
        if( tagName.compare(L"option")==0 ){
            wstring wsAnswer = the->innerText;
            captchaInfoVector.push_back( wsAnswer );
        }
    }
    _ieEntity->setCaptchaQuestion( captchaInfoVector );

    boost::this_thread::interruption_point();
    _ieEntity->makeCaptchaCallback("QUESTIONCAPTCHASEL");
    // if( dynamic_cast<RegistrationClass*>(_ieEntity)!=NULL ) {
    //     RegistrationClass::_regCaptchaCallback->InvokeAsync("", FB::variant_list_of(siteID)("QUESTIONCAPTCHASEL")(captchaUrl));
    // } else {
    //     long PMID = dynamic_cast<PublishClass*>(_ieEntity)->getPMID();
    //     PublishClass::_publishCaptchaCallback->InvokeAsync("", FB::variant_list_of(siteID)(PMID)("QUESTIONCAPTCHASEL")(captchaUrl));
    // }
    boost::this_thread::interruption_point();

    while( _ieEntity->getSelectIndex()==-1 ) {
        boost::this_thread::sleep(boost::posix_time::seconds(1));
    }

    _bstr_t bstrBag = (_bstr_t)"value";
    long selectIndex = _ieEntity->getSelectIndex();
    _ieEntity->resetSelectIndex();
    long countsel = 0;
    MSHTML::IHTMLElementPtr optElesel = NULL;
    pColl = he->all;
    len = pColl->length;
    for(int i=0; i<len; i++){
        _variant_t va(i);
        the = static_cast<MSHTML::IHTMLElementPtr>( pColl->item(va) );

        the->get_tagName(&tmpBstr);
        std::wstring tagName( tmpBstr );
        std::transform(tagName.begin(), tagName.end(), tagName.begin(), ::tolower);
        if( tagName.compare(L"option")==0 ){
            countsel++;
            if( countsel==selectIndex+1 ){
                optElesel = the;
                break;
            }
        }
    }

    if( optElesel!=NULL )
        if( FAILED(hr = static_cast<MSHTML::IHTMLOptionElementPtr>(optElesel)->put_selected( VARIANT_TRUE )) )
            return false;

    return true;
}

bool InvokeKeyPressEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser =_ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();

    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    MSHTML::IHTMLDocument4Ptr document = static_cast<MSHTML::IHTMLDocument4Ptr>( p_parser->getDocumentPtr() );
    MSHTML::IHTMLElement3Ptr element = static_cast<MSHTML::IHTMLElement3Ptr>(he);
    MSHTML::IHTMLEventObjPtr eventObject = document->CreateEventObject();
    eventObject->put_keyCode( ' ' );
    CComVariant var_eventObject;
    var_eventObject.pdispVal = eventObject;

    VARIANT_BOOL result = element->FireEvent(L"onkeydown", &var_eventObject);
    if( result==VARIANT_TRUE ){
     result = element->FireEvent(L"onkeypress", &var_eventObject);
     if( result==VARIANT_TRUE ){
         result = element->FireEvent(L"onkeyup", &var_eventObject);
         if( result==VARIANT_FALSE )
             return false;
     } else {
         return false;
     }
 } else {
     return false;
 }
 p_browser->checkDocumentComplete(WM_NOMSG);

 return true;
}

bool RedirectURLEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();

    if( oor.Url.empty() )
        return true;

    p_browser->navigate(oor.Url, p_parser);
    p_browser->checkDocumentComplete();
    return true;
}

bool UploadFileEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
	FBB::Parser::HtmlParserClass* p_parser =_ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();

    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    wstring wsEleOpt = FB::utf8_to_wstring( oor.eleOpt );
    wstring wsFileField = _ieEntity->getFromEntity( wsEleOpt );
    string fileField = FB::wstring_to_utf8( wsFileField );

    string fnameremote;
    string fnamelocal;
    getPath(fileField, oor.fileIndex, fnameremote, fnamelocal);
    localFilePath = fnamelocal;
    IEEntityClass::downloadCallback->Invoke("", FB::variant_list_of(fnameremote)(fnamelocal));

    while( FBB::Helper::UtilClass::isFileExist(localFilePath)==false );

    MSHTML::IHTMLDocument2Ptr doc2 = p_parser->getDocumentPtr();
    HWND hwnddoc;
    IOleWindowPtr olewin;
    HRESULT hr=doc2->QueryInterface(IID_IOleWindow,(void **)&olewin);
    if(FAILED(hr))
        return false;
    hr=olewin->GetWindow(&hwnddoc);
    if(FAILED(hr))
        return false;
    p_browser->invokeMember(he, "focus", p_parser);

    {
        boost::thread t1(&UploadFileEvaluator::SetFileThread, this, hwnddoc);
        t1.detach();        
        cout<<"begin wait 1"<<endl;

        // Sleep(100);
        while(!g_mutex.try_lock())
        {
            MSG msg;
            if(PeekMessage(&msg, NULL, 0, 0,0) > 0)
            {
              TranslateMessage(&msg);
              DispatchMessage(&msg);
            }
        }
        g_mutex.unlock();
        cout<<"end wait 1"<<endl;
    }

    return true;
}

void UploadFileEvaluator::getPath(
    string fileField, long fileIndex, string& fnameremote, string& fnamelocal) {
    string urlpre = FBB::Helper::UtilClass::getWebServiceDomain();
    // string localpath = FBB::Helper::UtilClass::getDLLPath();
    string localpath = FB::System::getTempPath();

    std::vector<string> fnamesplit = FBB::Helper::UtilClass::split(fileField, '|');
    fnameremote = ( fileIndex>=(long)fnamesplit.size() ) ?
                    fnamesplit[0] :
                    fnamesplit[fileIndex];
    fnamelocal = localpath + fnameremote;
    // from '/xx/yy' to '\xx\yy'
    // std::vector<string> pathSplit = FBB::Helper::UtilClass::split(fnamelocal, '/');
	vector<string> pathSplit; 
	boost::split(pathSplit, fnamelocal, boost::is_any_of ( "/\\" ), boost::token_compress_on);
    fnamelocal = pathSplit[0];
    for(unsigned int i=1; i<pathSplit.size(); i++)
        fnamelocal = fnamelocal + "\\" + pathSplit[i];

	fnameremote = urlpre + fnameremote;
}

BOOL CALLBACK UploadFileEvaluator::EnumChildProcOpen( HWND hwnd, LPARAM lParam )
{
	wstring titlestr=L"打开";
	WCHAR wstr[201];
	GetWindowTextW(hwnd,wstr,200);
	wstring wstrs(wstr);
	if(wstrs.find(titlestr)!=-1)
	{
		PostMessageW(hwnd,WM_KEYDOWN,VK_RETURN,0);
		return false;
	}
    return TRUE;
}

BOOL CALLBACK UploadFileEvaluator::EnumChildProc( HWND hwnd, LPARAM lParam )
{  
    UploadFileEvaluator* that = (UploadFileEvaluator*)lParam;
	string localFilePath = that->getLocalFilePath();
	for(int i=0;i<(int)localFilePath.length();i++)
	{
		PostMessageW(hwnd,WM_CHAR,(char)localFilePath[i],0);
	}
    return TRUE;
}

void UploadFileEvaluator::SetFileThread(HWND hwnd)
{
	static wstring titlestr=L"选择要加载的文件";//´ò¿ªÎÄ¼þ¶Ô»°¿òµÄ´°¿Ú±êÌâÈ«³Æ£¬×¢Òâ²»Í¬win²Ù×÷¿ÉÄÜ»á²»Ò»Ñù
	boost::lock_guard<boost::mutex> lg(g_mutex); //µÈ´ýÉèÖÃ»¥³âÁ¿	
	cout<<"testthread start"<<endl;
	// Sleep(200);
    PostMessageW(hwnd,WM_KEYDOWN,VK_SPACE,0);//·¢ËÍÏûÏ¢´ò¿ªÎÄ¼þ¶Ô»°¿ò
	HWND hwndchild=0;
	while(hwndchild==0)
	{
		hwndchild=FindWindowW(NULL,titlestr.c_str());
	}
	Sleep(100);	
	SetWindowPos(hwndchild,HWND_BOTTOM,0,0,0,0,SWP_HIDEWINDOW);	//Òþ²Ø´ò¿ªÎÄ¼þ¶Ô»°¿ò È¡ÏûÒþ²Ø¿É²é¿´Ð§¹û
	Sleep(3000);
		
	BOOL re=EnumChildWindows(hwndchild,EnumChildProc,(LPARAM)this);//ÉèÖÃÎÄ¼þÃû
	EnumChildWindows(hwndchild,EnumChildProcOpen,0);//¹Ø±Õ´ò¿ªÎÄ¼þ¶Ô»°¿ò
	Sleep(2000);//µÈ´ý¹Ø±ÕÍê³É	
	cout<<"testthread end"<<endl;	
}

bool Email_URLEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();

    wstring wsEmail = _ieEntity->getFromEntity(L"UserEmail");
    string email = FB::wstring_to_utf8( wsEmail );
    if( email.empty() )
        return false;
    if( oor.emailFind.empty()||oor.emailUrl.empty() )
        return false;

    COleDateTime dt;
    if( oor.emailTimeError!=-1 ) {
        COleDateTimeSpan ts(0, 0, -5, oor.emailTimeError);
        dt = COleDateTime::GetCurrentTime()+ts;
    }

    string turl;
    wstring wsPw = _ieEntity->getFromEntity(L"UserEmailPassword");
    string pw = FB::wstring_to_utf8( wsPw );

    string emsg = FindEmailURL(email, pw, oor.emailFind, oor.emailUrl, dt, turl, oor.emailTime);   
    if (!emsg.empty())//提取失败
        return false;

    regex reg("amp;");
    string replacestr="";
    turl=regex_replace(turl, reg, replacestr);
    p_browser->navigate(turl, p_parser);
    p_browser->checkDocumentComplete();
    return true;
}

string Email_URLEvaluator::FindEmailURL(
    string email, 
    string pw, 
    FBB::Entity::EmailFindRule erule, 
    FBB::Entity::EmailURLRule urlrule, 
    COleDateTime dt, 
    string& turl,
    long waittime)
{
    int loop = 3;
    int looptime = waittime/loop;
    if (looptime < 5)
        looptime = 5;
    turl = "";
    vector<string> splitStr =FabubaoMail::SplitString(email,"@");
    if ((int)splitStr.size() != 2)
    {
        return "邮箱格式错误";               
    }
    string userName = splitStr[0];
    string domainName = splitStr[1];
    FBB::Entity::EmailSite es;
    // bool re=GetEmailSite(domainName,es);//插件段 此处用webService获得
    bool re = FBB::WS::WSClientClass::GetEmailSite(domainName, es);
    if (!re)
        return "邮箱参数未设置";
    bool find = false;
    for (int i = 0; i < loop; i++)
    {
        //循环查找邮件
        FabubaoMail::CPop3 emailClient;
        if(!emailClient.Create(es.Pop3.c_str(),es.Port) )
            return "创建Pop3失败";      
        emailClient.SetTimeOut(3000);        
        try
        {
            if(!emailClient.Connect(userName.c_str() , pw.c_str() , es.UseSSL) ) {
				emailClient.Close();
				return "连接服务器失败";
			}
            vector<int> ids;    
            bool tre = !!emailClient.GetMailList(ids);
            if (tre)
            {
                for (int j = ids.size()-1; j >=0&&j>=((int)ids.size()-50); j--)
                {
                    try
                    {
                        FabubaoMail::RxMailMessage mailMsg;
                        if (emailClient.GetMail(ids[j], mailMsg))
                        {
                            if (MatchEmail(mailMsg, erule, dt))
                            {
                                if (GetUrlFromMail(mailMsg, urlrule, turl))
                                {
                                    find = true;
                                    //emailClient.DeleteMail(ids[j]); //删除邮件 测试时先不删除 集成到插件里面取消注释
                                    break;
                                }
                            }
                        }
                    }
                    catch(...) { }
                }
            }
            emailClient.Close();
        }
        catch(...) { 
			emailClient.Close();
		}

        if (find)
            break;
        Sleep(looptime*1000);    
    }
    if (find)
        return "";
    else
        return "提取邮箱URL失败";
}

bool Email_URLEvaluator::MatchEmail(FabubaoMail::RxMailMessage& mailMsg, FBB::Entity::EmailFindRule& erule, COleDateTime dt) {
    COleDateTime dd(mailMsg.DeliveryDate);
    if (dd < dt)
        return false;
    if (mailMsg.From.find(erule.fromStr)==-1)
        return false;
    if (mailMsg.Subject.find(erule.subjectStr)==-1)
        return false;
    return true;
}
    
bool Email_URLEvaluator::GetUrlFromMail(FabubaoMail::RxMailMessage& mailMsg, FBB::Entity::EmailURLRule& urlrule, string& turl) {
    turl = "";
    // regex reg ("<a(\\s|\\S)*href=[\"|\']?([^\"\']*)[\"|\']?[\\s|>](\\s|\\S)*" + urlrule.urlStr);
    regex reg ("<a(\\s|\\S)*href=[\"|\']?[\\s]*([^\"\'\\s]*)[\\s]*[\"|\']?[\\s|>](\\s|\\S)*" + urlrule.urlStr);
    if (!(mailMsg.Body.empty()))
    {    
        std:: match_results<std::string::const_iterator> result;
        if(regex_search(mailMsg.Body,result,reg))        
        {
            turl=result[2];
            return true;
        }
    }   
    {        
        for(int i=0;i<(int)mailMsg.Entities.size();i++)
        {
            FabubaoMail::RxMailMessage child=mailMsg.Entities[i];
            if (GetUrlFromMail(child, urlrule, turl))
                return true;
            // if (child.Body.empty())
            //     continue;
            // std:: match_results<std::string::const_iterator> result;
            // if(regex_search(child.Body,result,reg))        
            // {
            //     turl=result[1];
            //     return true;
            // }            
        }
    }
    return false;
}

bool WaitReadyEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();
    bool emptymsg = false;
    p_browser->WaitMsg(WM_NOMSG,oor.selindex*1000);
    return true;
}

// almost the same as CaptchaEvaluator, except for how to deal with 'value'
bool RecordCaptchaEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    FBB::Browser::BrowserClass* p_browser = _ieEntity->getBrowser();
    FBB::Helper::UtilClass* p_util = _ieEntity->getUtil();
    long siteID = _ieEntity->getSiteId();

    CComBSTR tmpBstr;
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);

    MSHTML::IHTMLElementPtr captchaele = NULL;
    if( oor.captchaNeedActive ){
       MSHTML::IHTMLElementPtr captchainputele = he;
       try {
           p_browser->invokeMember(captchainputele, "focus", p_parser);
       } catch(...) {}

       p_browser->checkDocumentComplete(WM_NOMSG);
   }

    captchaele = p_parser->FindElement(oor.captureEle);
    if( p_parser->copyImage(oor.captureEle, captchaele)==E_FAIL )
        return false;
    string captchaUrl = _ieEntity->generateCaptchaFilePath(); 
    if( p_util->saveImageFromClipboard(captchaUrl)==false )
        return false;

    bool autoResult = false;
    wstring value;
    if( oor.autoCaptcha ) {
        string rootdir=CaptchaEvaluator::autoCaptchaModelDir;//安装路径
        FBB::Entity::CaptchaPara para;//验证码识别参数
        bool re = FBB::WS::WSClientClass::GetCaptchaPara(siteID, oor.captchaType, para);
        if( re&&para.Valid ) {
            string captchaStr;
            autoResult = CaptchaPred(para, captchaUrl, rootdir, captchaStr);
            if( autoResult )
                value = FB::utf8_to_wstring( captchaStr );
        }
    }
    if( autoResult==false ) {
        boost::this_thread::interruption_point();
        _ieEntity->makeCaptchaCallback("CAPTCHA");
        boost::this_thread::interruption_point();
        while( _ieEntity->getCaptha()==L"" )
           boost::this_thread::sleep(boost::posix_time::seconds(1));
        value = _ieEntity->getCaptha();
        _ieEntity->resetCaptha();
    }
    _ieEntity->setCaptchaStr( FB::wstring_to_utf8(value) );

    return true;
}

bool RepeatCaptchaEvaluator::evaluate(FBB::Entity::OneOptRegEntity oor) {
    FBB::Parser::HtmlParserClass* p_parser = _ieEntity->getParser();
    MSHTML::IHTMLElementPtr he = p_parser->FindElement(oor.domEle);  
    try {
        HRESULT hr;
        string value = _ieEntity->getCaptchaStr();
        wstring wsValue = FB::utf8_to_wstring(value);
        _bstr_t bstrBag = (_bstr_t)"value";
        _variant_t var = _variant_t( wsValue.c_str() );
        if( FAILED( hr=he->setAttribute(bstrBag, var, 0) ) )
            return false;

        MSHTML::IHTMLElement3Ptr heo = static_cast<MSHTML::IHTMLElement3Ptr>(he);
        bstrBag = (_bstr_t)"onchange";
        heo->FireEvent(bstrBag);
    } catch(...) {}
    return true;
}
