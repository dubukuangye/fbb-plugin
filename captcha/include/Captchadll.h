/*********************************************
 * 所有导出dll 接口声明
 *********************************************/
#include <stdlib.h>
#ifndef _CAPTCHA_H
#define _CAPTCHA_H
#include <string>

using namespace std;
#define DLL _declspec(dllimport)

typedef struct{
	int row,col;
	unsigned char * pmdata;
}PMGrayWrapper;

//DLL bool _cdecl TestPmData(unsigned char **pmdata);
//公共操作 ID以0开始
extern "C" {
DLL bool _cdecl Captcha(unsigned long*  pmdata,int row, int col,int siteID,wchar_t *restr);//000

DLL bool _cdecl DLL_Color2Gray(unsigned long* pmdata,int row,int col,unsigned char * newpm); //001
DLL bool _cdecl DLL_RemoveFrame(unsigned char* pmdata,int row,int col);//002
DLL bool _cdecl DLL_ZoomPM(unsigned char** pmdata,int row,int col);//003

//二值化操作导出 1 操作ID以1开始
DLL bool _cdecl DLL_TwoValuedOTSU(unsigned char* pmdata,int row,int col); //101
DLL bool _cdecl DLL_TwoValuedOTSU_Fixed(unsigned char* pmdata,int row,int col,int thresh);//102

//去噪 2
DLL bool _cdecl DLL_ProduceConRegion(unsigned char* pmdata,int row,int col,int cri_size);//201
DLL bool _cdecl DLL_ProduceConRegion_Reverse(unsigned char* pmdata,int row,int col,int cri_size);//202
DLL bool _cdecl DLL_Correction_TemplateChangeBlack(unsigned char* pmdata,int row,int col);//203
DLL bool _cdecl DLL_Correction_TemplateChangeWhite(unsigned char* pmdata,int row,int col);//204
DLL bool _cdecl DLL_DilationPM(unsigned char* pmdata,int row,int col);//205
DLL bool _cdecl DLL_RuiHua_Gaosi(unsigned char* pmdata,int row,int col);//206
//细化 3
DLL bool _cdecl DLL_BackbonePM(unsigned char* pmdata,int row,int col);//301
DLL bool _cdecl DLL_ZoomBackbonePM(unsigned char** pmdata,int row,int col);//302

//切分 4
DLL bool _cdecl DLL_GeneralPartConSplit(unsigned char* pmdata,int row,int col,int modeltype,int charnum,PMGrayWrapper** cuts,int& cutnum,wchar_t* rootdir);//401

//识别 5
DLL bool _cdecl DLL_PredictPM(PMGrayWrapper* cuts,int numcuts,int modeltype,wchar_t** restr,wchar_t* rootdir);//501

DLL int _cdecl TestStr(wchar_t* str);
DLL int _cdecl TestStr2(char* str);
DLL void _cdecl TestTemp(wchar_t **str);
}

#endif