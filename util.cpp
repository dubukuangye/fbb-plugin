#include "util.h"
#include "utf8_tools.h"

/*
    string to time_t
    时间格式  2009-3-24
*/
int FBB::Helper::UtilClass::stringToTime(const string &strDateStr,time_t &timeData)
{
    char *pBeginPos = (char*) strDateStr.c_str();
    char *pPos = strstr(pBeginPos,"-");
    if(pPos == NULL)
    {
        return -1;
    }
    int iYear = atoi(pBeginPos);
    int iMonth = atoi(pPos + 1);
 
    pPos = strstr(pPos + 1,"-");
    if(pPos == NULL)
    {
        return -1;
    }
 
    int iDay = atoi(pPos + 1);
 
    struct tm sourcedate;
	memset((void*)&sourcedate, 0, sizeof(sourcedate));
    sourcedate.tm_mday = iDay;
    sourcedate.tm_mon = iMonth - 1; 
    sourcedate.tm_year = iYear - 1900;
 
    timeData = mktime(&sourcedate);  
 
    return 0;
}
 
/*
    time_t to string
*/
int FBB::Helper::UtilClass::timeToString(string &strDateStr,const time_t &timeData)
{
    char chTmp[15];
    memset(chTmp, 0, sizeof(chTmp));
 
    struct tm *p;
    p = localtime(&timeData);
 
	if( p==NULL ) {
		strDateStr = "";
	} else {
		p->tm_year = p->tm_year + 1900;
		p->tm_mon = p->tm_mon + 1;
		_snprintf(chTmp,sizeof(chTmp),"%04d-%02d-%02d",
			p->tm_year, p->tm_mon, p->tm_mday);
		strDateStr = chTmp;
	}
    return 0;
}

string FBB::Helper::UtilClass::generateValidValue(string value, long minLength, long maxLength){
	string validValue = value;
	long length = value.length();

	if( minLength==0&&maxLength==0 ){ return validValue; }

	if( length<minLength ){
		long currentLength = length;
		while( currentLength<minLength ){
			currentLength *= 2;
			validValue += validValue;
		}
		if( currentLength>maxLength ){
			long eraseStartPosition = maxLength;
			long uselessLength = currentLength-maxLength;
			validValue.erase(eraseStartPosition, uselessLength);
		}
		return validValue;
	} else {

		if( length<=maxLength ){ return validValue; }
		else{
			long eraseStartPosition = maxLength;
			long uselessLength = length-maxLength;
			validValue.erase(eraseStartPosition, uselessLength);
			return validValue;
		}

	}
}

string FBB::Helper::UtilClass::generateUsername(
	long minLength, long maxLength, long baseLength, 
	bool firstAlpha, bool containNum, bool lowercase) 
{
	int index = -1;
	string username = "";
	const string alphanum =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";
	
	// decide the length of the username
	long length = baseLength;
	while( length<minLength )
		length *= 2;
	if( length>maxLength )
		length = maxLength;

	// generate first letter if is alpha
	srand((unsigned)time(NULL)+rand());
	
	long start = 0;
	if( firstAlpha ) {
		index = rand()%52+10; // 生成10~61之间的随机数
		username += alphanum[index];
		start++;
	}
	if( containNum ) {
		index = rand()%10; // 生成0~9之间的随机数
		username += alphanum[index];
		start++;
	}

	for(int i=start; i<length; i++){
		index = rand()%62; // 生成0~61之间的随机数
		username += alphanum[index]; 
	}

	if( lowercase )
		std::transform(username.begin(), username.end(), username.begin(), ::tolower);

	return username;
}

string FBB::Helper::UtilClass::generatePassword(
	long minLength, long maxLength, long baseLength, 
	bool firstAlpha, bool containNum, bool lowercase)
{
	int index = -1;
	string password = "";
	const string alphanum =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";
	
	// decide the length of the password
	long length = baseLength;
	while( length<minLength ) 
		length *= 2;
	if( length>maxLength ){ length = maxLength; }

	// generate first letter if is alpha
	srand((unsigned)time(NULL)+rand());
	
	long start = 0;
	if( firstAlpha ){
		index = rand()%52+10; // 生成10~61之间的随机数
		password += alphanum[index];
		start++;
	}
	if( containNum ){
		index = rand()%10; // 生成0~9之间的随机数
		password += alphanum[index];
		start++;
	}

	for(int i=start; i<length; i++){
		index = rand()%62; // 生成0~61之间的随机数
		password += alphanum[index]; 
	}

	if( lowercase )
		std::transform(password.begin(), password.end(), password.begin(), ::tolower);

	return password;
}

bool FBB::Helper::UtilClass::saveBitmapToFile(HBITMAP hBitmap, LPCWSTR lpFileName)
	//hBitmap 为刚才的屏幕位图句柄

{      //lpFileName 为位图文件名
	HDC   hDC;         
	//设备描述表

	int   iBits;      
	//当前显示分辨率下每个像素所占字节数
	WORD  wBitCount = 16;   
	//位图中每个像素所占字节数
	//定义调色板大小， 位图中像素字节大小 ，
	// 位图文件大小 ， 写入文件字节数
	DWORD dwPaletteSize=0,dwBmBitsSize,dwDIBSize, dwWritten;
	BITMAP  Bitmap;        
	//位图属性结构
	BITMAPFILEHEADER   bmfHdr;        
	//位图文件头结构
	BITMAPINFOHEADER   bi;            
	//位图信息头结构 
	LPBITMAPINFOHEADER lpbi;          
	//指向位图信息头结构
	HANDLE  fh, hDib, hPal,hOldPal=NULL;
	//定义文件，分配内存句柄，调色板句柄

	//计算位图文件每个像素所占字节数
	hDC = CreateDC(L"DISPLAY",NULL,NULL,NULL);
	iBits=GetDeviceCaps(hDC,BITSPIXEL)*GetDeviceCaps(hDC,PLANES);
	DeleteDC(hDC);
	if (iBits <= 1)
		wBitCount = 1;
	else if (iBits <= 4)
		wBitCount = 4;
	else if (iBits <= 8)
		wBitCount = 8;
	else if (iBits <=24)
		wBitCount = 24;
	//计算调色板大小
	if (wBitCount <= 8)
		dwPaletteSize = (1 << wBitCount) *sizeof(RGBQUAD);

	//设置位图信息头结构
	GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&Bitmap);
	bi.biSize            = sizeof(BITMAPINFOHEADER);
	bi.biWidth           = Bitmap.bmWidth;
	bi.biHeight          = Bitmap.bmHeight;
	bi.biPlanes          = 1;
	bi.biBitCount          = wBitCount;
	bi.biCompression       = BI_RGB;
	bi.biSizeImage         = 0;
	bi.biXPelsPerMeter     = 0;
	bi.biYPelsPerMeter     = 0;
	bi.biClrUsed           = 0;
	bi.biClrImportant      = 0;

	dwBmBitsSize=((Bitmap.bmWidth*wBitCount+31)/32)* 4*Bitmap.bmHeight ;
	//为位图内容分配内存
	hDib=GlobalAlloc(GHND,dwBmBitsSize+dwPaletteSize+sizeof(BITMAPINFOHEADER));
	lpbi=(LPBITMAPINFOHEADER)GlobalLock(hDib);
	*lpbi=bi;
	// 处理调色板   
	hPal=GetStockObject(DEFAULT_PALETTE);
	if (hPal)
	{
		hDC  = GetDC(NULL);
		hOldPal = (HANDLE)SelectPalette(hDC, (HPALETTE)hPal, FALSE);
		RealizePalette(hDC);
	}
	// 获取该调色板下新的像素值
	GetDIBits(hDC, hBitmap, 0, (UINT) Bitmap.bmHeight,
		(LPSTR)lpbi + sizeof(BITMAPINFOHEADER)
		+dwPaletteSize,
		(BITMAPINFO *)
		lpbi, DIB_RGB_COLORS);
	//恢复调色板   
	if (hOldPal)
	{
		SelectPalette(hDC, (HPALETTE)hOldPal, TRUE);
		RealizePalette(hDC);
		ReleaseDC(NULL, hDC);
	}
	//创建位图文件    
	fh = CreateFile(lpFileName, GENERIC_WRITE, 
		0, NULL, CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	if (fh == INVALID_HANDLE_VALUE)
		return FALSE;
	// 设置位图文件头
	bmfHdr.bfType = 0x4D42;  // "BM"
	dwDIBSize    = sizeof(BITMAPFILEHEADER) 
		+ sizeof(BITMAPINFOHEADER)
		+ dwPaletteSize + dwBmBitsSize;  
	bmfHdr.bfSize = dwDIBSize;
	bmfHdr.bfReserved1 = 0;
	bmfHdr.bfReserved2 = 0;
	bmfHdr.bfOffBits = (DWORD)sizeof
		(BITMAPFILEHEADER) 
		+ (DWORD)sizeof(BITMAPINFOHEADER)
		+ dwPaletteSize;
	// 写入位图文件头
	WriteFile(fh, (LPSTR)&bmfHdr, sizeof
		(BITMAPFILEHEADER), &dwWritten, NULL);
	// 写入位图文件其余内容
	WriteFile(fh, (LPSTR)lpbi, dwDIBSize, 
		&dwWritten, NULL);
	//清除   
	GlobalUnlock(hDib);
	GlobalFree(hDib);
	CloseHandle(fh);
	// delete lpFileName;

	return true;
}

bool FBB::Helper::UtilClass::saveImageFromClipboard(string savePlace){ 
	OpenClipboard(NULL);
	HBITMAP hBitmap = (HBITMAP)GetClipboardData(CF_BITMAP);
	if( hBitmap==NULL ) return false;

	wstring wsSavePlace = FB::utf8_to_wstring( savePlace );
	saveBitmapToFile(hBitmap, wsSavePlace.c_str());

	CloseClipboard();

	return true;
}

std::vector<std::string>& FBB::Helper::UtilClass::split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<string> FBB::Helper::UtilClass::split(const string& s, char delim) {
	std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

string FBB::Helper::UtilClass::getWebServiceDomain() {
	return "http://e.88360.com";
}

string FBB::Helper::UtilClass::getDLLPath() {
	// return g_dllPath;
	return "D:";
}

void FBB::Helper::UtilClass::createDirectoryByPath(const string& path) {
	// std::vector<string> pathSplit = FBB::Helper::UtilClass::split(path, '/');
	vector<string> pathSplit; 
	boost::split(pathSplit, path, boost::is_any_of ( "/\\" ), boost::token_compress_on);
	string currentDirectory = pathSplit[0];

	for(unsigned int i=1; i<pathSplit.size()-1; i++) {
		currentDirectory = currentDirectory + "\\" + pathSplit[i];
		CreateDirectoryA(currentDirectory.c_str(), NULL);
	}
}

bool FBB::Helper::UtilClass::isFileExist(const string& filepath) {
	bool fileExist = true;

	ifstream fp(filepath);
	if( !fp )
		fileExist = false;
	fp.close();

	return fileExist;
}

