#pragma once

#include <string>
#include "soapws_USCOREInterfaceSoapProxy.h"
#include "soapWebServiceFabuParaSoapProxy.h"
#include <vector>
#include <map>
#include "regpara.h"
#include "deploytestAPI.h"
#include "cityinformation.h"

using namespace std;

struct CITYINFO;

/*
 * 		  	 written by Guo Qidong
 *
 * map gsoap structure with custom defined structure
 *  	 ugly code with a lot of repeatness
 *  		    need to improve
 *
 */


namespace FBB{
	namespace WS{
		class WSClientClass
		{
		public:
			WSClientClass(){}
			~WSClientClass(){}

			static bool loadSiteRegPara(int siteId, FBB::Entity::RegParaEntity& regPara);
			static bool loadSiteRegPara(vector<int> siteIds, map<long, FBB::Entity::RegParaEntity>& siteRegParaMap);
			
			static bool loadSitePubPara(int siteId, FBB::Entity::PublishParaEntity& pubPara);
			static bool loadSitePubPara(vector<int> siteIds, map<long, FBB::Entity::PublishParaEntity>& sitePubParaMap);
			
			static bool loadPublishEntity(long PMID, long SiteID, map<wstring, wstring>& publishEntityInfoMap);
			static bool loadUserInformation(long userId, map<wstring, wstring>& userInfoMap);
			
			static bool loadBuiDireMap(int siteId, map<long, FBB::Entity::BusinessDirectionPara>& pageBuiDireMap);
			static bool loadBuiDireMap(vector<int> siteIds, map<long, map<long, FBB::Entity::BusinessDirectionPara> >& siteBuiDireMap);

			static bool loadBuiModeMap(int siteId, map<long, FBB::Entity::BusinessModePara>& pageBuiModeMap);
			static bool loadBuiModeMap(vector<int> siteIds, map<long, map<long, FBB::Entity::BusinessModePara> >& siteBuiModeMap);
			
			bool loadProDireMap(vector<int> siteIds, map<long, map<long, FBB::Entity::ProductDirectoryPara> >& siteProDireMap);
			bool loadAreaMap(vector<int> siteIds, map<long, map<long, FBB::Entity::AreaPara> >& areaMap);
			bool getAllCityInformation(map<string, CITYINFO>& idCityInfoMap);
			static bool getAreaMapOptBySiteIDandPageType(long siteId, long pageType, string companyLocated, FBB::Entity::AreaPara& areaPara);
			static bool getProDireMapOptBySiteIDandPageType(long siteId, long pageType, long BIID, FBB::Entity::ProductDirectoryPara& proDirePara);

			bool saveRegistInfo(int userId, int siteId, int regStatus, string LoginName, string LoginPw);

			static bool GetEmailSite(string domainName, FBB::Entity::EmailSite& es);
			static bool GetCaptchaPara(long SiteID, long CaptchaType, FBB::Entity::CaptchaPara& para);

			static long CaptchaOptTypeMap[15];
		};
	}
}