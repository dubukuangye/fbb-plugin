#include <string>
#include <sstream>
#include <iostream>
#include <boost/weak_ptr.hpp>
#include "JSAPIAuto.h"
#include "BrowserHost.h"
#include "deploytest.h"
#include "SimpleStreamHelper.h"

/*********************Add by Guo**********
******************************************/
#include "wsClient.h"
#include "browser.h"
#include "regpara.h"
#include "htmlparser.h"
#include "util.h"
#include "fbbException.h"
#include "fbbcore.h"
#include "userinformation.h"
#include "cityinformation.h"
#include "thread.h"

#include "FBControl.h"

#include <set>
#include <map>
#include <list>
#include <vector>
#include <algorithm>

#ifndef H_deploytestAPI
#define H_deploytestAPI

class Evaluator;
class CityInformation;
class RegistrationClass;
class PublishClass;
class ThreadClass;

class PublishKey {
public:
	long _siteId;
	long _PMID;
	PublishKey(long siteId, long PMID) : _siteId(siteId), _PMID(PMID)
	{}

	bool operator<(const PublishKey& key) const {
		return _siteId>key._siteId ? true
				: _PMID>key._PMID ? true 
				: false;
	}
};

class deploytestAPI : public FB::JSAPIAuto
{
private:
	boost::shared_ptr<boost::thread> m_regThread;
	boost::shared_ptr<boost::thread> m_publishThread;
	boost::shared_ptr<boost::thread> m_stopRegThread;
	boost::shared_ptr<boost::thread> m_stopPublishThread;
	boost::shared_ptr<boost::thread> m_downloadThread;

    deploytestWeakPtr m_plugin;
    FB::BrowserHostPtr m_host;

	map<long, RegistrationClass* > siteRegistrationMap; // one site, one registration class
	map<PublishKey, PublishClass* > sitePublishMap; // one site and PMID, one publish class
	map<long, ThreadClass* > siteRegThreadMap; // one site, one registration class
	map<PublishKey, ThreadClass* > sitePublishThreadMap; // one site and PMID, one publish class

	FB::JSObjectPtr _batchFinishCallback;

	void downloadFromUrl(string, string);
public:
    deploytestAPI(const deploytestPtr& plugin, const FB::BrowserHostPtr& host) :
        m_plugin(plugin), m_host(host)
    {	
		FBLOG_INFO("guo", "path is " << g_dllPath );
		m_regThread = boost::shared_ptr<boost::thread>(new boost::thread());  
		m_publishThread = boost::shared_ptr<boost::thread>(new boost::thread());

        // Read-only property
        registerProperty("version", make_property(this, &deploytestAPI::get_version));

		registerMethod("initializeFBB", make_method(this, &deploytestAPI::initializeFBB));
		registerMethod("registerInBatch", make_method(this, &deploytestAPI::registerInBatch));
		registerMethod("publishInBatch", make_method(this, &deploytestAPI::publishInBatch));
		registerMethod("stopRegisterOrPublish", make_method(this, &deploytestAPI::stopRegisterOrPublish));
		
		registerMethod("setRegCaptchaAnswer", make_method(this, &deploytestAPI::setRegCaptchaAnswer));
		registerMethod("getRegCaptchaQuestion", make_method(this, &deploytestAPI::getRegCaptchaQuestion));
		
		registerMethod("setPublishCaptchaAnswer", make_method(this, &deploytestAPI::setPublishCaptchaAnswer));
		registerMethod("getPublishCaptchaQuestion", make_method(this, &deploytestAPI::getPublishCaptchaQuestion));

		registerMethod("beginDownload", make_method(this, &deploytestAPI::beginDownload));
    }
    virtual ~deploytestAPI();

    deploytestPtr getPlugin();
    std::string get_version();

   	/**
   	 * [initializeFBB description]
   	 * 初始化插件
   	 * 主要提供了用户id，javascript回调函数名
   	 * @param  userId
   	 * @param  regErrorCallback			注册错误回调
   	 * @param  regCompleteCallback		注册到完成步了的回调
   	 * @param  regFinishCallback		注册完全结束的回调
   	 * @param  regCaptchaCallback		注册时验证码的回调
   	 * @param  batchFinishCallback 		批执行结束，注册或者发布
   	 * @param  publishErrorCallback
   	 * @param  publishCompleteCallback
   	 * @param  publishCaptchaCallback
   	 * @param  downloadCallback			下载的回调
   	 * @return
   	 */
	bool initializeFBB(
		 long userId
		,const FB::JSObjectPtr& regErrorCallback
		,const FB::JSObjectPtr& regCompleteCallback
		,const FB::JSObjectPtr& regFinishCallback
		,const FB::JSObjectPtr& regCaptchaCallback
		,const FB::JSObjectPtr& batchFinishCallback
		,const FB::JSObjectPtr& publishErrorCallback
    	,const FB::JSObjectPtr& publishCompleteCallback
    	,const FB::JSObjectPtr& publishCaptchaCallback
		,const FB::JSObjectPtr& downloadCallback
	);

	void stopRegisterOrPublish(const string&);
	void stopRegister_thread();
	void stopPublish_thread();

	bool registerInBatch(const vector<long>& siteIds);
	void registerInBatch_thread(const vector<long>& siteIds);

	/**
	 * [publishInBatch description]
	 * @param  siteIds 	站点id
	 * @param  PMIDs	商情id
	 * @return
	 */
	bool publishInBatch(const vector<long>& siteIds, const std::vector<long>& PMIDs);
	void publishInBatch_thread(const vector<long>& siteIds, const std::vector<long>& PMIDs);
	
	vector<wstring> getRegCaptchaQuestion(long siteId, const string& capthcaKind);
	void setRegCaptchaAnswer(long siteId, const string& capthcaKind, const wstring& answer);

	vector<wstring> getPublishCaptchaQuestion(long siteId, long PMID, const string& capthcaKind);
	void setPublishCaptchaAnswer(long siteId, long PMID, const string& capthcaKind, const wstring& answer);

	void beginDownload(const string& fnameremote, const string& fnamelocal);
	void beginDownload_thread(const string& fnameremote, const string& fnamelocal);
};

#endif // H_deploytestAPI
