#pragma once

#include "boost/thread.hpp"
#include "fbbcore.h"

class IEEntityClass;

class ThreadClass {
public:
	ThreadClass(IEEntityClass*, string);
	~ThreadClass();

	static void interruptRegThreadGroup() {
		_regThreadGroup->interrupt_all();
	}
	static void waitUtillRegThreadsStop() {
		_regThreadGroup->join_all();
	}

	static void interruptPublishThreadGroup() {
		_publishThreadGroup->interrupt_all();
	}
	static void waitUtillPublishThreadsStop() {
		_publishThreadGroup->join_all();
	}
private:
	void excute(IEEntityClass* );

private:
	static boost::shared_ptr<boost::thread_group> _regThreadGroup;
	static boost::shared_ptr<boost::thread_group> _publishThreadGroup;
	boost::thread* _thread;
};