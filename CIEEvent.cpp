#include "cieevent.h"
#include <ExDispID.h>
#include <ExDisp.h>
#include <MsHtmdid.h>

CIEEvent::CIEEvent()
{
	m_dwRef=0;
	m_nPageCounter=0;	
	mCookie=0;
	mCookieEle=0;
	spCP=NULL;
	spCPEle=NULL;
	ele=NULL;
	ie=NULL;
}
CIEEvent::~CIEEvent(void)
{
	if(spCP!=NULL)
	{
		spCP->Unadvise(mCookie);
		spCP->Release();
	}
	if(spCPEle!=NULL)
	{
		spCPEle->Unadvise(mCookieEle);
		spCPEle->Release();
	}
}
bool CIEEvent::AttachEvent(SHDocVw::IWebBrowser2Ptr tie)
{
	this->ie=tie;
	if(tie==NULL)
		return false;
	HRESULT hr;
	IConnectionPointContainer* spCPC=NULL;		
	// IConnectionPoint * spCP=NULL;	
	if(SUCCEEDED(hr=ie->QueryInterface(IID_IConnectionPointContainer,(void **)&spCPC)))
	{
		if (SUCCEEDED(hr = spCPC->FindConnectionPoint(DIID_DWebBrowserEvents2, &spCP)))	 
		{
			hr = spCP->Advise(reinterpret_cast<IUnknown*>(this), &mCookie);			
		}
		else
		{
			spCPC->Release();
			return false;
		}			
		spCPC->Release();
	}
	else
		return false;
	if(SUCCEEDED(hr))
		return true;
	else
		return false;
}
bool CIEEvent::AttachEvent(MSHTML::IHTMLElementPtr tele)
{
	ele=tele;
	if(tele==NULL)
		return false;
	HRESULT hr;
	IConnectionPointContainer* spCPC=NULL;			
	if(SUCCEEDED(hr=tele->QueryInterface(IID_IConnectionPointContainer,(void **)&spCPC)))
	{
		if (SUCCEEDED(hr = spCPC->FindConnectionPoint(DIID_HTMLElementEvents2, &spCPEle)))	 
			hr = spCPEle->Advise(reinterpret_cast<IUnknown*>(this), &mCookieEle);			
		else if (SUCCEEDED(hr = spCPC->FindConnectionPoint(DIID_HTMLSelectElementEvents2, &spCPEle)))	 
			hr = spCPEle->Advise(reinterpret_cast<IUnknown*>(this), &mCookieEle);							
		else if (SUCCEEDED(hr = spCPC->FindConnectionPoint(DIID_HTMLInputTextElementEvents2, &spCPEle)))	 
			hr = spCPEle->Advise(reinterpret_cast<IUnknown*>(this), &mCookieEle);							
		else if (SUCCEEDED(hr = spCPC->FindConnectionPoint(DIID_HTMLAnchorEvents2, &spCPEle)))	 
			hr = spCPEle->Advise(reinterpret_cast<IUnknown*>(this), &mCookieEle);							
		else if (SUCCEEDED(hr = spCPC->FindConnectionPoint(DIID_HTMLButtonElementEvents2, &spCPEle)))	 
			hr = spCPEle->Advise(reinterpret_cast<IUnknown*>(this), &mCookieEle);							
		else if (SUCCEEDED(hr = spCPC->FindConnectionPoint(DIID_HTMLFormElementEvents2, &spCPEle)))	 
			hr = spCPEle->Advise(reinterpret_cast<IUnknown*>(this), &mCookieEle);							
		else if (SUCCEEDED(hr = spCPC->FindConnectionPoint(DIID_HTMLLabelEvents2, &spCPEle)))	 
			hr = spCPEle->Advise(reinterpret_cast<IUnknown*>(this), &mCookieEle);		
		else if (SUCCEEDED(hr = spCPC->FindConnectionPoint(DIID_HTMLTableEvents2, &spCPEle)))	 
			hr = spCPEle->Advise(reinterpret_cast<IUnknown*>(this), &mCookieEle);							
		else if (SUCCEEDED(hr = spCPC->FindConnectionPoint(DIID_HTMLTextContainerEvents2, &spCPEle)))	 
			hr = spCPEle->Advise(reinterpret_cast<IUnknown*>(this), &mCookieEle);							

		spCPC->Release();
	}
	else
		return false;
	if(SUCCEEDED(hr))
		return true;
	else
		return false;
}
STDMETHODIMP CIEEvent::QueryInterface(REFIID riid,void __RPC_FAR *__RPC_FAR *ppvObject)
{
	if (!ppvObject)
	return E_POINTER;

	*ppvObject = NULL;
	if (riid==__uuidof(IUnknown) || riid==__uuidof(IDispatch))
	{
	*ppvObject = this;
	return S_OK;
	}
	return E_NOINTERFACE;
}
ULONG __stdcall CIEEvent::AddRef(void)
{
	++m_dwRef;
	return m_dwRef;
}

ULONG __stdcall CIEEvent::Release(void)
{
	--m_dwRef;
	return m_dwRef;
}
STDMETHODIMP CIEEvent::GetTypeInfoCount(UINT *pctinfo)
{
	return E_NOTIMPL;
}
STDMETHODIMP CIEEvent::GetTypeInfo(UINT iTInfo,LCID lcid,ITypeInfo **ppTInfo)
{  
	*ppTInfo = NULL;
	return E_NOTIMPL;
}
STDMETHODIMP CIEEvent::GetIDsOfNames(REFIID riid,LPOLESTR *rgszNames,UINT cNames,LCID lcid, DISPID *rgDispId)
{
	return E_NOTIMPL;
}
STDMETHODIMP CIEEvent::Invoke(DISPID dispidMember,REFIID riid, LCID lcid,
  WORD wFlags, DISPPARAMS * pDispParams,
  VARIANT * pvarResult,EXCEPINFO * pexcepinfo,
  UINT * puArgErr)
{
	switch (dispidMember)
	{	
		//Webbrowser Event
		case DISPID_NAVIGATECOMPLETE2:
		{			
			//printf("navicomp2 fired\n");			
			HRESULT hr;
			IDispatch *document_dispatch = NULL;
			MSHTML::IHTMLDocument2Ptr doc=NULL;

			SHDocVw::IWebBrowser2Ptr tie=NULL;
			IDispatch* p1=pDispParams->rgvarg[1].pdispVal;
			hr=p1->QueryInterface(IID_IWebBrowser2,(void **)&tie);
			if(SUCCEEDED(hr)&&(tie!=NULL))
			{
				hr = tie->get_Document(&document_dispatch);				
			}
			if (SUCCEEDED(hr) && (document_dispatch != NULL)) {
				hr = document_dispatch->QueryInterface(IID_IHTMLDocument2,(void **)&doc);				
				document_dispatch->Release();		
			}		
			if(doc!=NULL)
			{				
				MSHTML::IHTMLWindow2 *win=NULL;			
				if(SUCCEEDED(hr=doc->get_parentWindow(&win)))
				{							
					try{					
						hr=win->execScript(L"function alert(obj){return \"\";}",L"javascript");
						hr=win->execScript(L"function confirm(obj){return true;}",L"javascript");
					}
					catch(_com_error er)
					{
						//printf("error\n");
					}
				 win->Release();
				}				
			}
		}
		break;
		
		case DISPID_DOCUMENTCOMPLETE:
		{
			IDispatch* disp=NULL;
			if( SUCCEEDED(ie->QueryInterface(IID_IDispatch,(void **)&disp)))
			{
				if(disp==pDispParams->rgvarg[1].pdispVal)
				{
					PostThreadMessageW(GetCurrentThreadId(),WM_WB_READY,(WPARAM)0,(LPARAM)0);
				}
				disp->Release();
				disp=NULL;
			}
			HRESULT hr;
			IDispatch *document_dispatch = NULL;
			MSHTML::IHTMLDocument2Ptr doc=nullptr;

			SHDocVw::IWebBrowser2Ptr tie=nullptr;
			IDispatch* p1=pDispParams->rgvarg[1].pdispVal;
			hr=p1->QueryInterface(IID_IWebBrowser2,(void **)&tie);
			if(SUCCEEDED(hr)&&(tie!=NULL))
			{
				hr = tie->get_Document(&document_dispatch);				
			}
			if (SUCCEEDED(hr) && (document_dispatch != NULL)) {
				hr = document_dispatch->QueryInterface(IID_IHTMLDocument2,(void **)&doc);				
				document_dispatch->Release();		
			}	
			if(doc!=NULL)
			{				
				MSHTML::IHTMLWindow2 *win=NULL;					
				if(SUCCEEDED(hr=doc->get_parentWindow(&win)))
				{							
					try{					
					 hr=win->execScript(L"document.body.oncopy=function(){ }",L"javascript");
					}
					catch(_com_error er)
					{
						//printf("error\n");
					}
				 win->Release();
				}				
			}
		}
		break;
		/*case DISPID_DOWNLOADCOMPLETE:
			printf("downloadcomplete fired\n");
			break;*/
		//Element Event
		case DISPID_HTMLELEMENTEVENTS2_ONPROPERTYCHANGE:
			printf(" element property changed fired\n");
			PostThreadMessageW(GetCurrentThreadId(),WM_ELE_ONCHANGE,(WPARAM)0,(LPARAM)0);
			break;
		default:
			break;
		}
return S_OK;
}

