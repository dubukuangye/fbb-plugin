#include "userinformation.h"

UserinformationClass* UserinformationClass::_singleton = NULL; // initialize statice member
long UserinformationClass::_userId = -1;
boost::mutex UserinformationClass::instance_mutex;

UserinformationClass::UserinformationClass() {
	if( _userId==-1 ) 
		throw FBB::Exception::FBBException("_userId not set in userinformation");
	
	FBB::WS::WSClientClass::loadUserInformation(_userId, _userInfoMap);
}

wstring UserinformationClass::getFromUserInfo(wstring first) {
	if( _userInfoMap.size()==0 )
		throw FBB::Exception::FBBException("_userInfoMap size=0 in userinformation");

	if( _userInfoMap.find(first)==_userInfoMap.end() )
		throw FBB::Exception::FBBException("no colum matched in user information");

	return _userInfoMap[first];
}