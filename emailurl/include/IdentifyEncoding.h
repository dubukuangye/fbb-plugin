#ifndef _IDENTIFYENCODING_H
#define _IDENTIFYENCODING_H

#include <string>
#include <vector>
#include "RxMailMessage.h"
using namespace std;

namespace FabubaoMail
{
    /// <summary>
    /// ����ַ��������
    /// <seealso cref="System.IO.Stream"/>  
    class IdentifyEncoding
    {      
        // Frequency tables to hold the GB, Big5, and EUC-TW character
        // frequencies
        int GBFreq[94][94];
        int GBKFreq[126][191];
        int Big5Freq[94][158];
        int EUC_TWFreq[94][94];

        vector<string> nicename;
		void Initialize_Frequencies();        
	public:
		IdentifyEncoding();
		EncodingType GetEncodingName(unsigned char* rawtext,int len);       
        int GB2312Probability(unsigned char* rawtext,int len);
		int GBKProbability(unsigned char* rawtext,int len);
		int HZProbability(unsigned char* rawtext,int len);
		int BIG5Probability(unsigned char* rawtext,int len);
		int ENCTWProbability(unsigned char* rawtext,int len);
		int ISO2022CNProbability(unsigned char* rawtext,int len);
		int UTF8Probability(unsigned char* rawtext,int len);
		int UnicodeProbability(unsigned char* rawtext,int len);
		int ASCIIProbability(unsigned char* rawtext,int len);
        
		int Identity(int literal)
        {
            return literal;
        }
        /// <summary>
        /// This method returns the literal value received
        /// </summary>
        /// <param name="literal">The literal to return</param>
        /// <returns>The received value</returns>
        long Identity(long literal)
        {
            return literal;
        }

        /// <summary>
        /// This method returns the literal value received
        /// </summary>
        /// <param name="literal">The literal to return</param>
        /// <returns>The received value</returns>
        unsigned long Identity(unsigned long literal)
        {
            return literal;
        }

        /// <summary>
        /// This method returns the literal value received
        /// </summary>
        /// <param name="literal">The literal to return</param>
        /// <returns>The received value</returns>
        float Identity(float literal)
        {
            return literal;
        }

        /// <summary>
        /// This method returns the literal value received
        /// </summary>
        /// <param name="literal">The literal to return</param>
        /// <returns>The received value</returns>
        double Identity(double literal)
        {
            return literal;
        }
    };
   
}

#endif