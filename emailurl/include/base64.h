// _Base64.h: interface for the C_Base64 class.
//
//////////////////////////////////////////////////////////////////////
#if !defined(AFX__BASE64_H__D718F826_7489_43E8_B5EF_64ED8EBBCD7E__INCLUDED_)
#define AFX__BASE64_H__D718F826_7489_43E8_B5EF_64ED8EBBCD7E__INCLUDED_
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable: 4786)
#include <map>
using namespace std;
class C_Base64  
{
public:
    C_Base64();
     virtual ~C_Base64();
     bool EncodeBase64(const unsigned char *Source, unsigned int nNum, unsigned char *Dest, unsigned int &nNumout);
     bool DecodeBase64(const unsigned char *Source, unsigned int nNum, unsigned char *Dest, unsigned int &nNumout);
	 bool DecodeBase64(const string& Source,string& Dest);
public:
    typedef unsigned char BYTE;
    typedef map<BYTE, BYTE> DECODETYPE;
private:
    bool Initial();
	void ProBase64(string& ori);
private:
    static const unsigned char CodeTable[] ;
    DECODETYPE DecodeTable;
    
};
#endif // !defined(AFX__BASE64_H__D718F826_7489_43E8_B5EF_64ED8EBBCD7E__INCLUDED_)
