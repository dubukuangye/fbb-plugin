#ifndef _STRINGOPT_H
#define _STRINGOPT_H

#include <string>
#include <vector>
using namespace std;

namespace FabubaoMail
{
	vector<string> SplitString(const string& str,const string& dim);
	void TrimString(string& str);
	std::string ws2s(const std::wstring& ws);
	std::wstring s2ws(const std::string& s);
}

#endif