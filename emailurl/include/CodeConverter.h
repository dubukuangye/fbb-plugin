//这是个类strCoding (strCoding.h文件)
#pragma once
#include <iostream>
#include <string>
#include <windows.h>
using namespace std;
//typedef wchar_t WCHAR;
namespace FabubaoMail
{
class strCoding
{
public:
    strCoding(void);
    ~strCoding(void);
   
    void UTF_8ToGB2312(string &pOut, const char *pText, int pLen);//utf_8转为gb2312
    void GB2312ToUTF_8(string& pOut,const char *pText, int pLen); //gb2312 转utf_8
	void UnicodeToGB2312(string& pOut,const char* wchar,int pLen);//Unicode to Gb2312
  
private:
    void Gb2312ToUnicode(WCHAR* pOut,const char *gbBuffer);
    void UTF_8ToUnicode(WCHAR* pOut,const char *pText);
    void UnicodeToUTF_8(char* pOut,const WCHAR* pText);
    void UnicodeToGB2312(char* pOut,const WCHAR uData);
    char  CharToInt(char ch);
    char StrToBin(char *str);

};
}