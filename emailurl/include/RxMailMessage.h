#ifndef _RXMAILMESSAGE_H
#define _RXMAILMESSAGE_H

#include <string>
#include <vector>
#include <time.h>
#include <algorithm>
using namespace std;

namespace FabubaoMail
{
	enum EncodingType
	{
		ASCII=0,
        GB2312=1,
        GBK=2,       
        UTF8=3,
        UNICODEA=4,
        OTHER=5  
	};
	enum TransferEncoding
    {       
        Unknown = -1,        
        QuotedPrintable = 0,       
        Base64 = 1,      
        SevenBit = 2
	};
	class ContentType
	{
	public:
		ContentType();
		string type;
		EncodingType charset;
		string Boundary;
	};
	class RxMailMessage//��ʾһ���ʼ�
	{
	public:
		RxMailMessage();
		string DeliveredTo;
		string ReturnPath;
		time_t DeliveryDate;
		string From;
		string To;
		string Subject;
		string Body;
		string Sender;		
		string MessageId;
		string MimeVersion;
		string ContentId;
		string ContentDescription;
		string TransferType;// something like "7bit" / "8bit" / "binary" / "quoted-printable" / "base64"
		TransferEncoding ContentTransferEncoding;
		EncodingType defaultBodyEncoding;
		EncodingType BodyEncoding;
		ContentType contentType;
		string MediaMainType;
		string MediaSubType;
		vector<RxMailMessage> Entities;
		vector<string> UnknowHeaderLines;
	
	public:
		void SetContentTypeFields(string type,EncodingType guessEncoding);
		 void MailStructure(string& re);
		 RxMailMessage CreateChildEntity();
	private:
		void decodeEntity(const RxMailMessage& entity,string& re);
	};
}

#endif