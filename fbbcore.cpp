#include "fbbcore.h"

FB::JSObjectPtr IEEntityClass::downloadCallback = FB::JSObjectPtr();

IEEntityClass::~IEEntityClass() {
	try {
		delete _parser;
		_parser = NULL;

		delete _browser;
		_browser = NULL;

		delete _util;
		_util = NULL;

		map<FBB::Entity::OneOptRegEntity::OneOptRegTypeEntity, Evaluator*>::iterator it = _evaluatorMap.begin();
		while( it!=_evaluatorMap.end() ) {
			delete it->second;
			it++;
		}
		_evaluatorMap.clear();
	} catch(...) {
		
	}
}

void IEEntityClass::createEvaluator() {
	_evaluatorMap[FBB::Entity::OneOptRegEntity::InvokeMember] = new InvokeMemberEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::SetDirectValue] = new SetDirectValueEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::SetFieldValue] = new SetFieldValueEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::SelectionDirect] = new SelectionDirectEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::SelectionSequence] = new SelectionSequenceEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::Captcha] = new CaptchaEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::User] = new UserEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::PassWord] = new PassWordEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::RepeatPassWord] = new RepeatPassWordEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::BuinessDirectionMap] = new BuinessDirectionMapEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::BuinessModeMap] = new BuinessModeMapEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::ProDirectoryMap] = new ProDirectoryMapEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::AreaMap] = new AreaMapEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::Focus] = new FocusEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::MobileCaptcha] = new MobileCaptchaEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::ExtractUserName] = new ExtractUserNameEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::QuestionCaptcha] = new QuestionCaptchaEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::QuestionCaptchaSel] = new QuestionCaptchaSelEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::InvokeKeyPress] = new InvokeKeyPressEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::RedirectURL] = new RedirectURLEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::SetRegUser] = new SetRegUserEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::AttachValue] = new UploadFileEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::UploadFile] = new UploadFileEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::Email_URL] = new Email_URLEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::WaitReady] = new Email_URLEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::RecordCaptcha] = new Email_URLEvaluator(this);
	_evaluatorMap[FBB::Entity::OneOptRegEntity::RepeatCaptcha] = new Email_URLEvaluator(this);
}

void IEEntityClass::EvaluateOneOptReg(FBB::Entity::OneOptRegEntity oor) {
	boost::this_thread::interruption_point();

	// equal to string.IsNullOrEmpty
	if( oor.domEle.tagName.empty() )
		return;

	Evaluator* evaluator = lookupEvaluator(oor.type);
 	bool success = evaluator->evaluate(oor);
	if( !success ) {
		FBLOG_INFO("guo", "[opt] : type=" << FBB::Helper::UtilClass::longToString( static_cast<long>(oor.type) ) << " fail" );
		
		throw FBB::Exception::FBBException(
			"[opt] : type=" + 
			FBB::Helper::UtilClass::longToString( static_cast<long>(oor.type) ) 
		);
	}
	else
		FBLOG_INFO("guo", "[opt] : type=" << FBB::Helper::UtilClass::longToString( static_cast<long>(oor.type) ) << " success" );
}

Evaluator* IEEntityClass::lookupEvaluator(FBB::Entity::OneOptRegEntity::OneOptRegTypeEntity type) {
	if( _evaluatorMap.find(type)==_evaluatorMap.end() )
		throw FBB::Exception::FBBException("[opt/lookupEvaluator] : type=" + 
			FBB::Helper::UtilClass::longToString( static_cast<long>(type) ) 
			);

	return _evaluatorMap[type];
}

void IEEntityClass::setCaptchaAnswer(string captchaKind, wstring answer) {
	if( captchaKind=="CAPTCHA" )
		_captcha = answer;
	else if( captchaKind=="MOBILECAPTCHA" )
		_moibleCaptcha = answer;
	else if( captchaKind=="QUESTIONCAPTCHA" )
		_questionCaptcha = answer;
	else if( captchaKind=="QUESTIONCAPTCHASEL" ) {
		string str_answer = FB::wstring_to_utf8( answer );
		_selectIndex = FBB::Helper::UtilClass::charArrayToLong( str_answer.c_str() );
	} 

	return;
}

FB::JSObjectPtr RegistrationClass::_regErrorCallback = FB::JSObjectPtr();
FB::JSObjectPtr RegistrationClass::_regCompleteCallback = FB::JSObjectPtr();
FB::JSObjectPtr RegistrationClass::_regFinishCallback = FB::JSObjectPtr();
FB::JSObjectPtr RegistrationClass::_regCaptchaCallback = FB::JSObjectPtr();

string RegistrationClass::generateCaptchaFilePath() {
	// CreateDirectory(L"C:\\CaptchaFolder", NULL);
	// _captchaUrl = "C:\\CaptchaFolder\\" + FBB::Helper::UtilClass::longToString(_siteId) + ".bmp";
	_captchaUrl = FB::System::getTempPath() + "\\" + FBB::Helper::UtilClass::longToString(_siteId) + ".bmp";
	return _captchaUrl;
}
void RegistrationClass::makeCaptchaCallback(string captchaKind) {
	if( captchaKind=="CAPTCHA" )
		_regCaptchaCallback->InvokeAsync("", FB::variant_list_of(_siteId)("CAPTCHA")(_captchaUrl));
	else if(captchaKind=="MOBILECAPTCHA" )
		_regCaptchaCallback->InvokeAsync("", FB::variant_list_of(_siteId)("MOBILECAPTCHA"));
	else if( captchaKind=="QUESTIONCAPTCHA" )
		_regCaptchaCallback->InvokeAsync("", FB::variant_list_of(_siteId)("QUESTIONCAPTCHA"));
	else if( captchaKind=="QUESTIONCAPTCHASEL" )
		_regCaptchaCallback->InvokeAsync("", FB::variant_list_of(_siteId)("QUESTIONCAPTCHASEL"));

	return;
}

wstring RegistrationClass::getFromEntity(wstring first) {
	return UserinformationClass::getInstance()->getFromUserInfo(first);
}

void RegistrationClass::excuteSingle() {
	try {
		boost::this_thread::interruption_point();

		createEvaluator();

		_parser = new FBB::Parser::HtmlParserClass();
		_browser = new FBB::Browser::BrowserClass(_parser);
		if( _browser->Init()==false )
			throw FBB::Exception::FBBException("browser init failed");
		_util = new FBB::Helper::UtilClass();

		FBB::WS::WSClientClass::loadSiteRegPara(_siteId, _regPara);
		if( _regPara.Valid==false )
			throw FBB::Exception::FBBException("reagpara.valid=fase error");
		_browser->navigate(_regPara.startUrl, _parser);

		boost::this_thread::interruption_point();
		
		if( _regPara.initWait ) 
			_browser->checkDocumentComplete(WM_NOMSG, 1000);
		if( _regPara.regCompStep==0 )
			_regPara.regCompStep = _regPara.totSteps;

		for(long stepi=0; stepi<_regPara.totSteps; stepi++) {
			for (unsigned long i = 0; i < _regPara.allsteps[stepi].allopts.size(); i++)
				EvaluateOneOptReg(_regPara.allsteps[stepi].allopts[i]);
					
			boost::this_thread::interruption_point();
			
			_browser->checkDocumentComplete();
			if( _regPara.allsteps[stepi].stepWait )
				_browser->checkDocumentComplete(WM_NOMSG, 1000);

			if(stepi+1==_regPara.regCompStep) { // reg complete step, not always the last step
				long regStatus = 1;

				wstring fullText = _parser->getOuterText();
				wstring ws_sucText = FB::utf8_to_wstring( _regPara.sucText );
				FBB::WS::WSClientClass wsClient;
				if( fullText.find(ws_sucText)==std::wstring::npos )
					throw FBB::Exception::FBBException("[regCompStep] : sucText not found");
				else if( wsClient.saveRegistInfo(UserinformationClass::getUserId(), _siteId, regStatus, _regPara.LoginName, _regPara.LoginPw)==false )
					throw FBB::Exception::FBBException("[regCompStep] : saveRegistInfo failed");
				else
					_regCompleteCallback->InvokeAsync("", FB::variant_list_of(_siteId));
			}
		}

		_regFinishCallback->InvokeAsync("", FB::variant_list_of(_siteId));
	} catch (std::exception& e) {
		_regErrorCallback->InvokeAsync("", FB::variant_list_of(_siteId)(e.what()));
	} catch(boost::exception& ){
		_regErrorCallback->InvokeAsync("", FB::variant_list_of(_siteId)("boost error"));
	} 
}

FB::JSObjectPtr PublishClass::_publishErrorCallback = FB::JSObjectPtr();;
FB::JSObjectPtr PublishClass::_publishCompleteCallback = FB::JSObjectPtr();
FB::JSObjectPtr PublishClass::_publishCaptchaCallback = FB::JSObjectPtr();

string PublishClass::generateCaptchaFilePath() {
	// CreateDirectory(L"C:\\CaptchaFolder", NULL);
	// _captchaUrl = "C:\\CaptchaFolder\\" + FBB::Helper::UtilClass::longToString(_siteId) + "_" + FBB::Helper::UtilClass::longToString(_PMID) + ".bmp";
	_captchaUrl = FB::System::getTempPath() + "\\" + FBB::Helper::UtilClass::longToString(_siteId) + "_" + FBB::Helper::UtilClass::longToString(_PMID) + ".bmp";
	return _captchaUrl;
}
void PublishClass::makeCaptchaCallback(string captchaKind) {
	if( captchaKind=="CAPTCHA" )
		_publishCaptchaCallback->InvokeAsync("", FB::variant_list_of(_siteId)(_PMID)("CAPTCHA")(_captchaUrl));
	else if(captchaKind=="MOBILECAPTCHA" )
		_publishCaptchaCallback->InvokeAsync("", FB::variant_list_of(_siteId)(_PMID)("MOBILECAPTCHA"));
	else if( captchaKind=="QUESTIONCAPTCHA" )
		_publishCaptchaCallback->InvokeAsync("", FB::variant_list_of(_siteId)(_PMID)("QUESTIONCAPTCHA"));
	else if( captchaKind=="QUESTIONCAPTCHASEL" )
		_publishCaptchaCallback->InvokeAsync("", FB::variant_list_of(_siteId)(_PMID)("QUESTIONCAPTCHASEL"));

	return;
}

wstring PublishClass::getFromEntity(wstring first) {
	if( _publishEntityInfoMap.size()==0 )
		FBB::WS::WSClientClass::loadPublishEntity(_PMID, _siteId, _publishEntityInfoMap);

	if( _publishEntityInfoMap.find(first)==_publishEntityInfoMap.end() )
		throw FBB::Exception::FBBException("no colum matched in publish entity information");

	return _publishEntityInfoMap[first];
}

void PublishClass::excuteSingle() {
	try {
		boost::this_thread::interruption_point();

		createEvaluator();

		_parser = new FBB::Parser::HtmlParserClass();
		_browser = new FBB::Browser::BrowserClass(_parser);
		if( _browser->Init()==false )
			throw FBB::Exception::FBBException("browser init failed");
		_util = new FBB::Helper::UtilClass();

		FBB::WS::WSClientClass::loadSitePubPara(_siteId, _publishPara);
		if( _publishPara.Valid==false )
			throw FBB::Exception::FBBException("publishPara.valid=fase error");
		if( _publishPara.PublishType!=1&&getFromEntity(L"HasProduct")==L"false" )
			throw FBB::Exception::FBBException("no product in publish entity error");
		_browser->navigate(_publishPara.startUrl, _parser);

		boost::this_thread::interruption_point();
		
		if( _publishPara.initWait ) 
			_browser->checkDocumentComplete(WM_NOMSG, 1000);

		for(long stepi=0; stepi<_publishPara.totSteps; stepi++) {
			for (unsigned long i = 0; i < _publishPara.allsteps[stepi].allopts.size(); i++)
				EvaluateOneOptReg(_publishPara.allsteps[stepi].allopts[i]);
					
			boost::this_thread::interruption_point();
			
			_browser->checkDocumentComplete();
			if( _publishPara.allsteps[stepi].stepWait )
				_browser->checkDocumentComplete(WM_NOMSG, 1000);
		}

		_publishCompleteCallback->InvokeAsync("", FB::variant_list_of(_siteId)(_PMID));
	} catch (std::exception& e) {
		_publishErrorCallback->InvokeAsync("", FB::variant_list_of(_siteId)(_PMID)(e.what()));
	} catch(boost::exception& ){
		_publishErrorCallback->InvokeAsync("", FB::variant_list_of(_siteId)(_PMID)("boost error"));
	} 
}