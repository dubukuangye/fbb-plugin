#/**********************************************************\ 
#
# Auto-Generated Plugin Configuration file
# for deploytest
#
#\**********************************************************/

set(PLUGIN_NAME "deploytest")
set(PLUGIN_PREFIX "DEP")
set(COMPANY_NAME "zhendao")

# ActiveX constants:
set(FBTYPELIB_NAME deploytestLib)
set(FBTYPELIB_DESC "deploytest 1.0 Type Library")
set(IFBControl_DESC "deploytest Control Interface")
set(FBControl_DESC "deploytest Control Class")
set(IFBComJavascriptObject_DESC "deploytest IComJavascriptObject Interface")
set(FBComJavascriptObject_DESC "deploytest ComJavascriptObject Class")
set(IFBComEventSource_DESC "deploytest IFBComEventSource Interface")
set(AXVERSION_NUM "1")

# NOTE: THESE GUIDS *MUST* BE UNIQUE TO YOUR PLUGIN/ACTIVEX CONTROL!  YES, ALL OF THEM!
set(FBTYPELIB_GUID 52484514-94fb-5d4a-87f9-ed9c384f9ef9)
set(IFBControl_GUID 34581f56-8a41-572e-a369-1a2304e05389)
set(FBControl_GUID 83a802ba-a34b-5841-81a3-207ec2d16a87)
set(IFBComJavascriptObject_GUID a7203236-2101-5496-909b-3a282fcb0859)
set(FBComJavascriptObject_GUID 142ba678-b730-5b9f-858d-e92d8aa280a2)
set(IFBComEventSource_GUID 22217f34-73c7-5f84-a28a-d3e05a648988)
if ( FB_PLATFORM_ARCH_32 )
    set(FBControl_WixUpgradeCode_GUID 7d6012cb-3bd2-585d-a22d-f772b3843315)
else ( FB_PLATFORM_ARCH_32 )
    set(FBControl_WixUpgradeCode_GUID aac69618-ee9d-51f9-b8ea-d8cfb20f8ded)
endif ( FB_PLATFORM_ARCH_32 )

# these are the pieces that are relevant to using it from Javascript
set(ACTIVEX_PROGID "zhendao.deploytest")
set(MOZILLA_PLUGINID "zhendao.com/deploytest")

# strings
set(FBSTRING_CompanyName "zhendao")
set(FBSTRING_PluginDescription "test deployment")
set(FBSTRING_PLUGIN_VERSION "1.0.0.0")
set(FBSTRING_LegalCopyright "Copyright 2013 zhendao")
set(FBSTRING_PluginFileName "np${PLUGIN_NAME}.dll")
set(FBSTRING_ProductName "deploytest")
set(FBSTRING_FileExtents "")
if ( FB_PLATFORM_ARCH_32 )
    set(FBSTRING_PluginName "deploytest")  # No 32bit postfix to maintain backward compatability.
else ( FB_PLATFORM_ARCH_32 )
    set(FBSTRING_PluginName "deploytest_${FB_PLATFORM_ARCH_NAME}")
endif ( FB_PLATFORM_ARCH_32 )
set(FBSTRING_MIMEType "application/x-deploytest")

# Uncomment this next line if you're not planning on your plugin doing
# any drawing:

set (FB_GUI_DISABLED 1)

# Mac plugin settings. If your plugin does not draw, set these all to 0
set(FBMAC_USE_QUICKDRAW 0)
set(FBMAC_USE_CARBON 0)
set(FBMAC_USE_COCOA 0)
set(FBMAC_USE_COREGRAPHICS 0)
set(FBMAC_USE_COREANIMATION 0)
set(FBMAC_USE_INVALIDATINGCOREANIMATION 0)

# If you want to register per-machine on Windows, uncomment this line
#set (FB_ATLREG_MACHINEWIDE 1)

# just for debug
add_firebreath_library(log4cplus)