#pragma once

#include <string>
#include <windows.h>
#include <sstream>
#include <ctime>
#include <cstring>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <fstream>
#include  <boost/algorithm/string/classification.hpp>  // boost split
#include  <boost/algorithm/string/split.hpp> 

#include "FBControl.h"
// #include <tchar.h>
// #include <initguid.h>

using namespace std;

namespace FBB{
	namespace Helper{
		class UtilClass
		{
		public:
			UtilClass(){}
			~UtilClass(){}

			string generateValidValue(string value, long minLength, long maxLength);
			string generateUsername(long minLength, long maxLength, long baseLength, bool firstAlpha, bool containNum, bool lowercase);
			string generatePassword(long minLength, long maxLength, long baseLength, bool firstAlpha, bool containNum, bool lowercase);
			bool saveImageFromClipboard(string );

			static string longToString(long number){
				stringstream ss;
				ss << number;
				return ss.str();
			}

			static long charArrayToLong(char number[]){
				return atol( number );
			}

			static long charArrayToLong(const char* number){
				return atol( number );
			}

			static int stringToTime(const string &strDateStr,time_t &timeData);
 
			static int timeToString(string &strDateStr,const time_t &timeData);

			static std::vector<string> split(const string& s, char delim);

			static string getWebServiceDomain();

			static string getDLLPath();

			static void createDirectoryByPath(const string& path);

			static bool isFileExist(const string& filepath);
		private:
			bool saveBitmapToFile(HBITMAP hBitmap, LPCWSTR lpFileName);

			static std::vector<std::string>& split(const std::string &s, char delim, std::vector<std::string> &elems);
		};
	}
}