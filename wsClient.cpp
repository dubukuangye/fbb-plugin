#include "wsClient.h"
#include "util.h"

#include <utf8_tools.h>
#include <assert.h>
#include <ctime>
#include "WebServiceFabuParaSoap.nsmap"

long FBB::WS::WSClientClass::CaptchaOptTypeMap[15] = {1, 2, 3, 101, 102, 201, 202, 203, 204, 205, 206, 301, 302, 401, 501};

#pragma region FBB::Entity::RegParaEntity
bool oneOptRegCustomMap(FBB::Entity::OneOptRegEntity* oneOptRegEntity, ns1__OneOptReg* oor){
	// DomElement
	FBB::Entity::DomElementEntity* domElementEntity = new FBB::Entity::DomElementEntity();
	domElementEntity->stype = static_cast<FBB::Entity::DomElementEntity::DomElementSeachTypeEntity>( oor->domEle->stype );
	string* p_eleId = (oor)->domEle->eleId;
	domElementEntity->eleId = (p_eleId==NULL) ? "" : *p_eleId;
	
	vector<int> int_vector;
	vector< int >::const_iterator int_it;

	if( (oor)->domEle->indexes!=NULL ){ // indexes
		int_vector = (oor)->domEle->indexes->int_;
		int_it = int_vector.begin();
		while( int_it!=int_vector.end() ){
			long index = *int_it;
			domElementEntity->indexes.push_back( index );

			int_it++;
		}
	}
	if( (oor)->domEle->frameIndexs!=NULL ){ // frameIndexs
		int_vector = (oor)->domEle->frameIndexs->int_;
		int_it = int_vector.begin();
		while( int_it!=int_vector.end() ){
			long frameIndex = *int_it;
			domElementEntity->frameIndexs.push_back( frameIndex );

			int_it++;
		}
	}

	string* p_tagName = (oor)->domEle->tagName;
	domElementEntity->tagName = (p_tagName==NULL) ? "" : *p_tagName;
	string* p_fitext = (oor)->domEle->fitext;
	domElementEntity->fitext = (p_fitext==NULL) ? "" : *p_fitext;
	string* p_pitext = (oor)->domEle->pitext;
	domElementEntity->pitext = (p_pitext==NULL) ? "" : *p_pitext;
	oneOptRegEntity->domEle = *domElementEntity;

	long type = static_cast<long>( oor->type );
	if( oor->type==17 )
		type = 161;
	if( oor->type>17 )
		type--;
	oneOptRegEntity->type = static_cast<FBB::Entity::OneOptRegEntity::OneOptRegTypeEntity>( type );
	string* p_eleOpt = (oor)->eleOpt;
	oneOptRegEntity->eleOpt = p_eleOpt==NULL ? "" : *p_eleOpt;
	oneOptRegEntity->selindex = (oor)->selindex;
	oneOptRegEntity->fireNext = (oor)->fireNext;

	// DomElement
	FBB::Entity::DomElementEntity* captureEle= new FBB::Entity::DomElementEntity();
	captureEle->stype = static_cast<FBB::Entity::DomElementEntity::DomElementSeachTypeEntity>( (oor)->captureEle->stype );
	p_eleId = (oor)->captureEle->eleId;
	captureEle->eleId = (p_eleId==NULL) ? "" : *p_eleId;
	
	if( (oor)->captureEle->indexes!=NULL ){ // indexes
		int_vector = (oor)->captureEle->indexes->int_;
		int_it = int_vector.begin();
		while( int_it!=int_vector.end() ){
			long index = *int_it;
			captureEle->indexes.push_back( index );

			int_it++;
		}
	}
	if( (oor)->captureEle->frameIndexs!=NULL ){ // frameIndexs
		int_vector = (oor)->captureEle->frameIndexs->int_;
		int_it = int_vector.begin();
		while( int_it!=int_vector.end() ){
			long frameIndex = *int_it;
			captureEle->frameIndexs.push_back( frameIndex );

			int_it++;
		}
	}

	p_tagName = (oor)->captureEle->tagName;
	captureEle->tagName = (p_tagName==NULL) ? "" : *p_tagName;
	p_fitext = (oor)->captureEle->fitext;
	captureEle->fitext = (p_fitext==NULL) ? "" : *p_fitext;
	p_pitext = (oor)->captureEle->pitext;
	captureEle->pitext = (p_pitext==NULL) ? "" : *p_pitext;
	oneOptRegEntity->captureEle = *captureEle;

	oneOptRegEntity->captchaNeedActive = (oor)->captchaNeedActive;

	// PWGenRule
	FBB::Entity::PWGenRuleEntity* pwgenrule= new FBB::Entity::PWGenRuleEntity();
	if( oor->pwgenrule!=NULL ) {
		pwgenrule->minLength = (oor)->pwgenrule->minLength;
		pwgenrule->maxLength = (oor)->pwgenrule->maxLength;
		pwgenrule->FirstAlpha = (oor)->pwgenrule->FirstAlpha;
		pwgenrule->ContainNum = (oor)->pwgenrule->ContainNum;
	}
	oneOptRegEntity->pwgenrule = *pwgenrule;

	// UNGenRule
	FBB::Entity::UNGenRuleEntity* usernamegenrule= new FBB::Entity::UNGenRuleEntity();
	if( oor->usernamegenrule!=NULL ) {
		usernamegenrule->minLength = (oor)->usernamegenrule->minLength;
		usernamegenrule->maxLength = (oor)->usernamegenrule->maxLength;
		usernamegenrule->FirstAlpha = (oor)->usernamegenrule->FirstAlpha;
		usernamegenrule->ContainNum = (oor)->usernamegenrule->ContainNum;
		usernamegenrule->lowercase = (oor)->usernamegenrule->lowercase;
	}
	oneOptRegEntity->usernamegenrule = *usernamegenrule;

	oneOptRegEntity->pageType = (oor)->pageType;
	string* p_Url = (oor)->Url;
	oneOptRegEntity->Url = p_Url==NULL ? "" : *p_Url;

	// DomElement [changeEle]
	FBB::Entity::DomElementEntity* changeEle= new FBB::Entity::DomElementEntity();

	if( oor->changeEle!=NULL ) {

		changeEle->stype = static_cast<FBB::Entity::DomElementEntity::DomElementSeachTypeEntity>( (oor)->changeEle->stype );
		p_eleId = (oor)->changeEle->eleId;
		changeEle->eleId = (p_eleId==NULL) ? "" : *p_eleId;
	
		if( (oor)->changeEle->indexes!=NULL ){ // indexes
			int_vector = (oor)->changeEle->indexes->int_;
			int_it = int_vector.begin();
			while( int_it!=int_vector.end() ){
				long index = *int_it;
				changeEle->indexes.push_back( index );

				int_it++;
			}
		}
		if( (oor)->changeEle->frameIndexs!=NULL ){ // frameIndexs
			int_vector = (oor)->changeEle->frameIndexs->int_;
			int_it = int_vector.begin();
			while( int_it!=int_vector.end() ){
				long frameIndex = *int_it;
				changeEle->frameIndexs.push_back( frameIndex );

				int_it++;
			}
		}

		p_tagName = (oor)->changeEle->tagName;
		changeEle->tagName = (p_tagName==NULL) ? "" : *p_tagName;
		p_fitext = (oor)->changeEle->fitext;
		changeEle->fitext = (p_fitext==NULL) ? "" : *p_fitext;
		p_pitext = (oor)->changeEle->pitext;
		changeEle->pitext = (p_pitext==NULL) ? "" : *p_pitext;
	}
	oneOptRegEntity->changeEle = *changeEle; // end of DomElement

	oneOptRegEntity->fileIndex = oor->fileIndex;

	// emailFind
	FBB::Entity::EmailFindRule* emailFind= new FBB::Entity::EmailFindRule();
	if( oor->emailFind!=NULL ) {
		string* p_fromStr = oor->emailFind->fromStr;
		emailFind->fromStr = (p_fromStr==NULL) ? "" : *p_fromStr;
		string* p_subjectStr = oor->emailFind->subjectStr;
		emailFind->subjectStr = (p_subjectStr==NULL) ? "" : *p_subjectStr;
	}
	oneOptRegEntity->emailFind = *emailFind;

	// emailUrl
	FBB::Entity::EmailURLRule* emailUrl= new FBB::Entity::EmailURLRule();
	if( oor->emailUrl!=NULL ) {
		string* p_urlStr = oor->emailUrl->urlStr;
		emailUrl->urlStr = (p_urlStr==NULL) ? "" : *p_urlStr;
	}
	oneOptRegEntity->emailUrl = *emailUrl;

	oneOptRegEntity->emailTime = oor->emailTime;
	oneOptRegEntity->emailTimeError = oor->emailTimeError;
	oneOptRegEntity->autoCaptcha = oor->autoCaptcha;
	oneOptRegEntity->captchaType = oor->captchaType;
	
	delete domElementEntity;
	delete captureEle;
	delete pwgenrule;
	delete usernamegenrule;
	delete changeEle;
	delete emailFind;
	delete emailUrl;

	return true;
}

bool oneOptRegCustomMap(vector<FBB::Entity::OneOptRegEntity>& oneOptRegVector, vector<class ns1__OneOptReg * >& OneOptReg){
	vector< ns1__OneOptReg* >::const_iterator it = OneOptReg.begin();
	// if( *it==NULL ){ return false; } // ÄÚÈÝÎª¿Õ£¨¿ÉÄÜÊÇ¸ÃÕ¾µãÃ»ÓÐ×¢²á²ÎÊý£©

	while( it!=OneOptReg.end() ){
		FBB::Entity::OneOptRegEntity* oneOptRegEntity = new FBB::Entity::OneOptRegEntity();
		
		oneOptRegCustomMap(oneOptRegEntity, *it);
		it++;
		oneOptRegVector.push_back( *oneOptRegEntity );
		
		delete oneOptRegEntity;
	}

	return true;
}

bool regStepCustomMap(vector<FBB::Entity::RegStepEntity>& regStepVector, vector<class ns1__RegStep * >&RegStep){
	vector< ns1__RegStep* >::const_iterator it = RegStep.begin();
	// if( *it==NULL ){ return false; } // ÄÚÈÝÎª¿Õ£¨¿ÉÄÜÊÇ¸ÃÕ¾µãÃ»ÓÐ×¢²á²ÎÊý£©

	while( it!=RegStep.end() ){
		// regParaCustomMap(*it, siteRegParaMap);
		vector<FBB::Entity::OneOptRegEntity> oneOptRegVector;
		oneOptRegCustomMap(oneOptRegVector, (*it)->allopts->OneOptReg);

		FBB::Entity::RegStepEntity* regStepEntity = new FBB::Entity::RegStepEntity();
		regStepEntity->allopts = oneOptRegVector;
		regStepEntity->stepWait = (*it)->stepWait;
		regStepVector.push_back( *regStepEntity );

		it++;

		delete regStepEntity;
	}

	return true;
}

bool regParaCustomMap(ns1__RegPara* ns1_reg,  map<long, FBB::Entity::RegParaEntity>& siteRegParaMap){
	FBB::Entity::RegParaEntity* regParaEntity = new FBB::Entity::RegParaEntity();
	long siteId = ns1_reg->siteID;

	regParaEntity->totSteps = ns1_reg->totSteps;
	string* p_startUrl = ns1_reg->startUrl;
	regParaEntity->startUrl = (p_startUrl==NULL) ? "" : *p_startUrl;
	
	//  RegStepEntity
	vector<FBB::Entity::RegStepEntity> regStepVector;
	regStepCustomMap(regStepVector, ns1_reg->allsteps->RegStep);
	regParaEntity->allsteps = regStepVector;
	
	string* p_LoginName = ns1_reg->LoginName;
	regParaEntity->LoginName = (p_LoginName==NULL) ? "" : *p_LoginName;
	string* p_LoginPw = ns1_reg->LoginPw;
	regParaEntity->LoginPw = (p_LoginPw==NULL) ? "" : *p_LoginPw;
	string* p_sucText = ns1_reg->sucText;
	regParaEntity->sucText = (p_sucText==NULL) ? "" : *p_sucText;
	regParaEntity->regCompStep = ns1_reg->regCompStep;
	regParaEntity->siteID = siteId;
	regParaEntity->initWait = ns1_reg->initWait;
	regParaEntity->Valid = ns1_reg->Valid;
	time_t* p_ValidTime = ns1_reg->ValidTime;
	regParaEntity->ValidTime = (p_ValidTime==NULL) ? NULL : *p_ValidTime;
	regParaEntity->NeedEmailValid = ns1_reg->NeedEmailValid;

	siteRegParaMap[siteId] = *regParaEntity;		

	delete regParaEntity;

	return true;
}

bool FBB::WS::WSClientClass::loadSiteRegPara(int siteId, FBB::Entity::RegParaEntity& regPara) {
	boost::this_thread::interruption_point();

	std::vector<int> siteIds;
	siteIds.push_back( siteId );
	map<long, FBB::Entity::RegParaEntity> siteRegParaMap;

	loadSiteRegPara(siteIds, siteRegParaMap);

	if( siteRegParaMap.find(siteId)!=siteRegParaMap.end() )
		regPara = siteRegParaMap[siteId];
	else
		throw FBB::Exception::FBBException("siteId: " + FBB::Helper::UtilClass::longToString(siteId) + " load regpara failed");

	return true;
}

bool FBB::WS::WSClientClass::loadSiteRegPara(vector<int> siteIds, map<long, FBB::Entity::RegParaEntity>& siteRegParaMap)
{
	boost::this_thread::interruption_point();

	WebServiceFabuParaSoapProxy reg(SOAP_C_UTFSTRING);

	_ns1__GetSiteRegParaBySiteIDs* req = new _ns1__GetSiteRegParaBySiteIDs();
	req->ah = new ns1__AuthHeader();
	req->ah->UserName = new std::string("zhendao");
	req->ah->PassWord = new std::string("zhendaofabu");
	
	req->siteindex = new ns1__ArrayOfInt();
	req->siteindex->int_ = siteIds;

	_ns1__GetSiteRegParaBySiteIDsResponse* resp = new _ns1__GetSiteRegParaBySiteIDsResponse();

	int flag = reg.GetSiteRegParaBySiteIDs(req, resp);
	if( reg.error ){
		reg.soap_stream_fault(std::cerr);

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->siteindex;
		delete req;
		delete resp;

		return false;
	} else {

		assert( resp->allregs->RegPara.empty()==false );

		vector< ns1__RegPara* >::const_iterator it = resp->allregs->RegPara.begin();
		if( *it==NULL ){ return false; } // ÄÚÈÝÎª¿Õ£¨¿ÉÄÜÊÇ¸ÃÕ¾µãÃ»ÓÐ×¢²á²ÎÊý£©

		while( it!=resp->allregs->RegPara.end() ){
			regParaCustomMap(*it, siteRegParaMap);

			it++;
		}

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->siteindex;
		delete req;
		delete resp;

		return true;
	}

}
#pragma endregion

#pragma region FBB::Entity::PublishParaEntity
bool pubParaCustomMap(ns1__PublishPara* ns1_pub,  map<long, FBB::Entity::PublishParaEntity>& sitePubParaMap){
	FBB::Entity::PublishParaEntity* pubParaEntity = new FBB::Entity::PublishParaEntity();
	long siteId = ns1_pub->siteID;

	pubParaEntity->totSteps = ns1_pub->totSteps;
	string* p_startUrl = ns1_pub->startUrl;
	pubParaEntity->startUrl = (p_startUrl==NULL) ? "" : *p_startUrl;
	
	//  RegStepEntity
	vector<FBB::Entity::RegStepEntity> regStepVector;
	regStepCustomMap(regStepVector, ns1_pub->allsteps->RegStep);
	pubParaEntity->allsteps = regStepVector;
	
	pubParaEntity->initWait = ns1_pub->initWait;
	pubParaEntity->siteID = siteId;
	pubParaEntity->Valid = ns1_pub->Valid;
	time_t* p_ValidTime = ns1_pub->ValidTime;
	pubParaEntity->ValidTime = (p_ValidTime==NULL) ? NULL : *p_ValidTime;
	pubParaEntity->PublishType = ns1_pub->PublishType;

	sitePubParaMap[siteId] = *pubParaEntity;

	delete pubParaEntity;

	return true;
}

bool FBB::WS::WSClientClass::loadSitePubPara(int siteId, FBB::Entity::PublishParaEntity& pubPara) {
	boost::this_thread::interruption_point();

	std::vector<int> siteIds;
	siteIds.push_back( siteId );
	map<long, FBB::Entity::PublishParaEntity> sitePubParaMap;

	loadSitePubPara(siteIds, sitePubParaMap);

	if( sitePubParaMap.find(siteId)!=sitePubParaMap.end() )
		pubPara = sitePubParaMap[siteId];
	else
		throw FBB::Exception::FBBException("siteId: " + FBB::Helper::UtilClass::longToString(siteId) + " load pubpara failed");

	return true;
}

bool FBB::WS::WSClientClass::loadSitePubPara(vector<int> siteIds, map<long, FBB::Entity::PublishParaEntity>& sitePubParaMap)
{ 
	WebServiceFabuParaSoapProxy pub(SOAP_C_UTFSTRING);

	_ns1__GetSitePubParaBySiteIDs* req = new _ns1__GetSitePubParaBySiteIDs();
	req->ah = new ns1__AuthHeader();
	req->ah->UserName = new std::string("zhendao");
	req->ah->PassWord = new std::string("zhendaofabu");
	
	req->siteindex = new ns1__ArrayOfInt();
	req->siteindex->int_ = siteIds;

	_ns1__GetSitePubParaBySiteIDsResponse* resp = new _ns1__GetSitePubParaBySiteIDsResponse();

	int flag = pub.GetSitePubParaBySiteIDs(req, resp);
	bool result = false;
	if( pub.error ) {
		pub.soap_stream_fault(std::cerr);
		result = false;
	} else {
		vector< ns1__PublishPara* >::const_iterator it = resp->allpubs->PublishPara.begin();
		if( *it==NULL ){ return false; } // ÄÚÈÝÎª¿Õ£¨¿ÉÄÜÊÇ¸ÃÕ¾µãÃ»ÓÐ×¢²á²ÎÊý£©

		while( it!=resp->allpubs->PublishPara.end() ){
			pubParaCustomMap(*it, sitePubParaMap);

			it++;
		}

		result = true;
	}

	delete req->ah->UserName;
	delete req->ah->PassWord;
	delete req->siteindex;
	delete req->ah;
	delete req;
	delete resp;

	return result;
}
#pragma endregion

bool FBB::WS::WSClientClass::loadUserInformation(long userId, map<wstring, wstring>& userInfoMap)
{ 
	boost::this_thread::interruption_point();

	ws_USCOREInterfaceSoapProxy userInfoProxy(SOAP_C_UTFSTRING);

	string str_userId = FBB::Helper::UtilClass::longToString( userId );
	

	_ns2__GetUserByID* req = new _ns2__GetUserByID();
	req->ah = new ns2__AuthHeader();
	req->ah->UserName = new std::string("zhendao");
	req->ah->PassWord = new std::string("zhendaofabu");
	req->ID = &str_userId;


	_ns2__GetUserByIDResponse* resp = new _ns2__GetUserByIDResponse();

	int flag = userInfoProxy.GetUserByID_(req, resp);
	if( userInfoProxy.error ){
		userInfoProxy.soap_stream_fault(std::cerr);

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req;
		delete resp;

		return false;
	} else {
		ns2__View_USCOREParameter* p_userInfo = resp->GetUserByIDResult;

		if( p_userInfo!=NULL ){
			long id = p_userInfo->ID; // ID
			string str_id = FBB::Helper::UtilClass::longToString( id );
			wstring ws_ID = FB::utf8_to_wstring( str_id );
			userInfoMap[L"ID"] = ws_ID;

			string* p_LoginName = p_userInfo->LoginName; // LoginName
			string LoginName = (p_LoginName==NULL) ? "" : *p_LoginName;
			wstring ws_LoginName = FB::utf8_to_wstring( LoginName );
			userInfoMap[L"LoginName"] = ws_LoginName;

			string* p_LoginPassword = p_userInfo->LoginPassword; // LoginPassword
			string LoginPassword = (p_LoginPassword==NULL) ? "" : *p_LoginPassword;
			wstring ws_LoginPassword = FB::utf8_to_wstring( LoginPassword );
			userInfoMap[L"LoginPassword"] = ws_LoginPassword;

			string* p_Email = p_userInfo->Email; // Email
			string Email = (p_Email==NULL) ? "" : *p_Email;
			wstring ws_Email = FB::utf8_to_wstring( Email );
			userInfoMap[L"Email"] = ws_Email;

			string* p_EmailUserName = p_userInfo->EmailUserName; // EmailUserName
			string EmailUserName = (p_EmailUserName==NULL) ? "" : *p_EmailUserName;
			wstring ws_EmailUserName = FB::utf8_to_wstring( EmailUserName );
			userInfoMap[L"EmailUserName"] = ws_EmailUserName;

			string* p_EmailDomainName = p_userInfo->EmailDomainName; // EmailDomainName
			string EmailDomainName = (p_EmailDomainName==NULL) ? "" : *p_EmailDomainName;
			wstring ws_EmailDomainName = FB::utf8_to_wstring( EmailDomainName );
			userInfoMap[L"EmailDomainName"] = ws_EmailDomainName;

			string* p_CompanyName = p_userInfo->CompanyName; // CompanyName
			string CompanyName = (p_CompanyName==NULL) ? "" : *p_CompanyName;
			wstring ws_CompanyName = FB::utf8_to_wstring( CompanyName );
			userInfoMap[L"CompanyName"] = ws_CompanyName;
			// userInfoMap[L"CompanyName"] = L"";

			string* p_ENCompanyName = p_userInfo->ENCompanyName; // ENCompanyName
			string ENCompanyName = (p_ENCompanyName==NULL) ? "" : *p_ENCompanyName;
			wstring ws_ENCompanyName = FB::utf8_to_wstring( ENCompanyName );
			userInfoMap[L"ENCompanyName"] = ws_ENCompanyName;

			string* p_CompanyShortName = p_userInfo->CompanyShortName; // CompanyShortName
			string CompanyShortName = (p_CompanyShortName==NULL) ? "" : *p_CompanyShortName;
			wstring ws_CompanyShortName = FB::utf8_to_wstring( CompanyShortName );
			userInfoMap[L"CompanyShortName"] = ws_CompanyShortName;

			string* p_BusinessIndustry = p_userInfo->BusinessIndustry; // BusinessIndustry
			string BusinessIndustry = (p_BusinessIndustry==NULL) ? "" : *p_BusinessIndustry;
			wstring ws_BusinessIndustry = FB::utf8_to_wstring( BusinessIndustry );
			userInfoMap[L"BusinessIndustry"] = ws_BusinessIndustry;

			int* p_BusinessDirectionID = p_userInfo->BusinessDirectionID; // BusinessDirectionID
			int BusinessDirectionID = (p_BusinessDirectionID==NULL) ? 0 : *p_BusinessDirectionID;
			string str_BusinessDirectionID = FBB::Helper::UtilClass::longToString( BusinessDirectionID );
			wstring ws_BusinessDirectionID = FB::utf8_to_wstring( str_BusinessDirectionID );
			userInfoMap[L"BusinessDirectionID"] = ws_BusinessDirectionID;

			string* p_BusinessDirection = p_userInfo->BusinessDirection; // BusinessDirection
			string BusinessDirection = (p_BusinessDirection==NULL) ? "" : *p_BusinessDirection;
			wstring ws_BusinessDirection = FB::utf8_to_wstring( BusinessDirection );
			userInfoMap[L"BusinessDirection"] = ws_BusinessDirection;

			string* p_BusinessProduct = p_userInfo->BusinessProduct; // BusinessProduct
			string BusinessProduct = (p_BusinessProduct==NULL) ? "" : *p_BusinessProduct;
			wstring ws_BusinessProduct = FB::utf8_to_wstring( BusinessProduct );
			userInfoMap[L"BusinessProduct"] = ws_BusinessProduct;

			string* p_ENBusinessProduct = p_userInfo->ENBusinessProduct; // ENBusinessProduct
			string ENBusinessProduct = (p_ENBusinessProduct==NULL) ? "" : *p_ENBusinessProduct;
			wstring ws_ENBusinessProduct = FB::utf8_to_wstring( ENBusinessProduct );
			userInfoMap[L"ENBusinessProduct"] = ws_ENBusinessProduct;

			string* p_BusinessBrand = p_userInfo->BusinessBrand; // BusinessBrand
			string BusinessBrand = (p_BusinessBrand==NULL) ? "" : *p_BusinessBrand;
			wstring ws_BusinessBrand = FB::utf8_to_wstring( BusinessBrand );
			userInfoMap[L"BusinessBrand"] = ws_BusinessBrand;

			time_t* p_CompanyEstablishDate = p_userInfo->CompanyEstablishDate; // CompanyEstablishDate
			time_t CompanyEstablishDate = (p_CompanyEstablishDate==NULL) ? NULL : *p_CompanyEstablishDate;
			string str_CompanyEstablishDate = "";
			if( FBB::Helper::UtilClass::timeToString(str_CompanyEstablishDate, CompanyEstablishDate)!=0 ){
				// return false;
			}
			wstring ws_CompanyEstablishDate = FB::utf8_to_wstring( str_CompanyEstablishDate );
			userInfoMap[L"CompanyEstablishDate"] = ws_CompanyEstablishDate;

			string* p_CompanyType = p_userInfo->CompanyType; // CompanyType
			string CompanyType = (p_CompanyType==NULL) ? "" : *p_CompanyType;
			wstring ws_CompanyType = FB::utf8_to_wstring( CompanyType );
			userInfoMap[L"CompanyType"] = ws_CompanyType;

			int* p_BusinessModelID = p_userInfo->BusinessModelID; // BusinessModelID
			int BusinessModelID = (p_BusinessModelID==NULL) ? 0 : *p_BusinessModelID;
			string str_BusinessModelID = FBB::Helper::UtilClass::longToString( BusinessModelID );
			wstring ws_BusinessModelID = FB::utf8_to_wstring( str_BusinessModelID );
			userInfoMap[L"BusinessModelID"] = ws_BusinessModelID;

			string* p_BusinessMode = p_userInfo->BusinessMode; // BusinessMode
			string BusinessMode = (p_BusinessMode==NULL) ? "" : *p_BusinessMode;
			wstring ws_BusinessMode = FB::utf8_to_wstring( BusinessMode );
			userInfoMap[L"BusinessMode"] = ws_BusinessMode;

			string* p_CompanyLocated = p_userInfo->CompanyLocated; // CompanyLocated
			string CompanyLocated = (p_CompanyLocated==NULL) ? "" : *p_CompanyLocated;
			wstring ws_CompanyLocated = FB::utf8_to_wstring( CompanyLocated );
			userInfoMap[L"CompanyLocated"] = ws_CompanyLocated;

			string* p_CompanyAddress = p_userInfo->CompanyAddress; // CompanyAddress
			string CompanyAddress = (p_CompanyAddress==NULL) ? "" : *p_CompanyAddress;
			wstring ws_CompanyAddress = FB::utf8_to_wstring( CompanyAddress );
			userInfoMap[L"CompanyAddress"] = ws_CompanyAddress;

			string* p_ENCompanyAddress = p_userInfo->ENCompanyAddress; // ENCompanyAddress
			string ENCompanyAddress = (p_ENCompanyAddress==NULL) ? "" : *p_ENCompanyAddress;
			wstring ws_ENCompanyAddress = FB::utf8_to_wstring( ENCompanyAddress );
			userInfoMap[L"ENCompanyAddress"] = ws_ENCompanyAddress;

			string* p_PostCode = p_userInfo->PostCode; // PostCode
			string PostCode = (p_PostCode==NULL) ? "" : *p_PostCode;
			wstring ws_PostCode = FB::utf8_to_wstring( PostCode );
			userInfoMap[L"PostCode"] = ws_PostCode;

			string* p_CompanyIntroduction = p_userInfo->CompanyIntroduction; // CompanyIntroduction
			string CompanyIntroduction = (p_CompanyIntroduction==NULL) ? "" : *p_CompanyIntroduction;
			wstring ws_CompanyIntroduction = FB::utf8_to_wstring( CompanyIntroduction );
			userInfoMap[L"CompanyIntroduction"] = ws_CompanyIntroduction;

			string* p_ENCompanyIntroduction = p_userInfo->ENCompanyIntroduction; // ENCompanyIntroduction
			string ENCompanyIntroduction = (p_ENCompanyIntroduction==NULL) ? "" : *p_ENCompanyIntroduction;
			wstring ws_ENCompanyIntroduction = FB::utf8_to_wstring( ENCompanyIntroduction );
			userInfoMap[L"ENCompanyIntroduction"] = ws_ENCompanyIntroduction;

			string* p_ProductIntroduce = p_userInfo->ProductIntroduce; // ProductIntroduce
			string ProductIntroduce = (p_ProductIntroduce==NULL) ? "" : *p_ProductIntroduce;
			wstring ws_ProductIntroduce = FB::utf8_to_wstring( ProductIntroduce );
			userInfoMap[L"ProductIntroduce"] = ws_ProductIntroduce;

			string* p_BusinessScope = p_userInfo->BusinessScope; // BusinessScope
			string BusinessScope = (p_BusinessScope==NULL) ? "" : *p_BusinessScope;
			wstring ws_BusinessScope = FB::utf8_to_wstring( BusinessScope );
			userInfoMap[L"BusinessScope"] = ws_BusinessScope;

			int* p_AuditState = p_userInfo->AuditState; // AuditState
			int AuditState = (p_AuditState==NULL) ? 0 : *p_AuditState;
			string str_AuditState = FBB::Helper::UtilClass::longToString( AuditState );
			wstring ws_AuditState = FB::utf8_to_wstring( str_AuditState );
			userInfoMap[L"AuditState"] = ws_AuditState;

			int* p_AgentID = p_userInfo->AgentID; // AgentID
			int AgentID = (p_AgentID==NULL) ? 0 : *p_AgentID;
			string str_AgentID = FBB::Helper::UtilClass::longToString( AgentID );
			wstring ws_AgentID = FB::utf8_to_wstring( str_AgentID );
			userInfoMap[L"AgentID"] = ws_AgentID;

			string* p_Balance = p_userInfo->Balance; // Balance
			string Balance = (p_Balance==NULL) ? "" : *p_Balance;
			wstring ws_Balance = FB::utf8_to_wstring( Balance );
			userInfoMap[L"Balance"] = ws_Balance;

			string* p_Mobile = p_userInfo->Mobile; // Mobile
			string Mobile = (p_Mobile==NULL) ? "" : *p_Mobile;
			wstring ws_Mobile = FB::utf8_to_wstring( Mobile );
			userInfoMap[L"Mobile"] = ws_Mobile;

			string* p_Name = p_userInfo->Name; // Name
			string Name = (p_Name==NULL) ? "" : *p_Name;
			wstring ws_Name = FB::utf8_to_wstring( Name );
			userInfoMap[L"Name"] = ws_Name;

			string* p_Department = p_userInfo->Department; // Department
			string Department = (p_Department==NULL) ? "" : *p_Department;
			wstring ws_Department = FB::utf8_to_wstring( Department );
			userInfoMap[L"Department"] = ws_Department;

			string* p_Position = p_userInfo->Position; // Position
			string Position = (p_Position==NULL) ? "" : *p_Position;
			wstring ws_Position = FB::utf8_to_wstring( Position );
			userInfoMap[L"Position"] = ws_Position;

			string* p_Telephone = p_userInfo->Telephone; // Telephone
			string Telephone = (p_Telephone==NULL) ? "" : *p_Telephone;
			wstring ws_Telephone = FB::utf8_to_wstring( Telephone );
			userInfoMap[L"Telephone"] = ws_Telephone;

			string* p_PhoneExtension = p_userInfo->PhoneExtension; // PhoneExtension
			string PhoneExtension = (p_PhoneExtension==NULL) ? "" : *p_PhoneExtension;
			wstring ws_PhoneExtension = FB::utf8_to_wstring( PhoneExtension );
			userInfoMap[L"PhoneExtension"] = ws_PhoneExtension;

			string* p_TelephoneAll = p_userInfo->TelephoneAll; // TelephoneAll
			string TelephoneAll = (p_TelephoneAll==NULL) ? "" : *p_TelephoneAll;
			wstring ws_TelephoneAll = FB::utf8_to_wstring( TelephoneAll );
			userInfoMap[L"TelephoneAll"] = ws_TelephoneAll;

			string* p_TelephoneAll1 = p_userInfo->TelephoneAll1; // TelephoneAll1
			string TelephoneAll1 = (p_TelephoneAll1==NULL) ? "" : *p_TelephoneAll1;
			wstring ws_TelephoneAll1 = FB::utf8_to_wstring( TelephoneAll1 );
			userInfoMap[L"TelephoneAll1"] = ws_TelephoneAll1;

			string* p_TelephoneAll2 = p_userInfo->TelephoneAll2; // TelephoneAll2
			string TelephoneAll2 = (p_TelephoneAll2==NULL) ? "" : *p_TelephoneAll2;
			wstring ws_TelephoneAll2 = FB::utf8_to_wstring( TelephoneAll2 );
			userInfoMap[L"TelephoneAll2"] = ws_TelephoneAll2;

			string* p_Fax = p_userInfo->Fax; // Fax
			string Fax = (p_Fax==NULL) ? "" : *p_Fax;
			wstring ws_Fax = FB::utf8_to_wstring( Fax );
			userInfoMap[L"Fax"] = ws_Fax;

			string* p_FaxexTension = p_userInfo->FaxexTension; // FaxexTension
			string FaxexTension = (p_FaxexTension==NULL) ? "" : *p_FaxexTension;
			wstring ws_FaxexTension = FB::utf8_to_wstring( FaxexTension );
			userInfoMap[L"FaxexTension"] = ws_FaxexTension;

			string* p_FaxAll = p_userInfo->FaxAll; // FaxAll
			string FaxAll = (p_FaxAll==NULL) ? "" : *p_FaxAll;
			wstring ws_FaxAll = FB::utf8_to_wstring( FaxAll );
			userInfoMap[L"FaxAll"] = ws_FaxAll;

			string* p_FaxAll1 = p_userInfo->FaxAll1; // FaxAll1
			string FaxAll1 = (p_FaxAll1==NULL) ? "" : *p_FaxAll1;
			wstring ws_FaxAll1 = FB::utf8_to_wstring( FaxAll1 );
			userInfoMap[L"FaxAll1"] = ws_FaxAll1;

			string* p_FaxAll2= p_userInfo->FaxAll2; // FaxAll2
			string FaxAll2 = (p_FaxAll2==NULL) ? "" : *p_FaxAll2;
			wstring ws_FaxAll2 = FB::utf8_to_wstring( FaxAll2 );
			userInfoMap[L"FaxAll2"] = ws_FaxAll2;

			string* p_OtherPhone = p_userInfo->OtherPhone; // OtherPhone
			string OtherPhone = (p_OtherPhone==NULL) ? "" : *p_OtherPhone;
			wstring ws_OtherPhone = FB::utf8_to_wstring( OtherPhone );
			userInfoMap[L"OtherPhone"] = ws_OtherPhone;

			string* p_Sex = p_userInfo->Sex; // Sex
			string Sex = (p_Sex==NULL) ? "" : *p_Sex;
			wstring ws_Sex = FB::utf8_to_wstring( Sex );
			userInfoMap[L"Sex"] = ws_Sex;

			string* p_Degree = p_userInfo->Degree; // Degree
			string Degree = (p_Degree==NULL) ? "" : *p_Degree;
			wstring ws_Degree = FB::utf8_to_wstring( Degree );
			userInfoMap[L"Degree"] = ws_Degree;

			string* p_IDCard = p_userInfo->IDCard; // IDCard
			string IDCard = (p_IDCard==NULL) ? "" : *p_IDCard;
			wstring ws_IDCard = FB::utf8_to_wstring( IDCard );
			userInfoMap[L"IDCard"] = ws_IDCard;

			time_t* p_Birthday = p_userInfo->Birthday; // Birthday
			time_t Birthday = (p_Birthday==NULL) ? NULL : *p_Birthday;
			if( Birthday==NULL )
				userInfoMap[L"Birthday"] = L"";
			else {
				string str_Birthday = "";
				if( FBB::Helper::UtilClass::timeToString(str_Birthday, Birthday)!=0 ){
				// return false;
				}
				wstring ws_Birthday = FB::utf8_to_wstring( str_Birthday );
				userInfoMap[L"Birthday"] = ws_Birthday;
			}

			string* p_MobilePhone = p_userInfo->MobilePhone; // MobilePhone
			string MobilePhone = (p_MobilePhone==NULL) ? "" : *p_MobilePhone;
			wstring ws_MobilePhone = FB::utf8_to_wstring( MobilePhone );
			userInfoMap[L"MobilePhone"] = ws_MobilePhone;

			string* p_Code = p_userInfo->Code; // Code
			string Code = (p_Code==NULL) ? "" : *p_Code;
			wstring ws_Code = FB::utf8_to_wstring( Code );
			userInfoMap[L"Code"] = ws_Code;

			string* p_QQ = p_userInfo->QQ; // QQ
			string QQ = (p_QQ==NULL) ? "" : *p_QQ;
			wstring ws_QQ = FB::utf8_to_wstring( QQ );
			userInfoMap[L"QQ"] = ws_QQ;

			string* p_MSN = p_userInfo->MSN; // MSN
			string MSN = (p_MSN==NULL) ? "" : *p_MSN;
			wstring ws_MSN = FB::utf8_to_wstring( MSN );
			userInfoMap[L"MSN"] = ws_MSN;

			string* p_UserEmail = p_userInfo->UserEmail; // UserEmail
			string UserEmail = (p_UserEmail==NULL) ? "" : *p_UserEmail;
			wstring ws_UserEmail = FB::utf8_to_wstring( UserEmail );
			userInfoMap[L"UserEmail"] = ws_UserEmail;

			string* p_UserEmailPassword = p_userInfo->UserEmailPassword; // UserEmailPassword
			string UserEmailPassword = (p_UserEmailPassword==NULL) ? "" : *p_UserEmailPassword;
			wstring ws_UserEmailPassword = FB::utf8_to_wstring( UserEmailPassword );
			userInfoMap[L"UserEmailPassword"] = ws_UserEmailPassword;

			string* p_Province = p_userInfo->Province; // Province
			string Province = (p_Province==NULL) ? "" : *p_Province;
			wstring ws_Province = FB::utf8_to_wstring( Province );
			userInfoMap[L"Province"] = ws_Province;

			string* p_City = p_userInfo->City; // City
			string City = (p_City==NULL) ? "" : *p_City;
			wstring ws_City = FB::utf8_to_wstring( City );
			userInfoMap[L"City"] = ws_City;

			string* p_County = p_userInfo->County; // County
			string County = (p_County==NULL) ? "" : *p_County;
			wstring ws_County = FB::utf8_to_wstring( County );
			userInfoMap[L"County"] = ws_County;

			int* p_BusinessIndustryID = p_userInfo->BusinessIndustryID; // BusinessIndustryID
			int BusinessIndustryID = (p_BusinessIndustryID==NULL) ? 0 : *p_BusinessIndustryID;
			string str_BusinessIndustryID = FBB::Helper::UtilClass::longToString( BusinessIndustryID );
			wstring ws_BusinessIndustryID = FB::utf8_to_wstring( str_BusinessIndustryID );
			userInfoMap[L"BusinessIndustryID"] = ws_BusinessIndustryID;

			string* p_LegalRepresentativeName = p_userInfo->LegalRepresentativeName; // LegalRepresentativeName
			string LegalRepresentativeName = (p_LegalRepresentativeName==NULL) ? "" : *p_LegalRepresentativeName;
			wstring ws_LegalRepresentativeName = FB::utf8_to_wstring( LegalRepresentativeName );
			userInfoMap[L"LegalRepresentativeName"] = ws_LegalRepresentativeName;

			string* p_BusinessLicenseNumber = p_userInfo->BusinessLicenseNumber; // BusinessLicenseNumber
			string BusinessLicenseNumber = (p_BusinessLicenseNumber==NULL) ? "" : *p_BusinessLicenseNumber;
			wstring ws_BusinessLicenseNumber = FB::utf8_to_wstring( BusinessLicenseNumber );
			userInfoMap[L"BusinessLicenseNumber"] = ws_BusinessLicenseNumber;

			string* p_WebSiteUrl = p_userInfo->WebSiteUrl; // WebSiteUrl
			string WebSiteUrl = (p_WebSiteUrl==NULL) ? "" : *p_WebSiteUrl;
			wstring ws_WebSiteUrl = FB::utf8_to_wstring( WebSiteUrl );
			userInfoMap[L"WebSiteUrl"] = ws_WebSiteUrl;

			// PostProUser 
			// generate by FBB Plugin, not through web service
			struct tm* timeinfo = localtime( p_CompanyEstablishDate ); // CompanyEstablishDate_Year
			long year = timeinfo->tm_year+1900;
			if( year==1970 ) // p_CompanyEstablishDate equals NULL
				year = 2010;
			string CompanyEstablishDate_Year = FBB::Helper::UtilClass::longToString( year );
			wstring ws_CompanyEstablishDate_Year = FB::utf8_to_wstring( CompanyEstablishDate_Year );
			userInfoMap[L"CompanyEstablishDate_Year"] = ws_CompanyEstablishDate_Year;

			string UserEmailUserName = "";
			string UserEmailDomainName = "";
			if( !UserEmail.empty()&&UserEmail!="" ) { // UserEmailUserName & UserEmailDomainName
				std::vector<string> v = FBB::Helper::UtilClass::split(UserEmail, '@');
				if( v.size()==2 ) {
					UserEmailUserName = v[0];
					UserEmailDomainName = v[1];
				}
			}
			wstring ws_UserEmailUserName = FB::utf8_to_wstring(UserEmailUserName);
			wstring ws_UserEmailDomainName = FB::utf8_to_wstring(UserEmailDomainName);
			userInfoMap[L"UserEmailUserName"] = ws_UserEmailUserName;
			userInfoMap[L"UserEmailDomainName"] = ws_UserEmailDomainName;

			if( !CompanyShortName.empty()&&CompanyShortName!="" ) { // CompanyShortName
				std::vector<string> v = FBB::Helper::UtilClass::split(CompanyShortName, ',');
				if( v.size()>0 )
					CompanyShortName = v[0];
			}
			wstring ws_CompanyShortName_post = FB::utf8_to_wstring( CompanyShortName );
			userInfoMap[L"CompanyShortName"] = ws_CompanyShortName_post;

			// MobilePhone-->Mobile Code-->PostCode
			userInfoMap[L"Mobile"] = userInfoMap[L"MobilePhone"];
			// userInfoMap[L"PostCode"] = userInfoMap[L"Code"];

			userInfoMap[L"TelephoneAll"] = userInfoMap[L"Code"]+userInfoMap[L"Telephone"];
			userInfoMap[L"TelephoneAll1"] = userInfoMap[L"Code"]+L"-"+userInfoMap[L"Telephone"];
			userInfoMap[L"TelephoneAll2"] = userInfoMap[L"Code"]+L" "+userInfoMap[L"Telephone"];
			userInfoMap[L"FaxAll"] = userInfoMap[L"Code"]+userInfoMap[L"Fax"];
			userInfoMap[L"FaxAll1"] = userInfoMap[L"Code"]+L"-"+userInfoMap[L"Fax"];
			userInfoMap[L"FaxAll2"] = userInfoMap[L"Code"]+L" "+userInfoMap[L"Fax"];
		}

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req;
		delete resp;

		return true;
	}

}

wstring stringPointerToWstring(string* initialPointer) {
	string initialString = (initialPointer==NULL) ? "" : *initialPointer;
	return FB::utf8_to_wstring( initialString ); 
}

wstring boolToWstring(bool initialValue) {
	return (initialValue==true) ? L"true" : L"false";
}

wstring intPointerToWstring(int* initialPointer) {
	int initialValue = (initialPointer==NULL) ? 0 : *initialPointer;
	string initialString = FBB::Helper::UtilClass::longToString( initialValue );
	return FB::utf8_to_wstring( initialString );
}

wstring intToWstring(int initialValue) {
	string initialString = FBB::Helper::UtilClass::longToString( initialValue );
	return FB::utf8_to_wstring( initialString );
}

wstring timePointerToWstring(time_t* initialPointer) {
	time_t initialValue = (initialPointer==NULL) ? NULL : *initialPointer;
	string str_time = "";
	FBB::Helper::UtilClass::timeToString(str_time, initialValue);
	return FB::utf8_to_wstring( str_time );
}

bool FBB::WS::WSClientClass::loadPublishEntity(long PMID, long SiteID, map<wstring, wstring>& publishEntityInfoMap)
{ 
	boost::this_thread::interruption_point();

	string str_PMID = FBB::Helper::UtilClass::longToString( PMID );
	string str_SiteID = FBB::Helper::UtilClass::longToString( SiteID );
	
	ws_USCOREInterfaceSoapProxy userInfoProxy(SOAP_C_UTFSTRING);
	_ns2__GetPublishByTerm* req = new _ns2__GetPublishByTerm();
	// req->ah = new ns2__AuthHeader();
	// req->ah->UserName = new std::string("zhendao");
	// req->ah->PassWord = new std::string("zhendaofabu");
	req->PMID = &str_PMID;
	req->SiteID = &str_SiteID;


	_ns2__GetPublishByTermResponse* resp = new _ns2__GetPublishByTermResponse();

	int flag = userInfoProxy.GetPublishByTerm_(req, resp);
	bool result = false;
	if( userInfoProxy.error ){
		userInfoProxy.soap_stream_fault(std::cerr);
		result = false;
	} else {
		ns2__View_USCOREPublish* p_publishEntityInfo = resp->GetPublishByTermResult;

		if( p_publishEntityInfo==NULL ) {
			result = false;
		} else {
			publishEntityInfoMap[L"LoginName"] = stringPointerToWstring( p_publishEntityInfo->LoginName );
			publishEntityInfoMap[L"Password"] = stringPointerToWstring( p_publishEntityInfo->Password );
			publishEntityInfoMap[L"HasProduct"] = boolToWstring( p_publishEntityInfo->HasProduct );
			publishEntityInfoMap[L"SiteId"] = intPointerToWstring( p_publishEntityInfo->SiteId );
			publishEntityInfoMap[L"ProductDirectoryID"] = intPointerToWstring( p_publishEntityInfo->ProductDirectoryID );
			publishEntityInfoMap[L"Title"] = stringPointerToWstring( p_publishEntityInfo->Title );
			publishEntityInfoMap[L"SeoKeyWord"] = stringPointerToWstring( p_publishEntityInfo->SeoKeyWord );
			publishEntityInfoMap[L"ProductClassID"] = intPointerToWstring( p_publishEntityInfo->ProductClassID );
			publishEntityInfoMap[L"ProductIntroduce"] = stringPointerToWstring( p_publishEntityInfo->ProductIntroduce );
			publishEntityInfoMap[L"ProductAttribute"] = stringPointerToWstring( p_publishEntityInfo->ProductAttribute );
			publishEntityInfoMap[L"SmallImageUrlList"] = stringPointerToWstring( p_publishEntityInfo->SmallImageUrlList );
			publishEntityInfoMap[L"BigImageUrlList"] = stringPointerToWstring( p_publishEntityInfo->BigImageUrlList );
			publishEntityInfoMap[L"ProductLink"] = stringPointerToWstring( p_publishEntityInfo->ProductLink );
			publishEntityInfoMap[L"Price"] = stringPointerToWstring( p_publishEntityInfo->Price );
			publishEntityInfoMap[L"ProductStandard"] = stringPointerToWstring( p_publishEntityInfo->ProductStandard );
			publishEntityInfoMap[L"ProductModel"] = stringPointerToWstring( p_publishEntityInfo->ProductModel );
			publishEntityInfoMap[L"ProductionPlace"] = stringPointerToWstring( p_publishEntityInfo->ProductionPlace );
			publishEntityInfoMap[L"ProductQuantity"] = stringPointerToWstring( p_publishEntityInfo->ProductQuantity );
			publishEntityInfoMap[L"ProductBrand"] = stringPointerToWstring( p_publishEntityInfo->ProductBrand );
			publishEntityInfoMap[L"CustomMade"] = boolToWstring( p_publishEntityInfo->CustomMade );
			publishEntityInfoMap[L"MQQ"] = stringPointerToWstring( p_publishEntityInfo->MQQ );
			publishEntityInfoMap[L"PackingDesrciption"] = stringPointerToWstring( p_publishEntityInfo->PackingDesrciption );
			publishEntityInfoMap[L"UOM"] = stringPointerToWstring( p_publishEntityInfo->UOM );
			publishEntityInfoMap[L"MPID"] = intToWstring( p_publishEntityInfo->MPID );
			publishEntityInfoMap[L"MPTitle"] = stringPointerToWstring( p_publishEntityInfo->MPTitle );
			publishEntityInfoMap[L"MPProductName"] = stringPointerToWstring( p_publishEntityInfo->MPProductName );
			publishEntityInfoMap[L"MPDescription"] = stringPointerToWstring( p_publishEntityInfo->MPDescription );
			publishEntityInfoMap[L"MPKeyWords"] = stringPointerToWstring( p_publishEntityInfo->MPKeyWords );
			publishEntityInfoMap[L"ID"] = intToWstring( p_publishEntityInfo->ID );
			publishEntityInfoMap[L"LoginNameReg"] = stringPointerToWstring( p_publishEntityInfo->LoginNameReg );
			publishEntityInfoMap[L"LoginPassword"] = stringPointerToWstring( p_publishEntityInfo->LoginPassword );
			publishEntityInfoMap[L"Email"] = stringPointerToWstring( p_publishEntityInfo->Email );
			publishEntityInfoMap[L"EmailUserName"] = stringPointerToWstring( p_publishEntityInfo->EmailUserName );
			publishEntityInfoMap[L"EmailDomainName"] = stringPointerToWstring( p_publishEntityInfo->EmailDomainName );
			publishEntityInfoMap[L"CompanyName"] = stringPointerToWstring( p_publishEntityInfo->CompanyName );
			publishEntityInfoMap[L"ENCompanyName"] = stringPointerToWstring( p_publishEntityInfo->ENCompanyName );
			publishEntityInfoMap[L"CompanyShortName"] = stringPointerToWstring( p_publishEntityInfo->CompanyShortName );
			publishEntityInfoMap[L"BusinessIndustry"] = stringPointerToWstring( p_publishEntityInfo->BusinessIndustry );
			publishEntityInfoMap[L"BusinessDirectionID"] = intPointerToWstring( p_publishEntityInfo->BusinessDirectionID );
			publishEntityInfoMap[L"BusinessDirection"] = stringPointerToWstring( p_publishEntityInfo->BusinessDirection );
			publishEntityInfoMap[L"BusinessProduct"] = stringPointerToWstring( p_publishEntityInfo->BusinessProduct );
			publishEntityInfoMap[L"ENBusinessProduct"] = stringPointerToWstring( p_publishEntityInfo->ENBusinessProduct );
			publishEntityInfoMap[L"BusinessBrand"] = stringPointerToWstring( p_publishEntityInfo->BusinessBrand );
			publishEntityInfoMap[L"CompanyEstablishDate"] = timePointerToWstring( p_publishEntityInfo->CompanyEstablishDate );
			publishEntityInfoMap[L"CompanyType"] = stringPointerToWstring( p_publishEntityInfo->CompanyType );
			publishEntityInfoMap[L"BusinessModelID"] = intPointerToWstring( p_publishEntityInfo->BusinessModelID );
			publishEntityInfoMap[L"BusinessMode"] = stringPointerToWstring( p_publishEntityInfo->BusinessMode );
			publishEntityInfoMap[L"CompanyLocated"] = stringPointerToWstring( p_publishEntityInfo->CompanyLocated );
			publishEntityInfoMap[L"CompanyAddress"] = stringPointerToWstring( p_publishEntityInfo->CompanyAddress );
			publishEntityInfoMap[L"PostCode"] = stringPointerToWstring( p_publishEntityInfo->PostCode );
			publishEntityInfoMap[L"CompanyIntroduction"] = stringPointerToWstring( p_publishEntityInfo->CompanyIntroduction );
			publishEntityInfoMap[L"ENCompanyIntroduction"] = stringPointerToWstring( p_publishEntityInfo->ENCompanyIntroduction );
			publishEntityInfoMap[L"CompanyProductIntroduce"] = stringPointerToWstring( p_publishEntityInfo->CompanyProductIntroduce );
			publishEntityInfoMap[L"BusinessScope"] = stringPointerToWstring( p_publishEntityInfo->BusinessScope );
			publishEntityInfoMap[L"AuditState"] = intPointerToWstring( p_publishEntityInfo->AuditState );
			publishEntityInfoMap[L"AgentID"] = intPointerToWstring( p_publishEntityInfo->AgentID );
			publishEntityInfoMap[L"Balance"] = stringPointerToWstring( p_publishEntityInfo->Balance );
			publishEntityInfoMap[L"BusinessScope"] = stringPointerToWstring( p_publishEntityInfo->BusinessScope );
			publishEntityInfoMap[L"Mobile"] = stringPointerToWstring( p_publishEntityInfo->Mobile );
			publishEntityInfoMap[L"Province"] = stringPointerToWstring( p_publishEntityInfo->Province );
			publishEntityInfoMap[L"City"] = stringPointerToWstring( p_publishEntityInfo->City );
			publishEntityInfoMap[L"County"] = stringPointerToWstring( p_publishEntityInfo->County );
			publishEntityInfoMap[L"WebSiteUrl"] = stringPointerToWstring( p_publishEntityInfo->WebSiteUrl );
			publishEntityInfoMap[L"Name"] = stringPointerToWstring( p_publishEntityInfo->Name );
			publishEntityInfoMap[L"Department"] = stringPointerToWstring( p_publishEntityInfo->Department );
			publishEntityInfoMap[L"Position"] = stringPointerToWstring( p_publishEntityInfo->Position );
			publishEntityInfoMap[L"Telephone"] = stringPointerToWstring( p_publishEntityInfo->Telephone );
			publishEntityInfoMap[L"PhoneExtension"] = stringPointerToWstring( p_publishEntityInfo->PhoneExtension );
			publishEntityInfoMap[L"TelephoneAll"] = stringPointerToWstring( p_publishEntityInfo->TelephoneAll );
			publishEntityInfoMap[L"TelephoneAll1"] = stringPointerToWstring( p_publishEntityInfo->TelephoneAll1 );
			publishEntityInfoMap[L"TelephoneAll2"] = stringPointerToWstring( p_publishEntityInfo->TelephoneAll2 );
			publishEntityInfoMap[L"Fax"] = stringPointerToWstring( p_publishEntityInfo->Fax );
			publishEntityInfoMap[L"FaxexTension"] = stringPointerToWstring( p_publishEntityInfo->FaxexTension );
			publishEntityInfoMap[L"FaxAll"] = stringPointerToWstring( p_publishEntityInfo->FaxAll );
			publishEntityInfoMap[L"FaxAll1"] = stringPointerToWstring( p_publishEntityInfo->FaxAll1 );
			publishEntityInfoMap[L"FaxAll2"] = stringPointerToWstring( p_publishEntityInfo->FaxAll2 );
			publishEntityInfoMap[L"OtherPhone"] = stringPointerToWstring( p_publishEntityInfo->OtherPhone );
			publishEntityInfoMap[L"Sex"] = stringPointerToWstring( p_publishEntityInfo->Sex );
			publishEntityInfoMap[L"Degree"] = stringPointerToWstring( p_publishEntityInfo->Degree );
			publishEntityInfoMap[L"IDCard"] = stringPointerToWstring( p_publishEntityInfo->IDCard );
			publishEntityInfoMap[L"Birthday"] = timePointerToWstring( p_publishEntityInfo->Birthday );
			publishEntityInfoMap[L"MobilePhone"] = stringPointerToWstring( p_publishEntityInfo->MobilePhone );
			publishEntityInfoMap[L"Code"] = stringPointerToWstring( p_publishEntityInfo->Code );
			publishEntityInfoMap[L"QQ"] = stringPointerToWstring( p_publishEntityInfo->QQ );
			publishEntityInfoMap[L"MSN"] = stringPointerToWstring( p_publishEntityInfo->MSN );
			publishEntityInfoMap[L"UserEmail"] = stringPointerToWstring( p_publishEntityInfo->UserEmail );
			publishEntityInfoMap[L"UserEmailPassword"] = stringPointerToWstring( p_publishEntityInfo->UserEmailPassword );
			publishEntityInfoMap[L"BusinessIndustryID"] = intPointerToWstring( p_publishEntityInfo->BusinessIndustryID );
			publishEntityInfoMap[L"LegalRepresentativeName"] = stringPointerToWstring( p_publishEntityInfo->LegalRepresentativeName );
			publishEntityInfoMap[L"BusinessLicenseNumber"] = stringPointerToWstring( p_publishEntityInfo->BusinessLicenseNumber );

			result = true;
		} 
	}

	// delete req->ah->UserName;
	// delete req->ah->PassWord;
	// delete req->ah;
	delete req;
	delete resp;

	return result;
}

#pragma region BusinessDirection
bool myBusinessDireCustomMap(FBB::Entity::MyBusinessDire* myBusinessDire, ns1__MyBusinessDire* mymode){
	long id = mymode->id;
	myBusinessDire->id = id;

	string* p_title = mymode->title;
	myBusinessDire->title = (p_title==NULL) ? "" : *p_title;

	return true;
}

bool buiDirePairCustomMap(vector<FBB::Entity::BusinessDirePair>& buiDirePairVector, std::vector<class ns1__BusinessDirePair * >& BusinessDirePair){
	vector< ns1__BusinessDirePair* >::const_iterator it = BusinessDirePair.begin();

	while( it!=BusinessDirePair.end() ){
		FBB::Entity::OneOptRegEntity* oneOptRegEntity = new FBB::Entity::OneOptRegEntity();
		oneOptRegCustomMap(oneOptRegEntity, (*it)->oor);
		FBB::Entity::MyBusinessDire* myBusinessDire = new FBB::Entity::MyBusinessDire();
		myBusinessDireCustomMap(myBusinessDire, (*it)->mymode);

		FBB::Entity::BusinessDirePair* businessDirePair = new FBB::Entity::BusinessDirePair();
		businessDirePair->mymode = *myBusinessDire;
		businessDirePair->oor = *oneOptRegEntity;
		buiDirePairVector.push_back( *businessDirePair );

		it++;

		delete oneOptRegEntity;
		delete myBusinessDire;
		delete businessDirePair;
	}

	return true;
}

bool buiDireCustomMap(ns1__BusinessDirectionPara* ns1_buiDire,  map<long, map<long, FBB::Entity::BusinessDirectionPara> >& siteBuiDireMap){
	FBB::Entity::BusinessDirectionPara* buiDire = new FBB::Entity::BusinessDirectionPara();
	long siteId = ns1_buiDire->siteID;
	long type = ns1_buiDire->Type;
	
	buiDire->siteID = siteId;
	buiDire->Type = type;
	string* p_Url = ns1_buiDire->Url;
	buiDire->Url = (p_Url==NULL) ? "" : *p_Url;

	//  BusinessDirePair
	vector<FBB::Entity::BusinessDirePair> buiDirePairVector;
	buiDirePairCustomMap(buiDirePairVector, ns1_buiDire->allbms->BusinessDirePair);
	buiDire->allbms = buiDirePairVector;

	buiDire->Valid = ns1_buiDire->Valid;
	time_t* p_ValidTime = ns1_buiDire->ValidTime;
	buiDire->ValidTime = (p_ValidTime==NULL) ? NULL : *p_ValidTime;

	map<long, FBB::Entity::BusinessDirectionPara> pageTypeMap;
	pageTypeMap[type] = *buiDire;
	siteBuiDireMap[siteId] = pageTypeMap;	

	delete buiDire;

	return true;
}

bool FBB::WS::WSClientClass::loadBuiDireMap(int siteId, map<long, FBB::Entity::BusinessDirectionPara>& pageBuiDireMap) {
	boost::this_thread::interruption_point();

	std::vector<int> siteIds;
	siteIds.push_back( siteId );
	map<long, map<long, FBB::Entity::BusinessDirectionPara> > siteBuiDireMap;

	loadBuiDireMap(siteIds, siteBuiDireMap);

	if( siteBuiDireMap.find(siteId)!=siteBuiDireMap.end() )
		pageBuiDireMap = siteBuiDireMap[siteId];
	else
		throw FBB::Exception::FBBException("siteId: " + FBB::Helper::UtilClass::longToString(siteId) + " load pageBuiDireMap failed");

	return true;
}

bool FBB::WS::WSClientClass::loadBuiDireMap(vector<int> siteIds, map<long, map<long, FBB::Entity::BusinessDirectionPara> >& siteBuiDireMap)
{ 
	boost::this_thread::interruption_point();

	WebServiceFabuParaSoapProxy buiDireMapProxy(SOAP_C_UTFSTRING);

	_ns1__GetBuiDireMapBySiteIDs* req = new _ns1__GetBuiDireMapBySiteIDs();
	req->ah = new ns1__AuthHeader();
	req->ah->UserName = new std::string("zhendao");
	req->ah->PassWord = new std::string("zhendaofabu");

	req->siteindex = new ns1__ArrayOfInt();
	req->siteindex->int_ = siteIds;

	_ns1__GetBuiDireMapBySiteIDsResponse* resp = new _ns1__GetBuiDireMapBySiteIDsResponse();

	int flag = buiDireMapProxy.GetBuiDireMapBySiteIDs_(req, resp);
	if( buiDireMapProxy.error ){
		buiDireMapProxy.soap_stream_fault(std::cerr);

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req->siteindex;
		delete req;
		delete resp;

		return false;
	} else {
		if( resp->allregs->BusinessDirectionPara.size()==0 ){ return true; } // Õ¾µãÃ»ÓÐ¶ÔÓ¦BusinessDirectionÊý¾Ý

		vector<class ns1__BusinessDirectionPara * >::const_iterator it = resp->allregs->BusinessDirectionPara.begin();
		if( *it==NULL ){ return false; } // Ö÷Óª·½Ïò²ÎÊýÎª¿Õ
		
		while( it!=resp->allregs->BusinessDirectionPara.end() ){
			buiDireCustomMap(*it, siteBuiDireMap);

			it++;
		}

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req->siteindex;
		delete req;
		delete resp;

		return true;
	}

}
#pragma endregion

#pragma region BusinessMode
bool myBusinessModeCustomMap(FBB::Entity::MyBusinessMode* myBusinessMode, ns1__MyBusinessMode* mymode){
	long id = mymode->id;
	myBusinessMode->id = id;

	string* p_title = mymode->title;
	myBusinessMode->title = (p_title==NULL) ? "" : *p_title;

	return true;
}

bool buiModePairCustomMap(vector<FBB::Entity::BusinessModePair>& buiModePairVector, std::vector<class ns1__BusinessModePair * >& BusinessModePair){
	vector< ns1__BusinessModePair* >::const_iterator it = BusinessModePair.begin();

	while( it!=BusinessModePair.end() ){
		FBB::Entity::OneOptRegEntity* oneOptRegEntity = new FBB::Entity::OneOptRegEntity();
		oneOptRegCustomMap(oneOptRegEntity, (*it)->oor);
		FBB::Entity::MyBusinessMode* myBusinessMode = new FBB::Entity::MyBusinessMode();
		myBusinessModeCustomMap(myBusinessMode, (*it)->mymode);

		FBB::Entity::BusinessModePair* businessModePair = new FBB::Entity::BusinessModePair();
		businessModePair->mymode = *myBusinessMode;
		businessModePair->oor = *oneOptRegEntity;
		buiModePairVector.push_back( *businessModePair );

		it++;

		delete oneOptRegEntity;
		delete myBusinessMode;
		delete businessModePair;
	}

	return true;
}

bool buiModeCustomMap(ns1__BusinessModePara* ns1_buiMode,  map<long, map<long, FBB::Entity::BusinessModePara> >& siteBuiModeMap){
	FBB::Entity::BusinessModePara* buiMode = new FBB::Entity::BusinessModePara();
	long siteId = ns1_buiMode->siteID;
	long type = ns1_buiMode->Type;
	
	buiMode->siteID = siteId;
	buiMode->Type = type;
	string* p_Url = ns1_buiMode->Url;
	buiMode->Url = (p_Url==NULL) ? "" : *p_Url;

	//  BusinessDirePair
	vector<FBB::Entity::BusinessModePair> buiModePairVector;
	buiModePairCustomMap(buiModePairVector, ns1_buiMode->allbms->BusinessModePair);
	buiMode->allbms = buiModePairVector;

	buiMode->Valid = ns1_buiMode->Valid;
	time_t* p_ValidTime = ns1_buiMode->ValidTime;
	buiMode->ValidTime = (p_ValidTime==NULL) ? NULL : *p_ValidTime;

	map<long, FBB::Entity::BusinessModePara> pageTypeMap;
	pageTypeMap[type] = *buiMode;
	siteBuiModeMap[siteId] = pageTypeMap;		

	delete buiMode;

	return true;
}

bool FBB::WS::WSClientClass::loadBuiModeMap(int siteId, map<long, FBB::Entity::BusinessModePara>& pageBuiModeMap) {
	boost::this_thread::interruption_point();

	std::vector<int> siteIds;
	siteIds.push_back( siteId );
	map<long, map<long, FBB::Entity::BusinessModePara> > siteBuiModeMap;

	loadBuiModeMap(siteIds, siteBuiModeMap);

	if( siteBuiModeMap.find(siteId)!=siteBuiModeMap.end() )
		pageBuiModeMap = siteBuiModeMap[siteId];
	else
		throw FBB::Exception::FBBException("siteId: " + FBB::Helper::UtilClass::longToString(siteId) + " load pageBuiModeMap failed");

	return true;
}

bool FBB::WS::WSClientClass::loadBuiModeMap(vector<int> siteIds, map<long, map<long, FBB::Entity::BusinessModePara> >& siteBuiModeMap)
{ 
	boost::this_thread::interruption_point();

	WebServiceFabuParaSoapProxy buiDireModeProxy(SOAP_C_UTFSTRING);

	_ns1__GetBuiModeMapBySiteIDs* req = new _ns1__GetBuiModeMapBySiteIDs();
	req->ah = new ns1__AuthHeader();
	req->ah->UserName = new std::string("zhendao");
	req->ah->PassWord = new std::string("zhendaofabu");

	req->siteindex = new ns1__ArrayOfInt();
	req->siteindex->int_ = siteIds;

	_ns1__GetBuiModeMapBySiteIDsResponse* resp = new _ns1__GetBuiModeMapBySiteIDsResponse();

	int flag = buiDireModeProxy.GetBuiModeMapBySiteIDs(req, resp);
	if( buiDireModeProxy.error ){
		buiDireModeProxy.soap_stream_fault(std::cerr);

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req->siteindex;
		delete req;
		delete resp;

		return false;
	} else {
		if( resp->allregs->BusinessModePara.size()==0 ){ return true; } // Õ¾µãÃ»ÓÐ¶ÔÓ¦BusinessModeÊý¾Ý

		vector<class ns1__BusinessModePara * >::const_iterator it = resp->allregs->BusinessModePara.begin();
		if( *it==NULL ){ return false; } // ¾­ÓªÄ£Ê½²ÎÊýÎª¿Õ
		
		while( it!=resp->allregs->BusinessModePara.end() ){
			buiModeCustomMap(*it, siteBuiModeMap);

			it++;
		}

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req->siteindex;
		delete req;
		delete resp;

		return true;
	}

}
#pragma endregion

#pragma region ProDireMap
bool myProDireCustomMap(FBB::Entity::MyProDire* myProDire, ns1__MyProDire* mymode){
	myProDire->id = mymode->id;

	string* p_title = mymode->title;
	myProDire->title = (p_title==NULL) ? "" : *p_title;

	myProDire->parentid = *(mymode->parentid);

	return true;
}

bool proDireCustomMap(vector<FBB::Entity::ProDirePair>& proDirePairVector, std::vector<class ns1__ProDirePair * >& ProDirePair) {
	vector< ns1__ProDirePair* >::const_iterator it = ProDirePair.begin();

	while( it!=ProDirePair.end() ){
		vector<FBB::Entity::OneOptRegEntity> oneOptRegVector;
		oneOptRegCustomMap(oneOptRegVector, (*it)->oors->OneOptReg);
		FBB::Entity::MyProDire* myProDire = new FBB::Entity::MyProDire();
		myProDireCustomMap(myProDire, (*it)->mymode);

		FBB::Entity::ProDirePair* proDirePair = new FBB::Entity::ProDirePair();
		proDirePair->mymode = *myProDire;
		proDirePair->oors = oneOptRegVector;
		proDirePairVector.push_back( *proDirePair );

		it++;

		delete myProDire;
		delete proDirePair;
	}

	return true;
}

bool proDireCustomMap(
	ns1__ProductDirectoryPara* ns1_proDirePara,  
	map<long, map<long, FBB::Entity::ProductDirectoryPara> >& proDireMap)
{
	FBB::Entity::ProductDirectoryPara* proDirePara = new FBB::Entity::ProductDirectoryPara();
	long siteId = ns1_proDirePara->siteID;
	long type = ns1_proDirePara->Type;
	
	proDirePara->siteID = siteId;
	proDirePara->Type = type;
	string* p_Url = ns1_proDirePara->Url;
	proDirePara->Url = (p_Url==NULL) ? "" : *p_Url;

	//  ProDirePair
	vector<FBB::Entity::ProDirePair> proDirePairVector;
	proDireCustomMap(proDirePairVector, ns1_proDirePara->allbms->ProDirePair);
	proDirePara->allbms = proDirePairVector;

	proDirePara->Valid = ns1_proDirePara->Valid;
	time_t* p_ValidTime = ns1_proDirePara->ValidTime;
	proDirePara->ValidTime = (p_ValidTime==NULL) ? NULL : *p_ValidTime;

	map<long, FBB::Entity::ProductDirectoryPara> pageTypeMap;
	pageTypeMap[type] = *proDirePara;
	proDireMap[siteId] = pageTypeMap;		

	delete proDirePara;

	return true;
}

bool FBB::WS::WSClientClass::loadProDireMap(vector<int> siteIds, map<long, map<long, FBB::Entity::ProductDirectoryPara> >& proDireMap)
{ 
	boost::this_thread::interruption_point();

	WebServiceFabuParaSoapProxy proDireProxy(SOAP_C_UTFSTRING);

	_ns1__GetProDireMapBySiteIDs* req = new _ns1__GetProDireMapBySiteIDs();
	req->ah = new ns1__AuthHeader();
	req->ah->UserName = new std::string("zhendao");
	req->ah->PassWord = new std::string("zhendaofabu");

	req->siteindex = new ns1__ArrayOfInt();
	req->siteindex->int_ = siteIds;

	_ns1__GetProDireMapBySiteIDsResponse* resp = new _ns1__GetProDireMapBySiteIDsResponse();

	int flag = proDireProxy.GetProDireMapBySiteIDs(req, resp);
	if( proDireProxy.error ){
		proDireProxy.soap_stream_fault(std::cerr);

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req->siteindex;
		delete req;
		delete resp;

		return false;
	} else {
		if( resp->allregs->ProductDirectoryPara.size()==0 )
			return true;

		vector<class ns1__ProductDirectoryPara * >::const_iterator it = resp->allregs->ProductDirectoryPara.begin();
		if( *it==NULL )
			return false;
		
		while( it!=resp->allregs->ProductDirectoryPara.end() ){
			proDireCustomMap(*it, proDireMap);

			it++;
		}

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req->siteindex;
		delete req;
		delete resp;

		return true;
	}

}

bool FBB::WS::WSClientClass::getProDireMapOptBySiteIDandPageType(
	long siteId, long pageType, long BIID, 
	FBB::Entity::ProductDirectoryPara& proDirePara)
{ 
	WebServiceFabuParaSoapProxy proDireOptProxy(SOAP_C_UTFSTRING);

	_ns1__GetProDireMapOptBySiteIDandPageType* req = new _ns1__GetProDireMapOptBySiteIDandPageType();
	req->ah = new ns1__AuthHeader();
	req->ah->UserName = new std::string("zhendao");
	req->ah->PassWord = new std::string("zhendaofabu");

	req->siteid = siteId;
	req->pageType = pageType;
	req->BIID = BIID;

	_ns1__GetProDireMapOptBySiteIDandPageTypeResponse* resp = new _ns1__GetProDireMapOptBySiteIDandPageTypeResponse();

	int flag = proDireOptProxy.GetProDireMapOptBySiteIDandPageType_(req, resp);
	if( proDireOptProxy.error ){
		proDireOptProxy.soap_stream_fault(std::cerr);

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req;
		delete resp;

		return false;
	} else {
		if( resp->allregs!=NULL ) {
			proDirePara.siteID = resp->allregs->siteID;
			proDirePara.Type = resp->allregs->Type;
			string* p_Url = resp->allregs->Url;
			proDirePara.Url = (p_Url==NULL) ? "" : *p_Url;

			//  ProDirePair
			if( resp->allregs->allbms!=NULL ) {
				vector<FBB::Entity::ProDirePair> proDirePairVector;
				proDireCustomMap(proDirePairVector, resp->allregs->allbms->ProDirePair);
				proDirePara.allbms = proDirePairVector;
			}

			proDirePara.Valid = resp->allregs->Valid;
			time_t* p_ValidTime = resp->allregs->ValidTime;
			proDirePara.ValidTime = (p_ValidTime==NULL) ? NULL : *p_ValidTime;
		}

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req;
		delete resp;

		return true;
	}

}
#pragma endregion

#pragma region AreaMap
bool myAreaCustomMap(FBB::Entity::MyArea* myArea, ns1__MyArea* mymode){
	string* p_id = mymode->id;
	myArea->id = (p_id==NULL) ? "" : *p_id;

	string* p_title = mymode->title;
	myArea->title = (p_title==NULL) ? "" : *p_title;

	string* p_parentid = mymode->parentid;
	myArea->parentid = (p_parentid==NULL) ? "" : *p_parentid;

	return true;
}

bool areaCustomMap(vector<FBB::Entity::AreaPair>& areaPairVector, std::vector<class ns1__AreaPair * >& AreaPair){
	vector< ns1__AreaPair* >::const_iterator it = AreaPair.begin();

	while( it!=AreaPair.end() ){
		vector<FBB::Entity::OneOptRegEntity> oneOptRegVector;
		oneOptRegCustomMap(oneOptRegVector, (*it)->oors->OneOptReg);
		FBB::Entity::MyArea* myArea = new FBB::Entity::MyArea();
		myAreaCustomMap(myArea, (*it)->mymode);

		FBB::Entity::AreaPair* areaPair = new FBB::Entity::AreaPair();
		areaPair->mymode = *myArea;
		areaPair->oors = oneOptRegVector;
		areaPairVector.push_back( *areaPair );

		it++;

		delete myArea;
		delete areaPair;
	}

	return true;
}

bool areaCustomMap(ns1__AreaPara* ns1_areaPara,  map<long, map<long, FBB::Entity::AreaPara> >& areaMap){
	FBB::Entity::AreaPara* areaPara = new FBB::Entity::AreaPara();
	long siteId = ns1_areaPara->siteID;
	long type = ns1_areaPara->Type;
	
	areaPara->siteID = siteId;
	areaPara->Type = type;
	string* p_Url = ns1_areaPara->Url;
	areaPara->Url = (p_Url==NULL) ? "" : *p_Url;

	//  AreaPair
	vector<FBB::Entity::AreaPair> areaPairVector;
	areaCustomMap(areaPairVector, ns1_areaPara->allbms->AreaPair);
	areaPara->allbms = areaPairVector;

	areaPara->Valid = ns1_areaPara->Valid;
	time_t* p_ValidTime = ns1_areaPara->ValidTime;
	areaPara->ValidTime = (p_ValidTime==NULL) ? NULL : *p_ValidTime;

	map<long, FBB::Entity::AreaPara> pageTypeMap;
	pageTypeMap[type] = *areaPara;
	areaMap[siteId] = pageTypeMap;		

	delete areaPara;

	return true;
}

bool FBB::WS::WSClientClass::loadAreaMap(vector<int> siteIds, map<long, map<long, FBB::Entity::AreaPara> >& areaMap)
{ 
	boost::this_thread::interruption_point();

	WebServiceFabuParaSoapProxy areaMapProxy(SOAP_C_UTFSTRING);

	_ns1__GetAreaMapBySiteIDs* req = new _ns1__GetAreaMapBySiteIDs();
	req->ah = new ns1__AuthHeader();
	req->ah->UserName = new std::string("zhendao");
	req->ah->PassWord = new std::string("zhendaofabu");

	req->siteindex = new ns1__ArrayOfInt();
	req->siteindex->int_ = siteIds;

	_ns1__GetAreaMapBySiteIDsResponse* resp = new _ns1__GetAreaMapBySiteIDsResponse();

	int flag = areaMapProxy.GetAreaMapBySiteIDs(req, resp);
	if( areaMapProxy.error ){
		areaMapProxy.soap_stream_fault(std::cerr);

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req->siteindex;
		delete req;
		delete resp;

		return false;
	} else {
		if( resp->allregs->AreaPara.size()==0 ){ return true; } // Õ¾µãÃ»ÓÐ¶ÔÓ¦areaMapÊý¾Ý

		vector<class ns1__AreaPara * >::const_iterator it = resp->allregs->AreaPara.begin();
		if( *it==NULL ){ return false; } // ¾­ÓªÄ£Ê½²ÎÊýÎª¿Õ
		
		while( it!=resp->allregs->AreaPara.end() ){
			areaCustomMap(*it, areaMap);

			it++;
		}

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req->siteindex;
		delete req;
		delete resp;

		return true;
	}

}

bool FBB::WS::WSClientClass::getAreaMapOptBySiteIDandPageType(
	long siteId, long pageType, string companyLocated, FBB::Entity::AreaPara& areaPara)
{ 
	boost::this_thread::interruption_point();

	WebServiceFabuParaSoapProxy areaOptProxy(SOAP_C_UTFSTRING);

	_ns1__GetAreaMapOptBySiteIDandPageType* req = new _ns1__GetAreaMapOptBySiteIDandPageType();
	req->ah = new ns1__AuthHeader();
	req->ah->UserName = new std::string("zhendao");
	req->ah->PassWord = new std::string("zhendaofabu");

	req->siteid = siteId;
	req->pageType = pageType;
	req->CompanyLocated = &companyLocated;

	_ns1__GetAreaMapOptBySiteIDandPageTypeResponse* resp = new _ns1__GetAreaMapOptBySiteIDandPageTypeResponse();

	int flag = areaOptProxy.GetAreaMapOptBySiteIDandPageType_(req, resp);
	if( areaOptProxy.error ){
		areaOptProxy.soap_stream_fault(std::cerr);

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req;
		delete resp;

		return false;
	} else {
		if( resp->allregs!=NULL ) {
			areaPara.siteID = resp->allregs->siteID;
			areaPara.Type = resp->allregs->Type;
			string* p_Url = resp->allregs->Url;
			areaPara.Url = (p_Url==NULL) ? "" : *p_Url;

			//  AreaPair
			if( resp->allregs->allbms!=NULL ) {
				vector<FBB::Entity::AreaPair> areaPairVector;
				areaCustomMap(areaPairVector, resp->allregs->allbms->AreaPair);
				areaPara.allbms = areaPairVector;
			}

			areaPara.Valid = resp->allregs->Valid;
			time_t* p_ValidTime = resp->allregs->ValidTime;
			areaPara.ValidTime = (p_ValidTime==NULL) ? NULL : *p_ValidTime;
		}

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req;
		delete resp;

		return true;
	}

}
#pragma endregion

#pragma region SaveRegistInfo
bool FBB::WS::WSClientClass::saveRegistInfo(int userId, int siteId, int regStatus, string LoginName, string LoginPw){
	WebServiceFabuParaSoapProxy saveRegistProxy(SOAP_C_UTFSTRING);

	_ns1__SaveRegistInfo* req = new _ns1__SaveRegistInfo();
	req->ah = new ns1__AuthHeader(); // ah
	req->ah->UserName = new std::string("zhendao");
	req->ah->PassWord = new std::string("zhendaofabu");
	req->psr = new ns1__PUserSiteRelation(); // psr
	req->psr->SiteId = siteId; // psr
	req->psr->UserId = userId;
	req->psr->RegStatus = regStatus;
	req->psr->LoginName = &LoginName;
	req->psr->Password = &LoginPw;

	_ns1__SaveRegistInfoResponse* resp = new _ns1__SaveRegistInfoResponse();

	int flag = saveRegistProxy.SaveRegistInfo(req, resp);

	delete req->ah->UserName;
	delete req->ah->PassWord;
	delete req->ah;
	delete req->psr;
	delete req;
	delete resp;
	if( saveRegistProxy.error ){ // ÊÇ²»ÊÇÐèÒª°Ñ³ö´íÐÅÏ¢ÉÏ´«£¿
		saveRegistProxy.soap_stream_fault(std::cerr);
		return false;
	} else {
		return true;
	}

	boost::this_thread::interruption_point();
}
#pragma endregion

// all city loadUserInformation
bool cityinfoCustomMap(ns2__CityInfo* ns2__cityinfo, map<string, CITYINFO>& idCityInfoMap) {
	CITYINFO cityinfo;
	cityinfo.id = FBB::Helper::UtilClass::longToString( ns2__cityinfo->Id );
	string* p_cityname = ns2__cityinfo->CityName;
	cityinfo.cityname = (p_cityname==NULL) ? "" : *p_cityname;
	cityinfo.parentid = FBB::Helper::UtilClass::longToString( *(ns2__cityinfo->ParentID) );

	idCityInfoMap[ cityinfo.id ] = cityinfo;
	return true;
}

bool FBB::WS::WSClientClass::getAllCityInformation(map<string, CITYINFO>& idCityInfoMap) {
	boost::this_thread::interruption_point();

	ws_USCOREInterfaceSoapProxy cityInfoProxy(SOAP_C_UTFSTRING);

	_ns2__GetAllCityInformation* req = new _ns2__GetAllCityInformation();
	// req->ah = new ns2__AuthHeader();
	// req->ah->UserName = new std::string("zhendao");
	// req->ah->PassWord = new std::string("zhendaofabu");

	_ns2__GetAllCityInformationResponse* resp = new _ns2__GetAllCityInformationResponse();

	int flag = cityInfoProxy.GetAllCityInformation(req, resp);
	if( cityInfoProxy.error ){
		cityInfoProxy.soap_stream_fault(std::cerr);

		// delete req->ah->UserName;
		// delete req->ah->PassWord;
		// delete req->ah;
		delete req;
		delete resp;

		return false;
	} else {
		ns2__ArrayOfCityInfo* p_cityInfo = resp->GetAllCityInformationResult;

		if( p_cityInfo!=NULL ){
			vector<ns2__CityInfo* >::const_iterator it = p_cityInfo->CityInfo.begin();
			while( it!=p_cityInfo->CityInfo.end() ) {
				cityinfoCustomMap(*it, idCityInfoMap);
				it++;
			} 
		}

		// delete req->ah->UserName;
		// delete req->ah->PassWord;
		// delete req->ah;
		delete req;
		delete resp;

		return true;
	}
}

bool FBB::WS::WSClientClass::GetEmailSite(string domainName, FBB::Entity::EmailSite& es) {
	boost::this_thread::interruption_point();

	WebServiceFabuParaSoapProxy emailSiteProxy(SOAP_C_UTFSTRING);

	_ns1__GetEmailSite* req = new _ns1__GetEmailSite();
	req->ah = new ns1__AuthHeader(); // ah
	req->ah->UserName = new std::string("zhendao");
	req->ah->PassWord = new std::string("zhendaofabu");
	req->domainName = &domainName;

	_ns1__GetEmailSiteResponse* resp = new _ns1__GetEmailSiteResponse();

	int flag = emailSiteProxy.GetEmailSite_(req, resp);

	if( emailSiteProxy.error ){
		emailSiteProxy.soap_stream_fault(std::cerr);

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req;
		delete resp;

		return false;
	} else {
		ns1__EmailSite* p_es = resp->es;

		if( p_es!=NULL ){
			es.ID = p_es->ID;
			string* p_SiteName = p_es->SiteName;
			es.SiteName = (p_SiteName==NULL) ? "" : *p_SiteName;
			string* p_SiteURL = p_es->SiteURL;
			es.SiteURL = (p_SiteURL==NULL) ? "" : *p_SiteURL;
			string* p_DomainName = p_es->DomainName;
			es.DomainName = (p_DomainName==NULL) ? "" : *p_DomainName;
			string* p_Pop3 = p_es->Pop3;
			es.Pop3 = (p_Pop3==NULL) ? "" : *p_Pop3;
			es.Port = p_es->Port;
			es.UseSSL = p_es->UseSSL;
		}

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req;
		delete resp;

		return true;
	}

	boost::this_thread::interruption_point();
}

bool captchaOptCustomMap(ns1__CaptchaOpt* ws_captchaOpt, FBB::Entity::CaptchaOpt* captchaOpt) {
	long opt = static_cast<long>( ws_captchaOpt->opt );
	captchaOpt->opt = static_cast<FBB::Entity::CaptchaOpt::CaptchaOptType>( FBB::WS::WSClientClass::CaptchaOptTypeMap[opt] );
	captchaOpt->intpara = ws_captchaOpt->intpara;
	return true;
}

bool FBB::WS::WSClientClass::GetCaptchaPara(long SiteID, long CaptchaType, FBB::Entity::CaptchaPara& para) {
	boost::this_thread::interruption_point();

	WebServiceFabuParaSoapProxy captchaParaProxy(SOAP_C_UTFSTRING);

	_ns1__GetCaptchaParaBySiteIDandType* req = new _ns1__GetCaptchaParaBySiteIDandType();
	req->ah = new ns1__AuthHeader(); // ah
	req->ah->UserName = new std::string("zhendao");
	req->ah->PassWord = new std::string("zhendaofabu");
	req->siteid = SiteID;
	req->captchaType = CaptchaType;

	_ns1__GetCaptchaParaBySiteIDandTypeResponse* resp = new _ns1__GetCaptchaParaBySiteIDandTypeResponse();

	int flag = captchaParaProxy.GetCaptchaParaBySiteIDandType_(req, resp);

	if( captchaParaProxy.error ){
		captchaParaProxy.soap_stream_fault(std::cerr);

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req;
		delete resp;

		return false;
	} else {
		ns1__CaptchaPara* p_captchapara = resp->captchapara;

		if( p_captchapara!=NULL ){
			para.SiteID = p_captchapara->SiteID;
			para.CaptchaType = p_captchapara->CaptchaType;

			// ns1__ArrayOfCaptchaOpt *allsteps
			vector<FBB::Entity::CaptchaOpt> allsteps;
			if( p_captchapara->allsteps!=NULL ) {
				vector<ns1__CaptchaOpt* >::const_iterator it = p_captchapara->allsteps->CaptchaOpt.begin();
				while( it!=p_captchapara->allsteps->CaptchaOpt.end() ) {
					FBB::Entity::CaptchaOpt* captchaOpt = new FBB::Entity::CaptchaOpt();
					captchaOptCustomMap(*it, captchaOpt);
					allsteps.push_back( *captchaOpt );
					delete captchaOpt;
					it++;
				}
			} 
			para.allsteps = allsteps; 

			para.modelType = p_captchapara->modelType;
			para.numChar = p_captchapara->numChar;
			para.Valid = p_captchapara->Valid;
			/*string* p_Note = p_captchapara->Note;
			para.Note = (p_Note==NULL) ? "" : *p_Note;
			time_t* p_UpdateTime = p_captchapara->UpdateTime;
			para.UpdateTime = (p_UpdateTime==NULL) ? NULL : *p_UpdateTime;		*/	
		}

		delete req->ah->UserName;
		delete req->ah->PassWord;
		delete req->ah;
		delete req;
		delete resp;

		return true;
	}

	boost::this_thread::interruption_point();
}