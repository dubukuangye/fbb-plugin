#pragma once

#include "htmlparser.h"
#include "fbbException.h"

#include <string>
#include <map>
// #include "APITypes.h"
// #include "variant.h"
#include <MsHTML.h>
#include <atlbase.h>
// #include <boost/thread.hpp>
// #include <exdispid.h>
#include "CIEEvent.h"
#include "htmlparser.h"
#include "boost/thread.hpp"

#import "mshtml.tlb" no_auto_exclude rename("TranslateAccelerator","TranslateAccelerator_HTML")
#import "shdocvw.dll" no_auto_exclude rename("tagREADYSTATE","tagIEREADYSTATE") rename("FindText","FindIEText")
using namespace std;

namespace FBB{
	namespace Browser{
		class WebBrowserEventHandler;

		class BrowserClass
		{
		public:
			BrowserClass(){//g
				ie=NULL;				
				this->evobj=NULL;				
			}
			BrowserClass(FBB::Parser::HtmlParserClass* p_parser){//g
				_parserPtr = p_parser;
				ie=NULL;				
				this->evobj=NULL;				
			}			
			~BrowserClass(){//g
				if(evobj!=NULL)
					delete evobj;
				shutdownIE();
				// ie.Release();
			}

			bool Init();//g

			void setBrowserPtr(SHDocVw::IWebBrowser2Ptr ie) {
				this->ie = ie;
			}
			SHDocVw::IWebBrowser2Ptr getBrowserPtr();
			
			/**
			 * [shutdownIE description]
			 * close ie instance. 
			 * In debug mode, we choose not to shutdown ie in order to find the error 
			 * @return if close ie instance successfully
			 */
			bool shutdownIE();

			/**
			 * [navigate description]
			 * navigate to a specific web site
			 * @param  url url of the web site
			 * @param  p_parser 与browser实例对应的htmlparser实例
			 * @return
			 */
			bool navigate(std::string url,  FBB::Parser::HtmlParserClass* p_parser);

			/**
			 * [invokeMember description]
			 * 对HTML元素执行相应操作（如click，focus等）
			 * @param  pElem	HTML元素
			 * @param  eleOpt	操作名称 
			 * @param  p_parser	useless, can be removed
			 * @return
			 */
			bool invokeMember(MSHTML::IHTMLElementPtr pElem, string eleOpt, FBB::Parser::HtmlParserClass* p_parser);
			
			MSHTML::IHTMLDocument2Ptr getDocumentPtr();

			/**
			 * [checkDocumentComplete description]
			 * detect if page load complete
			 * @param msg 			消息类型
			 * @param msperloop		等消息间隔的时间
			 */
			void checkDocumentComplete(unsigned int msg=WM_WB_READY,int msperloop=300);			
			bool likeDocumentComplete(long& last_doc_length);
			long getDocumentLength();

			bool WaitMsg(UINT msg,int millionsecond);
		private:
			SHDocVw::IWebBrowser2Ptr ie;
			CIEEvent* evobj;
			FBB::Parser::HtmlParserClass* _parserPtr;
		};
	}
}