﻿



$(function () {
    tmpIdList = $("#Hid_IdList").val().split(',');
    totalSitesCount = tmpIdList.length;

    $(".progress").hide();
});


//进度条
function processbar() {
    var progressValue = (100 / totalSitesCount) * finish;
    $("#progressbar").css("width", progressValue + "%");
}

function InputRegCheckCode(siteId) {
    var captchaStr = $("#txtcode" + siteId).val();
    if (captchaStr == "") { alert("请输入验证码！"); return false; }
    $("#dv_" + siteId).hide();
    plugin().setRegCaptchaAnswer(siteId, 'CAPTCHA', captchaStr); // 回传验证码字符串给插件
}


function printRegCheckCode(siteid, img) {
    var html = "<div id=\"dv_" + siteid + "\" class=\"prompt\"><span>请输入验证码" + siteid + "：</span><img src=\"" + img + "\" /><input id=\"txtcode" + siteid + "\" type=\"text\" /><input id=\"btnsubmit" + siteid + "\" type=\"button\" value=\"确定\"  onclick=\"InputRegCheckCode(" + siteid + ")\" siteID=\"" + siteid + "\" /></div>";
    $("#sss").append(html);
}

function onFBBRegError(siteId, err) {
    fail++;
    finish++;
    $("#span_desc").html("当前注册进度，总个数：【" + totalSitesCount + "】失败个数：【" + fail + "】成功个数：【" + succeed + "】已完成数：【" + finish + "】<br/>");
    processbar();
    $("#progressdesc").append("站点【" + siteId + "】失败，原因" + err + "<br/>");
}

function onFBBRegComplete(siteId) {
    succeed++;
    $("#span_desc").html("当前注册进度，总个数：【" + totalSitesCount + "】失败个数：【" + fail + "】成功个数：【" + succeed + "】已完成数：【" + finish + "】<br/>");
    processbar();
    $("#progressdesc").append("站点【" + siteId + "】注册成功<br/>");
}

function onFBBRegFinish(siteId) {
    finish++;
    $("#span_desc").html("当前注册进度，总个数：【" + totalSitesCount + "】失败个数：【" + fail + "】成功个数：【" + succeed + "】已完成数：【" + finish + "】<br/>");
    processbar();
}


function nextRound() {
    var siteIds = new Array();
    for (var i = 0; i < regNumberInBatch && tmpIdList.length > 0; i++)
        siteIds.push(tmpIdList.pop());
    return siteIds;
}

function beginRegister() {
    $(".progress").show();
    $("#span_desc").html("正在注册中，所需要时间可能较长，请耐心等候……");
    FBB_STATUS = 'PROCESSING';
    register(nextRound());
}

function register(siteIds) {
    if (siteIds <= 0) {
        $('#test').html('注册已结束');
        FBB_STATUS = 'STOPPING';
        return;
    }

    $("#test").html("正在注册【");
    for (var i = 0; i < siteIds.length; i++) {
        if (0 == i)
            $('#test').append(siteIds[i]);
        else
            $('#test').append(',' + siteIds[i]);
    }
    $("#test").append("】");

    plugin().registerInBatch(siteIds);
}

function regCaptchaCallback(siteId, captchaKind, captchaUrl) {
    if (captchaKind == "CAPTCHA")
        printRegCheckCode(siteId, captchaUrl);
    else if (captchaKind == "MOBILECAPTCHA")
        plugin().setRegCaptchaAnswer(siteId, captchaKind, "moible regNumberInBatch");
    else if (captchaKind == "QUESTIONCAPTCHA") { // 新增2013/6/27: 问题验证码
        var question = plugin().getRegCaptchaQuestion(siteId, captchaKind);

        var answer = prompt(question[0]);
        plugin().setRegCaptchaAnswer(siteId, captchaKind, answer);
    } else if (captchaKind == "QUESTIONCAPTCHASEL") { // 新增2013/6/27: 验证问题-选择答案验证码[未测试]
        // 1. get question list
        var question = plugin().getRegCaptchaQuestion(siteId, captchaKind);
        // 2. display to user
        // display( question )
        // 3. return answer to plugin
        // plugin().setCaptchaAnswer(siteId, captchaKind, answer);
    }
}



