﻿

$(function () {
    siteList = $("#hfsiteidlist").val().split(',');
    mpList = $("#hfmpidlist").val().split(',');
    totalPublishCount = siteList.length;
    
//    $(".progress").hide();
});

function publishprocessbar() {
    var progressValue = (100 / totalPublishCount) * p_finish;
    $("#progressbar").css("width", progressValue + "%");
    $("#span_desc").html("当前发布进度，总共" + totalPublishCount + "个，成功" + p_succeed + "个，失败" + p_fail + "个");
}

function printCheckCode(siteid, PMID, img) {
    var html = "<div id=\"dv_" + siteid + "\" class=\"prompt\"><span>请输入验证码 siteId = " + siteid + "; PMID = " + PMID + "：</span><img src=\"" + img + "\" /><input id=\"txtcode" + siteid + "\" " +
                   " type=\"text\" /><input id=\"btnsubmit" + siteid + "\" type=\"button\" value=\"确定\"  onclick=\"InputCheckCode(" + siteid + "," + PMID + ")\" siteID=\"" + siteid + "\" /></div>";
    $("#sss").append(html);
}

function InputCheckCode(siteId, PMID) {
    var captchaStr = $("#txtcode" + siteId).val();
    if (captchaStr == "") { alert("请输入验证码！"); return false; }
    $("#dv_" + siteId).hide();
    plugin().setPublishCaptchaAnswer(siteId, PMID, 'CAPTCHA', captchaStr); // 回传验证码字符串给插件
}


function onFBBPublishError(siteId, PMID, err) {
    p_fail++;
    p_finish++;
    $("#progressdesc").append("站点【" + siteId + "_" + PMID + "】失败，原因" + err + "<br/>");
    publishprocessbar();
}

function onFBBPublishComplete(siteId, PMID) {
    p_finish++;
    p_succeed++;
    $("#progressdesc").append("站点【" + siteId + ", " + PMID + "】发布成功<br/>");
    publishprocessbar()
}


function beginPublish() {
    $(".progress").show();
    var siteIds = nextSite();
    var PMIDs = nextMP();
    plugin().publishInBatch(siteIds, PMIDs);

    $("#span_desc").html("正在发布中，所需要时间可能较长，请耐心等候……");
}

function againPublish() {
    var siteIds = nextSite();
    var PMIDs = nextMP();
    plugin().publishInBatch(siteIds, PMIDs);
}


function nextSite() {
    var siteIds = new Array();
    for (var i = 0; i < publishNumber && siteList.length > 0; i++)
        siteIds.push(siteList.pop());
    return siteIds;
}

function nextMP() {
    var mpIds = new Array();
    for (var i = 0; i < publishNumber && mpList.length > 0; i++)
        mpIds.push(mpList.pop());
    return mpIds;
}

function publishCaptchaCallback(siteId, PMID, captchaKind, captchaUrl) {
    if (captchaKind == "CAPTCHA")
        printCheckCode(siteId, PMID, captchaUrl);
    else if (captchaKind == "MOBILECAPTCHA")
        plugin().setPublishCaptchaAnswer(siteId, PMID, captchaKind, "moible regNumberInBatch");
    else if (captchaKind == "QUESTIONCAPTCHA") { // 新增2013/6/27: 问题验证码
        var question = plugin().getPublishCaptchaQuestion(siteId, captchaKind);

        var answer = prompt(question[0]);
        plugin().setPublishCaptchaAnswer(siteId, PMID, captchaKind, answer);
    } else if (captchaKind == "QUESTIONCAPTCHASEL") { // 新增2013/6/27: 验证问题-选择答案验证码[未测试]
        // 1. get question list
        var question = plugin().getPublishCaptchaQuestion(siteId, PMID, captchaKind);
        // 2. display to user
        // display( question )
        // 3. return answer to plugin
        // plugin().setCaptchaAnswer(siteId, PMID, captchaKind, answer);
    }
}