﻿
// 为引用方便，定义plugin为获取插件的方法
function plugin0() {
    return document.getElementById('plugin0');
}
plugin = plugin0;

// 测试插件是否有效
function pluginValid() {
    if (plugin().valid) // valid属性
        alert("插件可以使用！");
    else
        alert("插件不能使用！");
}

function pluginLoaded() {
    succeed = fail = finish = 0;

    var userid = $("#Hid_UserID").val();
    plugin().initializeFBB(
                 userid
                , onFBBRegError
                , onFBBRegComplete // 注册完成步 -- if called, it means registration succeed, and username and password are upload to the server
                , onFBBRegFinish // all steps are done.
                , regCaptchaCallback
                , onBatchFinish // group of registration/publish thread stop
                , onFBBPublishError
                , onFBBPublishComplete
                , publishCaptchaCallback
                , downloadCallback
            );

    FBB_STATUS = 'NOT_START';
}

function downloadCallback(fnameremote, fnamelocal) { // triky implemetation
    plugin().beginDownload(fnameremote, fnamelocal);
}

function stopRegisterOrPublish(registerOrPublish) {
    plugin().stopRegisterOrPublish(registerOrPublish);
}

function onBatchFinish(regOrPublish) {
    if (regOrPublish === 'register') { // registration group stop
        $('#test').html('注册已停止。');
    } 
    else 
    { 
        // publish group stop
        if (mpList.length > 0) {
            againPublish();
        }
        else {
            $('#test').html('发布已停止。');
        }
    }
}