#pragma once

#include <map>
#include "regpara.h"
#include "deploytestAPI.h"
#include "wsClient.h"
#include "util.h"
#include "htmlparser.h"
#include "browser.h"
#include "evaluator.h"
#include "fbbException.h"

class HtmlParserClass;
class BrowserClass;
class UtilClass;

class IEEntityClass {
protected:
	long _siteId;
	string _captchaUrl;

	// pointers need for manipulate web pages
	FBB::Parser::HtmlParserClass* _parser;
	FBB::Browser::BrowserClass* _browser;
	FBB::Helper::UtilClass* _util;

	map<FBB::Entity::OneOptRegEntity::OneOptRegTypeEntity, Evaluator*> _evaluatorMap;

	// captcha related
	wstring _captcha;
	wstring _moibleCaptcha;
	wstring _questionCaptcha;
	long _selectIndex;
	vector<wstring> _captchaInfoVector;

	// just for RecordCaptcha and RepeatCaptcha evaluator
	string _captchaStr;
public:
	static FB::JSObjectPtr downloadCallback;

	void createEvaluator();
	void EvaluateOneOptReg(FBB::Entity::OneOptRegEntity oor);
	Evaluator* lookupEvaluator(FBB::Entity::OneOptRegEntity::OneOptRegTypeEntity type);
	virtual wstring getFromEntity(wstring ) = 0;
	virtual void excuteSingle() = 0;
	virtual string generateCaptchaFilePath() = 0;
	virtual void makeCaptchaCallback(string ) = 0;

	string getCaptchaStr() { return _captchaStr; }
	void setCaptchaStr(string captchaStr) { _captchaStr = captchaStr; }

	/*********a lot of getters**************/
	long getSiteId() { return _siteId; }

	FBB::Parser::HtmlParserClass* getParser() { return _parser; }
	FBB::Browser::BrowserClass* getBrowser() { return _browser; }
	FBB::Helper::UtilClass* getUtil() { return _util; }

	map<FBB::Entity::OneOptRegEntity::OneOptRegTypeEntity, Evaluator*> getEvaluatorMap() {
		return _evaluatorMap;
	}
	/*********a lot of getters end**************/

	// commmunicate with deploytest 
	void setCaptchaAnswer( string captchaKind, wstring answer );
	vector<wstring> getCaptchaQuestion() { return _captchaInfoVector; }

    // communicate with Evaluators 
	wstring getCaptha() { return _captcha; }
	void resetCaptha() { _captcha = L""; }

	wstring getMobileCaptha() { return _moibleCaptcha; }
	void resetMobileCaptcha() { _moibleCaptcha = L""; }

	wstring getQuestionCaptcha() { return _questionCaptcha; }
	void resetQuestionCaptcha() { _questionCaptcha = L""; }

	long getSelectIndex() { return _selectIndex; }
	void resetSelectIndex() { _selectIndex = -1; }

	void setCaptchaQuestion(vector<wstring> captchaInfoVector) { 
		_captchaInfoVector = captchaInfoVector;
	}

	IEEntityClass(long siteId) {
		_siteId = siteId;
		_captcha = _moibleCaptcha = _questionCaptcha = L"";
		_selectIndex = -1;
		_parser = NULL;
		_browser = NULL;
		_util = NULL;
	}
	virtual ~IEEntityClass();
};

class RegistrationClass : public IEEntityClass
{
private:
	static FB::JSObjectPtr _regErrorCallback;
	static FB::JSObjectPtr _regCompleteCallback;
	static FB::JSObjectPtr _regFinishCallback;
	static FB::JSObjectPtr _regCaptchaCallback;

	FBB::Entity::RegParaEntity _regPara;
public:
	RegistrationClass(long siteId) : IEEntityClass(siteId) {}
	~RegistrationClass() {}

	static void setCallback(
		 const FB::JSObjectPtr& regErrorCallback
    	,const FB::JSObjectPtr& regCompleteCallback
    	,const FB::JSObjectPtr& regFinishCallback
    	,const FB::JSObjectPtr& regCaptchaCallback)
	{
		_regErrorCallback = regErrorCallback;
		_regCompleteCallback = regCompleteCallback;
		_regFinishCallback = regFinishCallback;
		_regCaptchaCallback = regCaptchaCallback;
	}

	string getLoginNameFromRegPara() { return _regPara.LoginName; }
	void setLoginNameToRegPara(string LoginName) {_regPara.LoginName = LoginName; }

	string getLoginPwFromRegPara() { return _regPara.LoginPw; }
	void setLoginPwToRegPara(string LoginPw) { _regPara.LoginPw = LoginPw; }

	void excuteSingle();
	wstring getFromEntity(wstring );
	string generateCaptchaFilePath();
	void makeCaptchaCallback(string );
};

class PublishClass : public IEEntityClass
{
private:
	static FB::JSObjectPtr _publishErrorCallback;
	static FB::JSObjectPtr _publishCompleteCallback;
	static FB::JSObjectPtr _publishCaptchaCallback;

	FBB::Entity::PublishParaEntity _publishPara;

	map<wstring, wstring> _publishEntityInfoMap;

	long _PMID;
public:
	PublishClass(long siteId, long PMID) : IEEntityClass(siteId) {
		_PMID = PMID;
	}
	~PublishClass() {}

	static void setCallback(
		 const FB::JSObjectPtr& publishErrorCallback
    	,const FB::JSObjectPtr& publishCompleteCallback
    	,const FB::JSObjectPtr& publishCaptchaCallback)
	{
		_publishErrorCallback = publishErrorCallback;
		_publishCompleteCallback = publishCompleteCallback;
		_publishCaptchaCallback = publishCaptchaCallback;
	}

	long getPMID() { return _PMID; }

	void excuteSingle();
	wstring getFromEntity(wstring );
	string generateCaptchaFilePath();
	void makeCaptchaCallback(string );
};