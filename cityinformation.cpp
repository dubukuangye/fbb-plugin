#include "cityinformation.h"

CityInformation* CityInformation::_pCityInfo = NULL; // initialize statice member
const string CityInformation::_sFILENAME = getFileName();

const string CityInformation::getFileName() {
    // string appdata( getenv("APPDATA") );
    // return appdata+"\\zhendao"+"\\deploytest"+"\\1.0.0.0"+"\\cityinfo.txt";
    return FB::System::getAppDataPath("zhendao\\deploytest\\1.0.0.0\\cityinfo.txt");
}

CityInformation::CityInformation() {
    std::ifstream infile(_sFILENAME);
    if( infile ) { // file exist
        this->readFile();
    } else {
        FBB::WS::WSClientClass wsClient;
        wsClient.getAllCityInformation( _idCityInfoMap );
        writeFile();
    }
    infile.close();
}

void CityInformation::readFile() {
    std::ifstream infile(_sFILENAME);
    CITYINFO cityinfo;
    while (infile >> cityinfo.id >> cityinfo.cityname >> cityinfo.parentid)
        _idCityInfoMap[ cityinfo.id ] = cityinfo;

    infile.close();
}

void CityInformation::writeFile() {
    std::ofstream outfile(_sFILENAME);

    map<string, CITYINFO>::iterator it = _idCityInfoMap.begin();
    while( it!=_idCityInfoMap.end() ) {
        outfile << (it->second).id << "\t"
        << (it->second).cityname << "\t"
        << (it->second).parentid << "\n";

        it++;
    }

    outfile.close();
}

CITYINFO CityInformation::getCityInfoByID(string id) {
    map<string, CITYINFO>::iterator it = _idCityInfoMap.find(id);
    return it->second;
}

bool CityInformation::matchCityName(string provinceName, string cityName, string countryName, CITYINFO cityinfo) {
    CITYINFO m_cityinfo = cityinfo;
    string parentid = "-1";
    if( countryName==m_cityinfo.cityname ) {
        parentid = m_cityinfo.parentid;
        if( parentid=="0" )
            return true;
        m_cityinfo = getCityInfoByID( parentid );
        if( cityName==m_cityinfo.cityname ) {
            parentid = m_cityinfo.parentid;
            if( parentid=="0" )
                return true;
            m_cityinfo = getCityInfoByID( parentid );
            if( provinceName==m_cityinfo.cityname )
                return true;
        }
    }

    return false;
}

string CityInformation::GetLocated(string provinceName, string cityName, string countryName) {
    map<string, CITYINFO>::iterator it = _idCityInfoMap.begin();
    while( it!=_idCityInfoMap.end() ) {
        if( matchCityName(provinceName, cityName, countryName, it->second) ) // compare with the 'i' th CITYINFO
	        return it->first;
        
        it++;
    }        
    return ""; // not found
}