/**
 * written&maintain by Teacher Dong
 */

#pragma once
#include "oaidl.h"
#include <exdisp.h>
#include <atlbase.h>
#include <MsHTML.h>
#include "usermsg.h"

#import "mshtml.tlb" no_auto_exclude rename("TranslateAccelerator","TranslateAccelerator_HTML")
#import "shdocvw.dll" no_auto_exclude rename("tagREADYSTATE","tagIEREADYSTATE") rename("FindText","FindIEText")

class CIEEvent : public IDispatch
{
public:
	CIEEvent();
	~CIEEvent(void);
	bool AttachEvent(SHDocVw::IWebBrowser2Ptr tie);
	bool CIEEvent::AttachEvent(MSHTML::IHTMLElementPtr tele);
public:
	STDMETHOD(QueryInterface)(REFIID riid,void __RPC_FAR *__RPC_FAR *ppvObject);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);
	// IDispatch
	STDMETHOD(GetTypeInfoCount)(UINT *pctinfo);
	STDMETHOD(GetTypeInfo)(UINT iTInfo,LCID lcid,ITypeInfo **ppTInfo);
	STDMETHOD(GetIDsOfNames)(REFIID riid,LPOLESTR *rgszNames,UINT cNames,LCID lcid, DISPID *rgDispId);
	STDMETHOD(Invoke)(DISPID dispidMember,REFIID riid, LCID lcid,WORD wFlags, DISPPARAMS * pDispParams,VARIANT * pvarResult,EXCEPINFO * pexcepinfo,UINT * puArgErr);	
	
private:	
	int m_dwRef;
	int m_nPageCounter;
	DWORD mCookie,mCookieEle;
	SHDocVw::IWebBrowser2Ptr ie;
	IConnectionPoint * spCP,*spCPEle;
	MSHTML::IHTMLElementPtr ele;

};

