#include "captchapred.h"
#include "Captchadll.h"

bool CaptchaPred(FBB::Entity::CaptchaPara& para,string picfname,string rootdir,string& restr)
{
	unsigned long* colorData=NULL;
	unsigned char* grayData=NULL;
	PMGrayWrapper* cuts=NULL;
	wchar_t *wdir=NULL;
	wchar_t* tre=NULL;
	bool re=false;
	if(para.Valid==false)
		return false;
	try
	{		
		int i,j;		
		wstring wfname=str2wstr(picfname);	
		wstring wrootdir=str2wstr(rootdir);
		CImage img;	
		if(img.Load(wfname.c_str()))//��0ʧ��
		{		
			re=false;
			goto error;
		}
		int row=img.GetHeight();
		int col=img.GetWidth();		
		if((row<=0)||(col<=0))
		{		
			re=false;
			goto error;
		}
		colorData=new unsigned long[row*col];		
		for(i=0;i<row;i++)
		{
			for(j=0;j<col;j++)
			{
				colorData[i*col+j]=img.GetPixel(j,i);
			}
		}
		//grayData=new unsigned char[row*col];
		grayData=(unsigned char*)CoTaskMemAlloc(sizeof(unsigned char)*row*col);
		
		if(!DLL_Color2Gray(colorData,row,col,grayData))
		{re=false;goto error;}
		for(i=0;i<(int)para.allsteps.size();i++)
		{			
			re=true;
			switch(para.allsteps[i].opt)
			{
			case FBB::Entity::CaptchaOpt::RemoveFrame:
				re=DLL_RemoveFrame(grayData,row,col);					
				break;
			case FBB::Entity::CaptchaOpt::ZoomDouble:
				re=DLL_ZoomPM(&grayData,row,col);	
				row*=2;col*=2;
				break;
			case FBB::Entity::CaptchaOpt::TwoValuedOTSU:
				re=DLL_TwoValuedOTSU(grayData,row,col);					
				break;
			case FBB::Entity::CaptchaOpt::TwoValuedOTSU_Fixed:
				re=DLL_TwoValuedOTSU_Fixed(grayData,row,col,para.allsteps[i].intpara);					
				break;
			case FBB::Entity::CaptchaOpt::ProduceConRegion:
				re=DLL_ProduceConRegion(grayData,row,col,para.allsteps[i].intpara);					
				break;
			case FBB::Entity::CaptchaOpt::ProduceConRegion_Reverse://Ignore	
				re=DLL_ProduceConRegion_Reverse(grayData,row,col,para.allsteps[i].intpara);	
				break;
			case FBB::Entity::CaptchaOpt::Correction_TemplateChangeBlack:
				re=DLL_Correction_TemplateChangeBlack(grayData,row,col);					
				break;
			case FBB::Entity::CaptchaOpt::Correction_TemplateChangeWhite:
				re=DLL_Correction_TemplateChangeWhite(grayData,row,col);					
				break;
			case FBB::Entity::CaptchaOpt::DilationPM:
				re=DLL_DilationPM(grayData,row,col);					
				break;
			case FBB::Entity::CaptchaOpt::RuiHua_Gaosi:
				re=DLL_RuiHua_Gaosi(grayData,row,col);					
				break;
			case FBB::Entity::CaptchaOpt::BackbonePM:
				re=DLL_BackbonePM(grayData,row,col);					
				break;
			case FBB::Entity::CaptchaOpt::ZoomBackbonePM:
				re=DLL_ZoomBackbonePM(&grayData,row,col);					
				break;
			default:
				break;
			}
			if(!re)
				goto error;
		}
		int cutnum;
		wdir=new wchar_t[wrootdir.length()+1];
		wcscpy_s(wdir,wrootdir.length()+1,wrootdir.c_str());
		if(!DLL_GeneralPartConSplit(grayData,row,col,para.modelType,para.numChar,&cuts,cutnum,wdir))
		{re=false;goto error;		}
		if(!DLL_PredictPM(cuts,cutnum,para.modelType,&tre,wdir))
		{re=false;goto error;		}
		wstring trew(tre);
		restr=wstr2str(trew);
		re=true;
	}
	catch(...)
	{
		re=false;
		goto error;
	}
error:
	if(colorData!=NULL)
			delete[] colorData;
		if(grayData!=NULL)
			//delete[] grayData;
				CoTaskMemFree(grayData);
		if(cuts!=NULL)
			CoTaskMemFree((void*)cuts);
		if(wdir!=NULL)
			delete[] wdir;
		if(tre!=NULL)
			CoTaskMemFree((void *)tre);
	return re;
	

}
std::string wstr2str(const std::wstring& ws)
{
    std::string curLocale = setlocale(LC_ALL, NULL);        // curLocale = "C";
    setlocale(LC_ALL, "chs");
    const wchar_t* _Source = ws.c_str();
    size_t _Dsize = 2 * ws.size() + 1;
    char *_Dest = new char[_Dsize];
    memset(_Dest,0,_Dsize);
    wcstombs_s(NULL,_Dest,_Dsize,_Source,_TRUNCATE);
    std::string result = _Dest;
    delete []_Dest;
    setlocale(LC_ALL, curLocale.c_str());
    return result;
}

std::wstring str2wstr(const std::string& s)
{
    setlocale(LC_ALL, "chs"); 
    const char* _Source = s.c_str();
    size_t _Dsize = s.size() + 1;
    wchar_t *_Dest = new wchar_t[_Dsize];
    wmemset(_Dest, 0, _Dsize);
   // mbstowcs(_Dest,_Source,_Dsize);
	mbstowcs_s(NULL,_Dest,_Dsize,_Source,_TRUNCATE);
    std::wstring result = _Dest;
    delete []_Dest;
    setlocale(LC_ALL, "C");
    return result;
}