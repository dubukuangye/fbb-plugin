#pragma once

#include <set>
#include <map>
#include <list>
#include <vector>
#include <algorithm>

#include <string>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>
#define _WINSOCKAPI_
#include <Windows.h>
#include <exdisp.h>
#include <MsHTML.h>
#include <atlbase.h>
#include <oleacc.h>
// for Email_URL
#include <iostream>
#include <regex>
#include <ATLComTime.h>
#include "Pop3.h"
#include "stringopt.h"
// for auto captcha 
#include "Captchadll.h"
#include "captchapred.h"

#include "wsClient.h"
#include "browser.h"
#include "regpara.h"
#include "htmlparser.h"
#include "util.h"
#include "fbbException.h"
#include "fbbcore.h"
#include "userinformation.h"
#include "thread.h"
#include "cityinformation.h"

/* ***************************************************************
 * 
 * 					written by Guo Qidong
 *	  			 use Command Design Pattern 
 * to spilt so long switch function into small independent classes
 *
 * 		every evaluator class stands for one operation
 *
 * ****************************************************************/

class IEEntityClass;

class Evaluator {
public:
	Evaluator(IEEntityClass* ieEntity) {
		_ieEntity = ieEntity;
	}
	virtual ~Evaluator(){}
	
	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor) = 0;
protected:
	IEEntityClass* _ieEntity;
};

class InvokeMemberEvaluator : public Evaluator {
public:
	InvokeMemberEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity) 
	 {}
	 ~InvokeMemberEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class SetDirectValueEvaluator : public Evaluator {
public:
	SetDirectValueEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity) 
	 {}
	~SetDirectValueEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class SetFieldValueEvaluator : public Evaluator {
public:
	SetFieldValueEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity) 
	 {}
	~SetFieldValueEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class SelectionDirectEvaluator : public Evaluator {
public:
	SelectionDirectEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity) 
	 {}
	~SelectionDirectEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class SelectionSequenceEvaluator : public Evaluator {
public:
	SelectionSequenceEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~SelectionSequenceEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class CaptchaEvaluator : public Evaluator {
public:
	CaptchaEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~CaptchaEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);

	static const string autoCaptchaModelDir;
};

class UserEvaluator : public Evaluator {
public:
	UserEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~UserEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class PassWordEvaluator : public Evaluator {
public:
	PassWordEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~PassWordEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class RepeatPassWordEvaluator : public Evaluator {
public:
	RepeatPassWordEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~RepeatPassWordEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class BuinessDirectionMapEvaluator : public Evaluator {
public:
	BuinessDirectionMapEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~BuinessDirectionMapEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class BuinessModeMapEvaluator : public Evaluator {
public:
	BuinessModeMapEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity) {}
	~BuinessModeMapEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class ProDirectoryMapEvaluator : public Evaluator {
public:
	ProDirectoryMapEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~ProDirectoryMapEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class AreaMapEvaluator : public Evaluator {
public:
	AreaMapEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~AreaMapEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class FocusEvaluator : public Evaluator {
public:
	FocusEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~FocusEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class MobileCaptchaEvaluator : public Evaluator {
public:
	MobileCaptchaEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~MobileCaptchaEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class ExtractUserNameEvaluator : public Evaluator {
public:
	ExtractUserNameEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~ExtractUserNameEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class QuestionCaptchaEvaluator : public Evaluator {
public:
	QuestionCaptchaEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~QuestionCaptchaEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class QuestionCaptchaSelEvaluator : public Evaluator {
public:
	QuestionCaptchaSelEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~QuestionCaptchaSelEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class InvokeKeyPressEvaluator : public Evaluator {
public:
	InvokeKeyPressEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~InvokeKeyPressEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class SetRegUserEvaluator : public Evaluator {
public:
	SetRegUserEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~SetRegUserEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class RedirectURLEvaluator : public Evaluator {
public:
	RedirectURLEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~RedirectURLEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class AttachValueEvaluator : public Evaluator {
public:
	AttachValueEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~AttachValueEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class UploadFileEvaluator : public Evaluator {
public:
	UploadFileEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~UploadFileEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);

	string getLocalFilePath() {
		return localFilePath;
	}
private:
	string localFilePath;
	void getPath(string fileField, long fileIndex, string& fnameremote, string& fnamelocal);

	boost::mutex g_mutex;
	void SetFileThread(HWND hwnd);

	static BOOL CALLBACK EnumChildProcOpen( HWND hwnd, LPARAM lParam );
	static BOOL CALLBACK EnumChildProc( HWND hwnd, LPARAM lParam );
};
    
class Email_URLEvaluator : public Evaluator {
public:
	Email_URLEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~Email_URLEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
private:
	string FindEmailURL (
		string email, 
		string pw, 
		FBB::Entity::EmailFindRule erule, 
		FBB::Entity::EmailURLRule urlrule, 
		COleDateTime dt, 
		string& turl,
		long waittime
	);

	bool MatchEmail(FabubaoMail::RxMailMessage& mailMsg, FBB::Entity::EmailFindRule& erule, COleDateTime dt);
	bool GetUrlFromMail(FabubaoMail::RxMailMessage& mailMsg, FBB::Entity::EmailURLRule& urlrule, string& turl);
};

class WaitReadyEvaluator : public Evaluator {
public:
	WaitReadyEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~WaitReadyEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};

class RecordCaptchaEvaluator : public Evaluator {
public:
	RecordCaptchaEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~RecordCaptchaEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);

	static const string autoCaptchaModelDir;
};

class RepeatCaptchaEvaluator : public Evaluator {
public:
	RepeatCaptchaEvaluator(IEEntityClass* ieEntity)
	 : Evaluator(ieEntity)
	  {}
	~RepeatCaptchaEvaluator(){}

	virtual bool evaluate(FBB::Entity::OneOptRegEntity oor);
};