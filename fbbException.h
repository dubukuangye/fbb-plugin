#pragma once

#include<exception>  
#include<iostream>  
using namespace std;  
  
namespace FBB{
	namespace Exception{
		class FBBException : public exception
		{
		public:
			FBBException(string m="exception!") : _msg(m) {}
			~FBBException() throw() {}
			const char* what() const throw() { return _msg.c_str(); }

		private:
			string _msg;
		};
	}
}
