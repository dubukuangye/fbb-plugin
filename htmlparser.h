#pragma once

#include <string>

#include <windows.h>
#include <wininet.h>
#include <urlmon.h>
#include <comdef.h>
#include <mshtml.h>
#include <atlbase.h>
#include <string>
#include <vector>
#include <iostream>
#include <map>
#include <ExDisp.h>
#include <comutil.h>

#include "regpara.h"
#include "fbbException.h"

#import "mshtml.tlb" no_auto_exclude rename("TranslateAccelerator","TranslateAccelerator_HTML")

using namespace std;

namespace FBB{
	namespace Parser{
		class Finder;
		class IDFinder;
		class IndexFinder;
		class PartialInnerTextFinder;
		class FullInnerTextFinder;

		class HtmlParserClass
		{
		public:
			HtmlParserClass() {
				createFinder();
			}
			~HtmlParserClass();
			
			void setDocumentPtr(MSHTML::IHTMLDocument2Ptr pDoc2) {
				this->pDoc2 = pDoc2;

				// when document load complete, overrite "alert" method in JavaScript
				// pDoc2->parentWindow->execScript(L"function alert(obj){return \"\";}", L"javascript"); // ����JS�е�alert����,��ֹ����
			}
			MSHTML::IHTMLDocument2Ptr getDocumentPtr() {
				return pDoc2;
			}

			STDMETHODIMP copyImage(FBB::Entity::DomElementEntity captureEle, MSHTML::IHTMLElementPtr captchaele);
			
			MSHTML::IHTMLElementPtr FindElement(FBB::Entity::DomElementEntity domele);

			wstring getOuterText();
			bool changeTargetAttribute();
			
		private:
			void createFinder();
			Finder* lookupFinder(FBB::Entity::DomElementEntity::DomElementSeachTypeEntity stype) {
				if( finderMap.find(stype)==finderMap.end() )
					throw FBB::Exception::FBBException("[FindElement/lookupFinder] : error");
				
				return finderMap[stype];
			}

			typedef enum InputElementTyp{
				IET_UNKNOW,
				IET_TEXT,
				IET_PASSWORD,
				IET_HIDDEN,
				IET_CHECKBOX,
				IET_RADIO,
				IET_SELECT,
				IET_TEXTAREA
			}IET;

			MSHTML::IHTMLDocument2Ptr pDoc2;
			map<FBB::Entity::DomElementEntity::DomElementSeachTypeEntity, Finder*> finderMap;
		};

		class Finder {
		public:
			Finder(HtmlParserClass* parser) : p_parser(parser) {}
			virtual ~Finder(){}
			virtual MSHTML::IHTMLElementPtr find(FBB::Entity::DomElementEntity domele) = 0;
		protected:
			HtmlParserClass* p_parser;
		};
		class IDFinder : public Finder {
		public:
			IDFinder(HtmlParserClass* parser) : Finder(parser) {}
			~IDFinder(){}
			virtual MSHTML::IHTMLElementPtr find(FBB::Entity::DomElementEntity domele);
		};
		class IndexFinder : public Finder {
		public:
			IndexFinder(HtmlParserClass* parser) : Finder(parser) {}
			~IndexFinder(){}
			virtual MSHTML::IHTMLElementPtr find(FBB::Entity::DomElementEntity domele);
		private:
			MSHTML::IHTMLElementPtr getBodyElement(FBB::Entity::DomElementEntity domele);
		};
		class PartialInnerTextFinder : public Finder {
		public:
			PartialInnerTextFinder(HtmlParserClass* parser) : Finder(parser) {}
			~PartialInnerTextFinder(){}
			virtual MSHTML::IHTMLElementPtr find(FBB::Entity::DomElementEntity domele);
		};
		class FullInnerTextFinder : public Finder {
		public:
			FullInnerTextFinder(HtmlParserClass* parser) : Finder(parser) {}
			~FullInnerTextFinder(){}
			virtual MSHTML::IHTMLElementPtr find(FBB::Entity::DomElementEntity domele);
		};
	}
}