简介
---

发布宝插件是以[Firebreath-1.7](http://www.firebreath.org/display/documentation/FireBreath+Home)为框架搭建的浏览器插件，可在Firefox、Chrome、IE等浏览器下正常运行。目前仅支持Window平台。

**（注意：为了支持Windows XP，不要用C++11新特性，开发工具请选择Visual Studio 2010）**
	
发布宝插件的主要作用是：根据通过webservice从服务器获取的参数，自动注册网站、登录并发布商情等。


对外属性和接口
------------

在包含了插件的页面中，Javascript代码可通过插件的对外属性与接口，与插件进行交互。

### 属性 ###

*  version
*  valid

### 接口（方法） ###

* 主要接口
	1. initializeFBB
	2. registerInBatch
	3. publishInBatch
	4. stopRegisterOrPublish
* 验证码相关接口
  	1. setRegCaptchaAnswer
  	2. getRegCaptchaQuestion
  	3. setPublishCaptchaAnswer
 	4. getPublishCaptchaQuestion
* 辅助接口
  	1. beginDownload（a tricky implementation, just for 'UploadFile' evaluator）
      
  		从服务器下载文件，比如营业执照等


生成项目、编译、注册插件
-------------------

请参考[Firebreath官网](http://www.firebreath.org/display/documentation/FireBreath+Home)


关于源代码理解
------------

请参考[Wiki页面](https://bitbucket.org/dubukuangye/fbb-plugin/wiki/Home)

