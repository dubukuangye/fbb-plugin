#include "htmlparser.h"
#include <string>
#include <cstring>
#include <atlbase.h>
#include "utf8_tools.h"

FBB::Parser::HtmlParserClass::~HtmlParserClass() {
	map<FBB::Entity::DomElementEntity::DomElementSeachTypeEntity, Finder*>::iterator it = finderMap.begin();
	while( it!=finderMap.end() ) {
		delete it->second;
		it++;
	}
}

wstring FBB::Parser::HtmlParserClass::getOuterText(){
	HRESULT hr;
	CComBSTR bstr;

	if( FAILED(hr = pDoc2->body->get_outerHTML(&bstr)) )
		return L"ERROR";
	if( !bstr )
		return L"ERROR";
	
	std::wstring ws(bstr, SysStringLen(bstr));
	return ws;
}

// copy image from IHTMLDocument2 to clipboard
STDMETHODIMP FBB::Parser::HtmlParserClass::copyImage(FBB::Entity::DomElementEntity captureEle, MSHTML::IHTMLElementPtr captchaele)
{
	MSHTML::IHTMLDocument2Ptr pDoc = NULL;
	if( captureEle.frameIndexs.empty() ){
		pDoc = pDoc2;
	} else {
		MSHTML::IHTMLWindow2Ptr hw = pDoc2->parentWindow;
		for(unsigned int i=0; i<captureEle.frameIndexs.size(); i++){
			long numOfFrame = hw->frames->length;
			if( numOfFrame<=captureEle.frameIndexs[i] ){ return false; }

			CComVariant pvarIndex( captureEle.frameIndexs[i] );
			hw = hw->frames->item(&pvarIndex);
		}

		pDoc = hw->document;
	}

	HRESULT hr = E_FAIL;
	MSHTML::IHTMLElementPtr pelmBody = NULL;
	MSHTML::IHTMLElement2Ptr pelmBodyTwo = NULL;
	IDispatchPtr pdispCtrlRange = NULL;
	MSHTML::IHTMLControlElementPtr pCtrlElement = NULL;
	MSHTML::IHTMLControlRangePtr pCtrlRange = NULL;
	CComBSTR bstrCommand(L"Copy");
	_bstr_t bstr(bstrCommand);
	VARIANT_BOOL vbReturn;
	CComVariant vEmpty;

	if (FAILED(pDoc->get_body(&pelmBody)) || pelmBody == NULL)
		goto Cleanup;

	if (FAILED(pelmBody->QueryInterface(IID_IHTMLElement2, (void**) &pelmBodyTwo)) 
		|| pelmBodyTwo == NULL)
		goto Cleanup;

	pdispCtrlRange = pelmBodyTwo->createControlRange();
	if( pdispCtrlRange==NULL ) goto Cleanup;

	if (FAILED(pdispCtrlRange->QueryInterface(IID_IHTMLControlRange, (void**) &pCtrlRange)) 
		|| pCtrlRange == NULL)
		goto Cleanup;

	pCtrlElement = static_cast<MSHTML::IHTMLControlElementPtr>(captchaele);
	if( pCtrlElement==NULL )
		goto Cleanup;

	hr = pCtrlRange->add(pCtrlElement);

	if (SUCCEEDED(hr))
		vbReturn = pCtrlRange->execCommand(bstr, VARIANT_FALSE, vEmpty);

	hr = S_OK;

Cleanup:

	::SysFreeString(bstrCommand);

	return hr;
}

// to ensure open in the same window
bool FBB::Parser::HtmlParserClass::changeTargetAttribute(){
	HRESULT hr;
	MSHTML::IHTMLFormElementPtr pFormElem;
	MSHTML::IHTMLAnchorElementPtr pAnchorElem;
	wstring realValue = L"_self";
	CComBSTR bstr( realValue.c_str() );

	MSHTML::IHTMLElementCollectionPtr pColl = pDoc2->forms;
	long length = pColl->length;
	for(int i=0; i<length; i++){
		pFormElem = static_cast<MSHTML::IHTMLFormElementPtr>( pColl->item(i) );
		if( pFormElem==NULL )
			return false;

		if( FAILED( hr=pFormElem->put_target(bstr) ) )
			return false; 
	}

	pColl = pDoc2->links;
	length = pColl->length;
	for(int i=0; i<length; i++){
		// for debug
		// MSHTML::IHTMLElementPtr pElem = static_cast<MSHTML::IHTMLElementPtr>( pColl->item(i) );
		// _bstr_t bstr1 = pElem->outerHTML;

		pAnchorElem = static_cast<MSHTML::IHTMLAnchorElementPtr>( pColl->item(i) );
		if( pAnchorElem==NULL ) continue;

		if( FAILED( hr=pAnchorElem->put_target(bstr) ) )
			return false;
	}
	return true;
}

/******************** implement ****Finder class *************/

void FBB::Parser::HtmlParserClass::createFinder() {
	finderMap[FBB::Entity::DomElementEntity::ID] = new IDFinder( this );
	finderMap[FBB::Entity::DomElementEntity::Indexes] = new IndexFinder( this );
	finderMap[FBB::Entity::DomElementEntity::PartialInnerText] = new PartialInnerTextFinder( this );
	finderMap[FBB::Entity::DomElementEntity::FullInnerText] = new FullInnerTextFinder( this );
}

MSHTML::IHTMLElementPtr FBB::Parser::HtmlParserClass::FindElement(FBB::Entity::DomElementEntity domele){
	Finder* finder = lookupFinder( domele.stype ); 
	return finder->find( domele );
}

MSHTML::IHTMLElementPtr FBB::Parser::IndexFinder::getBodyElement(FBB::Entity::DomElementEntity domele) {
	if( p_parser==NULL )
		throw FBB::Exception::FBBException("[FindElement/IndexFinder] : p_parse=null error");

	MSHTML::IHTMLDocument2Ptr pDoc2 = p_parser->getDocumentPtr();
	if( pDoc2==NULL )
		throw FBB::Exception::FBBException("[FindElement/IndexFinder] : pDoc2=null error");

	MSHTML::IHTMLElementPtr body = NULL;
	MSHTML::IHTMLWindow2Ptr hw = pDoc2->parentWindow;

	if( domele.frameIndexs.empty() ) {
		body = pDoc2->body;
	} else {
		for(unsigned int i=0; i<domele.frameIndexs.size(); i++) {
			long numOfFrame = hw->frames->length;
			if( numOfFrame<=domele.frameIndexs[i] )
				throw FBB::Exception::FBBException("[FindElement/IndexFinder] : numOfFrame<=domele.frameIndexs[i] error");

			CComVariant pvarIndex( domele.frameIndexs[i] );
			hw = hw->frames->item(&pvarIndex);
		}

		body = hw->document->body;
	}

	if( body==NULL )
		throw FBB::Exception::FBBException("[FindElement/IndexFinder] : body=null error");

	return body;
}

MSHTML::IHTMLElementPtr FBB::Parser::IDFinder::find(FBB::Entity::DomElementEntity domele) {
	MSHTML::IHTMLDocument2Ptr pDoc2 = p_parser->getDocumentPtr();
	MSHTML::IHTMLElementCollectionPtr pColl = pDoc2->all;
	MSHTML::IHTMLElementPtr the = static_cast<MSHTML::IHTMLElementPtr>( pColl->item(CComVariant(domele.eleId.c_str()), CComVariant(0)) );
	if( the==NULL ) 
		throw FBB::Exception::FBBException("[FindElement/IDFinder] : null error");

	return the;
}

MSHTML::IHTMLElementPtr FBB::Parser::IndexFinder::find(FBB::Entity::DomElementEntity domele) {
	MSHTML::IHTMLElementCollectionPtr pColl = NULL;

	MSHTML::IHTMLElementPtr the = getBodyElement(domele);

	if ( !domele.indexes.empty() ) {
		std::vector<long>::reverse_iterator rit = domele.indexes.rbegin();

		for (rit = domele.indexes.rbegin(); rit!= domele.indexes.rend(); ++rit){
			long validIndex = (*rit);

			// for debug
			// _bstr_t bstr = the->GetouterHTML();
			if( the==NULL ) 
				throw FBB::Exception::FBBException("[FindElement/IndexFinder] : the=null error");

			MSHTML::IHTMLDOMNodePtr node = static_cast<MSHTML::IHTMLDOMNodePtr>(the);
			MSHTML::IHTMLDOMChildrenCollectionPtr cns = static_cast<MSHTML::IHTMLDOMChildrenCollectionPtr>(node->childNodes);	
			long index = 0;
			bool find = false;
			// for debug
			// long debug_length = cns->length;
			for(int i=0;i<cns->length;i++) {
				MSHTML::IHTMLDOMNodePtr cnst=static_cast<MSHTML::IHTMLDOMNodePtr>(cns->item(i));
				if(cnst->nodeType==1||cnst->nodeType==8)
					index++;
				if( index==validIndex+1 ) {
					find = true;
					the = cns->item(i);
					break;
				}
			}	
			if( !find )
				throw FBB::Exception::FBBException("[FindElement/IndexFinder] : !find error");
		}
	} 

	if( the==NULL )
		throw FBB::Exception::FBBException("[FindElement/IndexFinder] : the=null error");

	return the;
}

MSHTML::IHTMLElementPtr FBB::Parser::PartialInnerTextFinder::find(FBB::Entity::DomElementEntity domele) {
	MSHTML::IHTMLDocument2Ptr pDoc2 = p_parser->getDocumentPtr();
	MSHTML::IHTMLElementCollectionPtr pColl = pDoc2->all;
	MSHTML::IHTMLElementPtr the = NULL;
	long len = pColl->length;

	for(int i=0; i<len; i++){
		CComVariant va(i);
		the = static_cast<MSHTML::IHTMLElementPtr>( pColl->item(va) );
		if( the==NULL )
			throw FBB::Exception::FBBException("[FindElement/PartialInnerTextFinder] : the=null error");

		wstring wsPitext = FB::utf8_to_wstring( domele.pitext );
		if( wcsstr( (const wchar_t*)the->innerText, wsPitext.c_str())!=NULL )
			return the;
	}
	
	throw FBB::Exception::FBBException("[FindElement/PartialInnerTextFinder] : not find error"); // not find
}

MSHTML::IHTMLElementPtr FBB::Parser::FullInnerTextFinder::find(FBB::Entity::DomElementEntity domele) {
	MSHTML::IHTMLDocument2Ptr pDoc2 = p_parser->getDocumentPtr();
	MSHTML::IHTMLElementCollectionPtr pColl = pDoc2->all;
	MSHTML::IHTMLElementPtr the = NULL;
	long len = pColl->length;

	for(int i=0; i<len; i++){
		CComVariant va(i);
		the = static_cast<MSHTML::IHTMLElementPtr>( pColl->item(va) );
		if( the==NULL )
			throw FBB::Exception::FBBException("[FindElement/FullInnerTextFinder] : the=null error");

		wstring wsFitext = FB::utf8_to_wstring( domele.fitext );
		if( wcscmp( (const wchar_t*)the->innerText, wsFitext.c_str())==NULL )
			return the;
	}
	
	throw FBB::Exception::FBBException("[FindElement/FullInnerTextFinder] : not find error");
}

/********************* implement ****Finder class *************/