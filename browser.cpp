#pragma once
#include "browser.h"

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <sstream>

bool FBB::Browser::BrowserClass::Init()//g
{
	ie = getBrowserPtr();
	if(ie==NULL)
		return false;
	evobj=new CIEEvent();
	if(!evobj->AttachEvent(ie))
		return false;
	return true;
}

SHDocVw::IWebBrowser2Ptr FBB::Browser::BrowserClass::getBrowserPtr(){
	if( ie!=NULL ){ return ie; }
	else {
		HRESULT hr=ie.CreateInstance("InternetExplorer.Application");
		if(FAILED(hr))
			return NULL;

		ie->Visible = true;
		ie->Silent = true;
		
		return ie;
	}
}

bool FBB::Browser::BrowserClass::shutdownIE() { 
	 // try {
	 // 	HRESULT hr;
	 // 	if( ie==nullptr )
	 // 		return true;
	 // 	if( FAILED( hr=ie->Quit() )){
	 // 		if( FAILED( hr=ie->Quit() )){
	 // 			return false;
	 // 		} else {
	 // 			return true;
	 // 		}
	 // 	} else {
	 // 		return true;
	 // 	} 
	 // } catch (std::exception& ) {

	 // } catch (...) {
		
	 // }

	 // return false;

	return true;
}

bool FBB::Browser::BrowserClass::invokeMember(MSHTML::IHTMLElementPtr pElem, string eleOpt, FBB::Parser::HtmlParserClass* p_parser){
	// process according to eleOpt
	HRESULT hr;
	if( eleOpt=="focus" ){
		if( FAILED( hr=static_cast<MSHTML::IHTMLElement2Ptr>(pElem)->focus() ) )
			return false;
	} else if( eleOpt=="click" ){ // need to judge document load complete
		if( FAILED( hr=pElem->click() ) )
			return false;
	} 

	return true;
}

bool FBB::Browser::BrowserClass::navigate(std::string url, FBB::Parser::HtmlParserClass* p_parser){ // p_parser is useless
	HRESULT hr = ie->Navigate( _bstr_t(url.c_str()) );
	checkDocumentComplete();	
	return getDocumentPtr()==NULL ? false : true;
}

void FBB::Browser::BrowserClass::checkDocumentComplete(UINT msg,int msperloop){
	int nochange_count = 0;
	int loop_count = 0;
	long last_doc_length = 0;

	while( nochange_count<3 ) {
		boost::this_thread::interruption_point();
		
		loop_count++;
		if( loop_count>120 ) // maximum loop time, can change
			throw FBB::Exception::FBBException("[checkDocumentComplete] : loop_count>120");
		if(WaitMsg(msg,msperloop))
			break;

		if( likeDocumentComplete(last_doc_length) )
			nochange_count++; 
		else 
			nochange_count = 0;
	}

	MSHTML::IHTMLDocument2Ptr pDoc2 = getDocumentPtr();
	if( pDoc2==NULL ) 
		throw FBB::Exception::FBBException("[checkDocumentComplete] : pDoc2=NULL error");

	_parserPtr->setDocumentPtr( pDoc2 );
	_parserPtr->changeTargetAttribute(); // change 'target' --> '_self'
	
}

bool FBB::Browser::BrowserClass::WaitMsg(UINT msgtype,int millionsecond)
{
	const ULONG nTimeoutTime = GetTickCount() + millionsecond;
	const HANDLE hFakeEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	bool getit=false;
	for(; ; )
	{
		const LONG nWaitTime = nTimeoutTime - GetTickCount();		
		if(nWaitTime <= 0)
			break; // Timeout
		const DWORD nWaitResult = MsgWaitForMultipleObjects(1, &hFakeEvent, FALSE, nWaitTime, QS_ALLINPUT | QS_ALLPOSTMESSAGE);    
		if(nWaitResult == WAIT_TIMEOUT)
			break; // Timeout
		MSG Message;
		while(PeekMessage(&Message, NULL, WM_NULL, WM_NULL, PM_REMOVE))
		{   
			if (msgtype == Message.message && NULL == Message.hwnd)
			{
				getit=true;
				//
				//console.log('event msg\n');
				break;
			}
			else
			{
				TranslateMessage(&Message);
				DispatchMessage(&Message);				
			}
		}
		if(getit)
			break;
	}
	CloseHandle(hFakeEvent);
	return getit;
}

bool FBB::Browser::BrowserClass::likeDocumentComplete(long& last_doc_length){
	try {
		if( ie->Busy==VARIANT_FALSE&&ie->ReadyState==READYSTATE_COMPLETE ){	
			long doc_length = getDocumentLength();
			if( doc_length==last_doc_length ) return true;
			else {
				last_doc_length = doc_length;
				return false;
			}
		} else { return false; }
	} catch(const _com_error& ) { return false; }
}

long FBB::Browser::BrowserClass::getDocumentLength(){
	MSHTML::IHTMLDocument2Ptr pDoc2 = getDocumentPtr();
	if( pDoc2==NULL )
		return -1;
	else 
		return pDoc2->all->length;
}

MSHTML::IHTMLDocument2Ptr FBB::Browser::BrowserClass::getDocumentPtr(){
	MSHTML::IHTMLDocument2Ptr doc=NULL;
	try
	{
		doc=static_cast<MSHTML::IHTMLDocument2Ptr>( ie->Document );
	}
	catch(...){
		doc=NULL;
	}
	return doc;
}