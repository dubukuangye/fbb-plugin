#pragma once

#include <boost/thread/mutex.hpp>

#include "fbbException.h"
#include "wsClient.h"

class UserinformationClass
{
private:
	UserinformationClass();

	static UserinformationClass* _singleton;
	static long _userId;
	map<wstring, wstring> _userInfoMap;

	static boost::mutex instance_mutex;
public:
	~UserinformationClass(){}

	static UserinformationClass* getInstance() {
		if( _singleton==NULL ) {
			boost::mutex::scoped_lock lock(instance_mutex);
			if( _singleton==NULL )
				_singleton = new UserinformationClass();
		}

		return _singleton;
	}

	static void deleteInstance() {
		delete _singleton;
		_singleton = NULL;
	}

	static void setUserId(long userId) {
		_userId = userId;
	}

	static long getUserId() {
		return _userId;
	}

	wstring getFromUserInfo(wstring first);
};
